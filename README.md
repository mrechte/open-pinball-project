Open Pinball Project, hosted at https://gitlab.com/mrechte/open-pinball-project

This is a fork of open-pinball-project https://sourceforge.net/projects/open-pinball-project/ at version 2.4.0.0.

It consists in hardware design (wing cards) and software (firmware) for an stm32 based MCU, called blue pill.

The aim of this project is to provide cheap hardware to control EM pinball machines. 

This is the low level part of the project, the high level (playfield logic) being controlled by The Mission Pinball Framework (or other). 

A single BluePill MCU may control up to 32 GPIO lines, logically divided in 4 wings of 8 lines each.

A wing is usually specialized (switch inputs, solenoids, lamps, ...) according to the hardware attached.

See https://pinballmakers.com/ and https://missionpinball.org/ for details


This version focuses on the blue pill plus (https://github.com/WeActStudio/BluePill-Plus) which has the advantage
of having blue LED on PB2 which is not used by card headers. It also features an USBC-C connector.

However note that the debug connector is not positioned exactly as on BluePill and that there is slightly different pin mapping.

The blue pill plus features a STM32F103C8T6 - LQFP48 (48-pin) (STM32F103x8)
    72Mhz, 20KB RAM, 64KB ROM (Medium-density) Cortex-M3
    flash disposes of 64 (0x40) 1KB pages (0x0400)
    remark: BOOT0 switch is useless because this MCU only supports boot loading from USART1 (PB6, PB7).


# Firmware

## What differs

This firmware is a development version numbered 2.4.ff.xx where xx is the build number (see opp.h).
When considered production ready, it will bear 2.5.0.0 version.


We try to keep code compatible with the original version (not alter existing commands).

The only wing types currently tested are:
- WING_SOL
- WING_INP
- WING_INCAND
- WING_NEO
- WING_NEO_SOL
- WING_SOL_8

## Release notes

2025/01/31 tag 2.4.255.11
- fix solenoid initial kick is not lasting expected time. Init kick pwm is 31, not 255. 
- suppress Extended Configure Solenoid Board command, 0x1c (and update gen2test.py accordingly),
  introduced recently, and use the legacy command 0x06 with extra parameters. These config commands changes are not considered breaking, 
  because they are used once (typically with gen2test), and are not used by third party software like MPF.

2024/10/24 tag 2.4.255.10
- fix a regression on solenoid module since code refactoring (this module no longer worked on 2.4.255.9). Took opportunity to improve the module.
- reorder parameters for extended solenoid config command (this is for memory alignment purpose).
- gen2test.py: add a find card by serial command (-f).

2024/09/16 tag 2.4.255.9
- huge code refactor to make it more modular. This will also make it easier to understand.
- switch debounce is now time based (rather than loop count based). Default 5ms. Config commands have been updated accordingly.
- refactor and test NeoPixel code (was not working before).
- NeoPixel can now manage a second LED string on wing 3.
- command 0x12 (Set neopixel Color Table) does not require the 91 unused bytes. These unused bytes have been removed from flash (nvm).
  The command has been renamed to Cfg neopixel and an extra byte has been added (ChainType) in order to distinguish between wing 0 / 3
  and between neopxl and spipixel (rather then relying on a special color byte). A new fade type (offset) NEO2 has been added to cope 
  with the second LED string.
- These config commands changes are not considered breaking, because they are used once (typically with gen2test), and are not used by third party software like MPF.
- gen2test.py has a --fade command, and neoCfg (before colorCfg) from config file is now honoured.
  However inpCfg in config files, will have to be updated to account for the new debounce value.
- a demo_neo.py program has been added.
- interface document updated accordingly.


2024/04/18 tag 2.4.255.8
- fix Set Solenoid Input, cmd 0x17 to clear an input
- gen2test.py has now got a shell and other options

2024/01/04 2.4.255.7
- add crcErr, badCmd, timeOut error counters
- gen2test.py updated accordingly


2023/12/30 tag 2.4.255.6
- is now able to manage 32 solenoids using 4 WING_SOL_8 wings, while keeping compability with legacy firmware
- rework legacy WING_SOL code in order to keep compatibility (formely introduced regression)
- add protocol command 0x27 "Extended Kick Solenoids" to allow kicking 16..31 solenoids
- refactor, refactor, ..., more comments
- gen2test.py updated with the new commands
- interface document updated accordingly.

2023/12/23 tag 2.4.255.5
- fixes several issues with solenoid proc:
  * interference between input control and software control, like in a coil show
  * MPF can reconfigure a coil just to issue a pulse with specific parameters, and reset back the original conf, while the pulse is not complete. 
    This was not handled properly by OPP.
- adds protocol debug command 0xe0 "Get Solenoid state"
- refactor code to make it more readable, add more comments (some modules)
- several minor changes
- gen2test.py: updated with new parameters. Allow to specify a config file loacted in any directory.
- interface documentation updated

2023/12/01 tag 2.4.255.4
- fixes serial num erased when saving config.

2023/09/28 2.4.2553.3 Several changes have been made since 2.4.0.0 version fork:
- Fix flash code according to PM0068 manual
- Add an Error_Handler routine that will flash blue LED fast 5 times, followed by slow blink, where stroke # indicates:
  * 1 = Hardware initialization error
  * 2 = assert error (cf. __assert_func)
  * 3 = hard fault (cf. HardFault_Handler)
  * 4 = unable to program flash
  * 5 = unable to alloc memory in heap
- add protocol commands 0x1C "extended configure solenoid board" and 0x1D "extended configure individual solenoid"
- add protocol command 0x1E "get wing config"
- insert asserts in code
- support new wing type WING_SOL_8 driving 8 solenoids (USE_SWITCH hence not applicable).
- save extra config parameters to flash (solenoid PWM and assigned inputs)
- CRC is now checked for all regular commands
- Interface document updated
- gen2test.py: ported to Python 3 with new options.


# Hardware

O16I16: 16 soldenoid / incandescent outputs and 16 inputs combo card

![O16I16 with compnents](hardware/opp16O16I/img/O16I16_comps.jpg)


O32:    32 outputs soldenoid / incandescent combo card

This is the card equipped with low budget transitors to drive lamps.

![O32 with budget transitors](hardware/opp32O/img/O32_comps.jpg)


## Release notes

2024/04/24 tag O32-O16I16-rev1

- O32: increase spacing between tracks (some short circuit found at production)
- O32 + O16I16: bluepill 4-pin connector : holes are too small
- 032: short circuit (at production) on FET transisors (drain - source) for 2-0, 2-1 and 2-4
- 032 + O16I16: add reset pin to ST/Link connector
- 032 + O16I16: add more silk text to identify I/O pins
- 032 + O16I16: add OPP logo


# Work in progress / planned / bugs

Firmware:
- for commands that requires parameters, if they do not come after a given amount of time, the command should be discarded !
- add an ACK/NACK return for each configuration protocol command to confirm / infirm commands (optional).
- handle dual circuit coils with EOS extra switch, viz. having one EOS input to fire main + hold coils (in serial) 
    AND release main coil, and one input to fire main coil. That woulld require to add a coil mode where trigger is a switch and release another switch...
- find a way to report GEN2G_DEBUG_PORT value
- sending color table with no WING_NEO wing, produces a hard fault (TBC)
- USB DFU: altough USB DFU boot loader project exists for this MCU (4-8 KB footprint however), unfortunately we have no switch to force boot mode
  BOOT0 cannot be used for that purpose. Alternative would be to provide a flash read / write command. If something goes wrong during the process,
  we would end up in an unsable firmware. Thanks to checksum validation, the firmware would enter DFU. See new OPP implementation (booty). 
  I will *not* implement a boot loader, considering this MCU does not support it through USB.
- Default value for switch debounce is a bit high 50 => 40.

Hardware (using WING_SOL_8 wings):
- See silk label for reset pin on ST/Link
