#!/usr/bin/env python
#
#Interface data (linked to serial protocol)

GET_SER_NUM         = 0x00
GET_PROD_ID         = 0x01
GET_VERS            = 0x02
SET_SER_NUM         = 0x03
RESET               = 0x04
GO_BOOT             = 0x05
CONFIG_SOL          = 0x06
KICK_SOL            = 0x07
READ_INP            = 0x08
CONFIG_INP          = 0x09
SAVE_CFG            = 0x0b
ERASE_CFG           = 0x0c
GET_CFG             = 0x0d
SET_CFG             = 0x0e
CFG_NEO             = 0x12
INCAND              = 0x13
CONFIG_IND_SOL      = 0x14
CONFIG_IND_INP      = 0x15
SET_IND_NEO         = 0x16
SET_SOL_INPUT       = 0x17
READ_MATRIX_INP     = 0x19
GET_INP_TIMESTAMP   = 0x1a
SOL_KICK_PWM        = 0x1b
EXT_CONFIG_IND_SOL  = 0x1d
GET_WING_CFG        = 0x1e
EXT_KICK_SOL        = 0x27
FADE                = 0x40
GET_SOL_STATE       = 0xe0
GET_COUNTERS        = 0xe1

INV_CMD             = 0xf0
EOM_CMD             = 0xff

CARD_ID_TYPE_MASK   = 0xf0
CARD_ID_GEN2_CARD   = 0x20

NUM_G2_WING_PER_BRD = 4
WING_UNUSED         = 0x00
WING_SOL            = 0x01
WING_INP            = 0x02
WING_INCAND         = 0x03
WING_SW_MATRIX_OUT  = 0x04
WING_SW_MATRIX_IN   = 0x05
WING_NEO            = 0x06
WING_HI_SIDE_INCAND = 0x07
WING_NEO_SOL        = 0x08
WING_SPI            = 0x09
WING_SW_MATRIX_OUT_LOW = 0x0a
WING_LAMP_MATRIX_COL= 0x0b
WING_LAMP_MATRIX_ROW= 0x0c
WING_SOL_8          = 0x0d

NUM_INP             = 32
NUM_SW_MATRIX_INP   = 64

CFG_INP_STATE       = 0x00
CFG_INP_FALL_EDGE   = 0x01
CFG_INP_RISE_EDGE   = 0x02

NUM_SOL_LEGACY      = 16
NUM_SOL             = 32

CFG_SOL_DISABLE     = 0x00
CFG_SOL_USE_SWITCH  = 0x01
CFG_SOL_AUTO_CLR    = 0x02
CFG_SOL_ON_OFF      = 0x04
CFG_SOL_DLY_KICK    = 0x08
CFG_SOL_USE_MTRX_INP= 0x10
CFG_SOL_CAN_CANCEL  = 0x20

#Note:  Derived from above bits
CFG_SOL_ON_OFF_USE_SW = 0x05
CFG_SOL_USE_MTRX_AUTO_CLR= 0x12

NUM_NEO             = 2

