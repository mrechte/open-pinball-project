#!/bin/env python

#===============================================================================
#
# gen2test.py
# Test and config utility for Gen2 OPP cards.
#
#===============================================================================

testVers = '00.00.10'

import sys
import argparse
import serial
import array
import time
import os
from os.path import splitext, expanduser
import importlib.util
from glob import glob
from os import stat, makedirs, umask, sep
from readline import set_history_length, read_history_file, write_history_file
from time import sleep

# Must follow OPP_CODE_VERS in opp.h
__version__ = "2.4.255.11"

from rs232intf import *

debug = False
matrixInpData = []
gen2Addr = 0x20
currWingCfg = []
cardVersion = None
cardSerNum = None
hasMatrix = False
ser = None

CRC8ByteLookup = \
    [ 0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15, 0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d, \
      0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65, 0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d, \
      0xe0, 0xe7, 0xee, 0xe9, 0xfc, 0xfb, 0xf2, 0xf5, 0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd, \
      0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85, 0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd, \
      0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2, 0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea, \
      0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2, 0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a, \
      0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32, 0x1f, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0d, 0x0a, \
      0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42, 0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a, \
      0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c, 0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4, \
      0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec, 0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4, \
      0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c, 0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44, \
      0x19, 0x1e, 0x17, 0x10, 0x05, 0x02, 0x0b, 0x0c, 0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34, \
      0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b, 0x76, 0x71, 0x78, 0x7f, 0x6a, 0x6d, 0x64, 0x63, \
      0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b, 0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13, \
      0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb, 0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83, \
      0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb, 0xe6, 0xe1, 0xe8, 0xef, 0xfa, 0xfd, 0xf4, 0xf3 ]


WING_TYPE_STR = ("UNUSED_WING", "SOL_WING", "INP_WING", "INCAND_WING", "SW_MATRIX_OUT_WING", "SW_MATRIX_IN_WING", "NEO_WING",
    "INCAND_HI_WING", "NEO_SOL_WING", "SPI_WING", "SW_MATRIX_OUT_LOW_WING", "LAMP_MATRIX_COL_WING", "LAMP_MATRIX_ROW_WING", "SOL_8_WING")

SOL_FLAGS = ("DISABLE", "USE_SWITCH", "AUTO_CLR", "ON_OFF", "DLY_KICK", "USE_MTRX_INP", "CAN_CANCEL")

INP_TYPES = ("STATE_INPUT", "FALL_EDGE", "RISE_EDGE")

INCAND_FLAGS = ("ROT_LEFT", "ROT_RIGHT", "LED_ON", "LED_OFF", "LED_BLINK_SLOW", "LED_BLINK_FAST", "LED_BLINK_OFF", "LED_SET_ON_OFF", "SET")
INCAND_SET_FLAGS = ("SET_ON", "SET_BLINK_SLOW", "SET_BLINK_FAST")

# Do not change order
FADE_TYPES = ("NEO", "INCAND", "MATRIX", "SERVO", "NEO2")

SOL_STATES = ("STATE_IDLE", "INITIAL_KICK", "SUSTAIN_PWM", "MIN_TIME_OFF", "WAIT_BEFORE_KICK", "FULL_ON_SOLENOID")


def open_serial(port):
    """ try to open the given serial port """
    global ser
    try:
        # timeout should be 0.2 if using a debugger, or 0.1 if not.
        ser = serial.Serial(port, baudrate=115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=.1)
        #flushSerialData() # flush buffer
    except serial.SerialException:
        ser = None
    

def calcCrc8(msgChars):
    """ Calculate a crc8. msgChars is an array of bytes.
    """
    crc8Byte = 0xff
    for indChar in msgChars:
        indInt = int(indChar)
        crc8Byte = CRC8ByteLookup[crc8Byte ^ indInt]
    return crc8Byte


#grab data from serial port
def getSerialData():
    resp = ser.read(100)
    if (len(resp) == 0):
        print("Timed out while reading on serial port")
        proto_error("getSerialData", 100)
        return None
    # strip leading EOM (except last)
    while len(resp) > 1 and resp[0] == EOM_CMD:
        resp = resp[1:]  
    return resp


def flushSerialData():
    ser.read(1000)


#rcv ack or nack message resp (TODO in firmware)
def rcvAckNackResp():
    """    
    data = getSerialData();
    if (data[0] != EOM_CMD):
        return 300
    """
    return 0


#send 4 byte data command
def send4ByteDataCmd(cmd):
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(cmd)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send 0 byte data command
def send0ByteDataCmd(cmd):
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(cmd)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0



# get a response for commmand @cmd expecting @nvarbytes variable bytes
def getResp(cmd, nvarbytes):
    data = getSerialData()
    if not data:
        return None
    if len(data) < 4:
        print("\nToo short response")
        dump(data, True)
        return None
    if (data[0] != gen2Addr):
        print("\nAddress data = %d, expected = %d" % (data[0], gen2Addr))
        dump(data, True)
        return None
    if (data[1] != cmd):
        print("\nCmd data = %d, expected = %d" % (data[1], cmd))
        dump(data, True)
        return None
    vardatalen = len(data) - 4  # Addr, cmd, CRC, EOM
    if vardatalen != nvarbytes:
        print("nData length = %d, expected = %d" %  (vardatalen, nvarbytes))
        dump(data, True)
        return None
    tmpData = data[0:-2]
    crc8 = calcCrc8(tmpData)
    if (data[-2] != crc8):
        print("\nCRC data = %d, expected = %d" % (data[-2], crc8))
        dump(data, True)
        return None
    if (data[-1] != EOM_CMD):
        print("\nEOM data = %d, expected = %d" % (data[-1], EOM_CMD))
        dump(data, True)
        return None
    if debug: dump(data, True)
    return data[2:-2]


# get a response for commmand @cmd where variable part starts @offset, which is the length byte
def getVarLenResp(cmd, offset):
    assert offset > 1
    data = getSerialData()
    if not data:
        return None
    if len(data) < offset + 3: # len, [var data], CRC, EOM
        print("\nToo short response")
        dump(data, True)
        return None, None
    if (data[0] != gen2Addr):
        print("\nAddress data = %d, expected = %d" % (data[0], gen2Addr))
        dump(data, True)
        return None, None
    if (data[1] != cmd):
        print("\nCmd data = %d, expected = %d" % (data[1], cmd))
        dump(data, True)
        return None, None
    vardatalen = data[offset]
    if len(data) != offset + vardatalen + 3:  # Addr, cmd, [fixed data], len, [var data], CRC, EOM
        print("\nInvalid response length (%d, variable length expected %d)" % (len(data), vardatalen))
        dump(data, True)
        return None, None
    tmpData = data[0:-2]
    crc8 = calcCrc8(tmpData)
    if (data[-2] != crc8):
        print("\nCRC data = %d, expected = %d" % (data[-2], crc8))
        dump(data, True)
        return None, None
    if (data[-1] != EOM_CMD):
        print("\nEOM data = %d, expected = %d" % (data[-1], EOM_CMD))
        dump(data, True)
        return None, None
    if debug: dump(data, True)
    return data[2:offset], data[offset:-2]



def proto_error(fn, rc):
    global error
    print(fn + "() returned %d" % rc)
    error = True


def dump(cmdArr, inp=False):
    print('<' if inp else '>', cmdArr, ' - ', [*map(hex, cmdArr)])


def wingTypeStr(wing_type):
    if wing_type < 0 or wing_type >= len(WING_TYPE_STR):
        return "Error"
    return WING_TYPE_STR[wing_type]



# COMMANDS
# ===========================================

#send serial number command (0x00)
def sendGetSerNumCmd(quiet=False):
    if not quiet:
        print("Sending get serial num cmd (GET_SER_NUM - 0x00)")
    return send4ByteDataCmd(GET_SER_NUM)


#rcv get serial number response
def rcvGetSerNumResp(quiet=False):
    global cardSerNum
    data = getResp(GET_SER_NUM, 4)
    if not data:
        return 1100
    cardSerNum = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]
    if not quiet:
        if cardSerNum != 0xffffffff:
            print("Card has serial num 0x%08x (%d) programmed, so it will be preserved" % (cardSerNum, cardSerNum))
        else:
            print("Card has no serial num programmed")
    return 0


#send get version command (0x02)
def sendGetVersCmd(quiet=False):
    if not quiet:
        print("Sending get version cmd (GET_VERS - 0x02)")
    return send4ByteDataCmd(GET_VERS)


#rcv get version response
def rcvGetVersResp():
    global cardVersion
    data = getResp(GET_VERS, 4)
    if not data:
        return 1000
    cardVersion = str(data[0]) + "." + str(data[1]) + "." + str(data[2]) + "." + str(data[3])
    print("Firmware version = %s" % cardVersion)
    return 0

# send serial number (0x03)
def sendSetSerNumCmd(data, quiet=False):
    if not quiet:
        print("Sending set serial num cmd (SET_SER_NUM - 0x03)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(SET_SER_NUM)
    cmdArr.append((data >> 24) & 0xff)
    cmdArr.append((data >> 16) & 0xff)
    cmdArr.append((data >> 8) & 0xff)
    cmdArr.append(data & 0xff)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0

# send Reset (0x04)
def sendResetCmd(quiet=False):
    if not quiet:
        print("Sending reset cmd (RESET - 0x04)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(RESET)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0

# send Go boot (0x05)
# Not implemented on ARM plateform

# send sol cfg cmd (0x06)
def sendSolCfgCmd(quiet=False):
    if not quiet:
        print("Sending solenoid cfg cmd (CONFIG_SOL - 0x06)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(CONFIG_SOL)
    for loop in range(NUM_SOL):
        offset = loop * 5
        # send first 4 fields as bytes
        for i in range(4):
            cmdArr.append(cfgFile.solCfg[offset + i])
        u32 = cfgFile.solCfg[offset + 4]
        for i in range(4):
            cmdArr.append(u32 & 0xff)
            u32 >>= 8
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send kick solenoids (0x07)
def sendKickSolCmd(sols, mask, quiet=False):
    if not quiet:
        print("Sending kick solenoid cmd (KICK_SOL - 0x07)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(KICK_SOL)
    sols &= 0xffff
    mask &= 0xffff
    cmdArr.append(sols >> 8)
    cmdArr.append(sols & 0xff)
    cmdArr.append(mask >> 8)
    cmdArr.append(mask & 0xff)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send read input board (0x08)
def sendReadInpBrdCmd(quiet=False):
    if not quiet:
        print("Sending read input board cmd (READ_INP - 0x08)")
    return send4ByteDataCmd(READ_INP)


# rcv read input cmd
def rcvReadInpResp():
    currInpData = []
    data = getResp(READ_INP, 4)
    if not data:
        return 500
    # bytes order is port D, C, B, A, let's reorder
    currInpData.append(data[3])
    currInpData.append(data[2])
    currInpData.append(data[1])
    currInpData.append(data[0])
    outStr = ""
    for index in range(NUM_G2_WING_PER_BRD):
        if index > 0:
            outStr += ' '
        outStr += "W[%d]:" % index
        outStr += " {0:08b}".format(currInpData[index])
    print(outStr)
    return 0

# send input cfg cmd (0x09)
def sendInpCfgCmd(quiet=False):
    if not quiet:
        print("Sending input cfg cmd (CONFIG_INP - 0x09)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(CONFIG_INP)
    for loop in range(NUM_INP):
        offset = loop * 2
        inpCfg = cfgFile.inpCfg[offset:offset + 2]
        cmdArr.append(inpCfg[0])
        debounce = inpCfg[1]
        cmdArr.append(debounce >> 8)
        cmdArr.append(debounce & 0xff)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0

# Not implemented (0x0a)
# 

# send save cfg cmd (0x0b)
def sendSaveCfgCmd(quiet=False):
    if not quiet:
        print("Sending save cfg cmd (SAVE_CFG - 0x0b)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(SAVE_CFG)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send erase cmd (0x0c)
def sendEraseCmd(quiet=False):
    if not quiet:
        print("Sending erase cfg cmd (ERASE_CFG - 0x0c)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(ERASE_CFG)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send read wing cfg board (0x0d)
def sendReadWingCfgCmd(quiet=False):
    if not quiet:
        print("Sending get wing cfg (GET_CFG - 0x0d)")
    return send4ByteDataCmd(GET_CFG)


# rcv read wing cfg resp
def rcvReadWingCfgResp():
    data = getResp(GET_CFG, 4)
    if not data:
        return 700
    currWingCfg = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]
    print("WingCfg = 0x{:08x}".format(currWingCfg))
    for index in range(NUM_G2_WING_PER_BRD):
        outStr = "W[%d]:" % index
        outStr += wingTypeStr(data[index])
        if index < NUM_G2_WING_PER_BRD - 1:
            outStr += ", "
            print(outStr, end='')
        else:
            print(outStr)
    return 0


# send wing cfg cmd (0x0e)
def sendWingCfgCmd(quiet=False):
    if not quiet:
        print("Sending wing cfg cmd (SET_CFG - 0x0e)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(SET_CFG)
    for loop in range(NUM_G2_WING_PER_BRD):
        cmdArr.append(cfgFile.wingCfg[loop])
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send neopixel cmd (0x0f)
# No longer used


# send change neopixel cmd (0x10)
# No longer used


# send change neopixel color table cmd (0x11)
# No longer used


# send set neopixel color table cfg cmd (0x12)
def sendNeoCfgCmd(quiet=False):
    if not quiet:
        print("Sending neopixel cfg cmd (CFG_NEO - 0x12)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(CFG_NEO)
    cmdArr.extend(cfgFile.neoCfg)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send incandescent cmd (0x13)
def sendIncandCmd(cmd, a_mask, b_mask, c_mask, d_mask, quiet=False):
    if not quiet:
        print("Sending incandescent cmd (INCAND - 0x13)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(INCAND)
    cmd &= 0x8f
    cmdArr.append(cmd)
    cmdArr.append(d_mask)
    cmdArr.append(c_mask)
    cmdArr.append(b_mask)
    cmdArr.append(a_mask)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0



# send configure individual solenoid cmd (0x14)
def sendConfIndSol(sol, nflags, init_kick, prm, quiet=False):
    if not quiet:
        print("Sending configure individual solenoid cmd (CONFIG_IND_SOL - 0x14)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(CONFIG_IND_SOL)
    sol &= 0x0f
    cmdArr.append(sol)
    nflags &= 0xff
    cmdArr.append(nflags)
    init_kick &= 0xff
    cmdArr.append(init_kick)
    prm &= 0xff
    cmdArr.append(prm)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send configure individual input cmd (0x15)
def sendConfInput(switch, inptype, debounce, quiet=False):
    if not quiet:
        print("Sending conf individual input cmd (CONFIG_IND_INP - 0x15)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(CONFIG_IND_INP)
    switch &= 0x1f
    cmdArr.append(switch)
    inptype &= 0x03
    cmdArr.append(inptype)
    cmdArr.append(debounce >> 8)
    cmdArr.append(debounce & 0xff)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send individual neopixel cmd (0x16)
# TODO


# send set solenoid input cmd (0x17)
def sendConfSolInput(sol, switch, quiet=False):
    if not quiet:
        print("Sending set solenoid input cmd (SET_SOL_INPUT - 0x17)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(SET_SOL_INPUT)
    switch &= 0x1f
    cmdArr.append(switch)
    sol &= 0x8f
    cmdArr.append(sol)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send upgrade other board cmd (0x18)
# Not on ARM plateform


# send read matrix input cmd (0x19)
def sendReadMatrixCmd(quiet=False):
    if not quiet:
        print("Sending read matrix cmd (READ_MATRIX_INP - 0x19")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(READ_MATRIX_INP)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(0x00)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    ser.write(cmdArr)
    return 0


# rcv read matrix resp
def rcvReadMatrixResp():
    global matrixInpData
    data = getResp(READ_MATRIX_INP, 8)
    if not data:
        return 1600
    outStr = ""
    for index in range(NUM_G2_MATRIX_INP/8):
        matrixInpData[index] = data[index]
        outStr += "L[%d]:" % index
        outStr += " {0:08b}".format(matrixInpData[index])
        print(outStr)
    return 0


# send get input timestamp (0x1a)
def sendGetInpTsCmd(quiet=False):
    if not quiet:
        print("Sending get input timestamp cmd (GET_INP_TIMESTAMP - 0x1a)")
    return send0ByteDataCmd(GET_INP_TIMESTAMP)


# send with crc error get input timestamp (0x1a)
def sendCrcErrCmd(quiet=False):
    if not quiet:
        print("Sending with crc error get input timestamp cmd (GET_INP_TIMESTAMP - 0x1a)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(GET_INP_TIMESTAMP)
    cmdArr.append(0x03) # bad crc
    cmdArr.append(EOM_CMD)
    ser.write(cmdArr)
    return 0



def rcvGetInpTsResp():
    data = getResp(GET_INP_TIMESTAMP, NUM_G2_INP_PER_BRD*2)
    if not data:
        return 1
    for wing in range(NUM_G2_WING_PER_BRD):
        outStr = "W[%d]:" % wing
        for i in range(8):
            inp = wing * 8 + i
            ts = data[inp*2] << 8 + data[inp*2 + 1]
            outStr += " b{0:d}: {1:d}".format(i, ts)
        print(outStr)
    return 0
        
    
# send solenoid power PWM cmd (0x1b)
def sendConfSolPower(sol, pwm, quiet=False):
    if not quiet:
        print("Sending solenoid power PWM cmd (SOL_KICK_PWM - 0x1b)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(SOL_KICK_PWM)
    pwm &= 0x1f
    cmdArr.append(pwm)
    sol &= 0x0f
    cmdArr.append(sol)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send individual sol extended cfg cmd (0x1d)
def sendExtCfgIndSol(sol, nflags, init_kick, prm, kick_pwm, inputs, quiet=False):
    if not quiet:
        print("Sending extended configure individual solenoid cmd (EXT_CONFIG_IND_SOL - 0x1d)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(EXT_CONFIG_IND_SOL)
    sol &= 0x0f
    cmdArr.append(sol)
    nflags &= 0xff
    cmdArr.append(nflags)
    init_kick &= 0xff
    cmdArr.append(init_kick)
    prm &= 0xff
    cmdArr.append(prm)
    cmdArr.append(kick_pwm)
    u32 = inputs & 0xffffffff
    for i in range(4):
        cmdArr.append(u32 & 0xff)
        u32 >>= 8
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send get wing config cmd (0x1e)
def sendGetWingCfgCmd(wing, quiet=False):
    if not quiet:
        print("Sending get wing config cmd (GET_WING_CFG - 0x1e)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(GET_WING_CFG)
    wing &= 0x03
    cmdArr.append(wing)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0

def printSolCfg(sol, cfgdata):
    sol_type = cfgdata[0]
    init_kick = cfgdata[1]
    min_off_duty = cfgdata[2]
    kick_pwm = cfgdata[3]
    input_bits =  cfgdata[4] + (cfgdata[5] << 8) + (cfgdata[6] << 16) + (cfgdata[7] << 24)
    # flags
    if sol_type == 0:
        flags = SOL_FLAGS[0]
    else:
        flags = ""
        bit = 1
        for i in range(1, 9):
            if sol_type & bit:
                if flags:
                    flags += ' | '
                flags += SOL_FLAGS[i]
            bit <<= 1
    print("Solenoid %d - type: %30s, init kick: %3d, min off duty: 0x%02x, kick pwm: %3d, input bits: 0x%08x" % 
        (sol, flags, init_kick, min_off_duty, kick_pwm, input_bits))

def printInpCfg(inp, cfgdata):
    inp_type = cfgdata[0]
    assert inp_type < len(INP_TYPES)
    debounce = cfgdata[1] << 8 | cfgdata[2]
    print("Input %d - type: %s, debounce: %d" % 
        (inp, INP_TYPES[inp_type], debounce))

def printNeoCfg(cfgData):
    neo_type = cfgData[0]
    bytes_per_pixel = cfgData[1]
    num_pixels = cfgData[2]
    init_colors = cfgData[3:]
    print("neo type: 0x%02x, bytes per pixel: %d, num pixels: %d, init colors: %s" %
        (neo_type, bytes_per_pixel, num_pixels, init_colors))

# rcv get wing config resp
def rcvGetWingCfgResp():
    data, data2 = getVarLenResp(GET_WING_CFG, 4)
    if not data:
        return 700
    index = data[0]
    # wing type
    wing_type = data[1]
    outStr = "W[%d]:" % index
    outStr += wingTypeStr(wing_type)
    print(outStr)
    # Config
    if wing_type == WING_SOL:
        assert data2[0] == 4 * 8 # 4 solenoids x 8 bytes each
        for i in range(4):
            offset = i * 8 + 1
            cfgdata = data2[offset:offset + 8]
            printSolCfg(i, cfgdata)
    elif wing_type == WING_SOL_8:
        assert data2[0] == 8 * 8 # 8 solenoids x 8 bytes each
        for i in range(8):
            offset = i * 8 + 1
            cfgdata = data2[offset:offset + 8]
            printSolCfg(i, cfgdata)
    elif wing_type == WING_INP:
        assert data2[0] == 8 * 3 # 8 inputs x 3 bytes each
        for i in range(8):
            offset = i * 3 + 1
            cfgdata = data2[offset:offset + 3]
            printInpCfg(i, cfgdata)
    elif wing_type in (WING_NEO, WING_NEO_SOL):
        assert data2[0] == 7
        cfgdata = data2[1:]
        printNeoCfg(cfgdata)
    
    return 0


# send ext kick solenoids (0x27)
def sendExtKickSolCmd(sols, mask, quiet=False):
    if not quiet:
        print("Sending ext kick solenoid cmd (EXT_KICK_SOL_CMD - 0x27)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(EXT_KICK_SOL)
    sols &= 0xffffffff
    mask &= 0xffffffff
    cmdArr.append(sols >> 24)
    cmdArr.append((sols >> 16) & 0xff)
    cmdArr.append((sols >> 8) & 0xff)
    cmdArr.append(sols & 0xff)
    cmdArr.append(mask >> 24)
    cmdArr.append((mask >> 16) & 0xff)
    cmdArr.append((mask >> 8) & 0xff)
    cmdArr.append(mask & 0xff)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send fade cmd (0x40)
def sendFadeCmd(fade_type, offset, nbytes, duration, data, quiet=False):
    if not quiet:
        print("Sending fade cmd (FADE - 0x40)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(FADE)
    offset |= fade_type << 12
    cmdArr.append(offset >> 8)
    cmdArr.append(offset & 0xff)
    cmdArr.append(nbytes >> 8)
    cmdArr.append(nbytes & 0xff)
    cmdArr.append(duration >> 8)
    cmdArr.append(duration & 0xff)
    cmdArr.extend(data)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0



# send get sol state cmd (0xe0)
def sendGetSolStateCmd(sol, quiet=False):
    if not quiet:
        print("Sending get sol state cmd (GET_SOL_STATE - 0xe0)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(GET_SOL_STATE)
    sol &= 0x0f
    cmdArr.append(sol)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# rcv get sol state resp
def rcvGetSolStateResp():
    data = getResp(GET_SOL_STATE, 2)
    if not data:
        return 700
    sol = data[0]
    solState = data[1]
    solStateStr = SOL_STATES[solState]
    print("SolState for solneoid %d is %s" % (sol, solStateStr))
    return 0


# send get counters cmd (0xe1)
def sendGetCountersCmd(quiet=False):
    if not quiet:
        print("Sending get counters cmd (GET_COUNTERS - 0xe1)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(GET_COUNTERS)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# rcv get counters resp
def rcvGetCountersResp():
    data = getResp(GET_COUNTERS, 6)
    if not data:
        return 700
    crcErr = (data[0] << 8) + data[1]
    badCmd = (data[2] << 8) + data[3]
    badCmd = (data[2] << 8) + data[3]
    timeOut = (data[4] << 8) + data[5]
    print("crcErr: %d\nbadCmd: %d\ntimeOut: %d" % (crcErr, badCmd, timeOut))
    return 0


# send inventory cmd (0xf0)
def sendInvCmd(quiet=False):
    if not quiet:
        print("Sending inventory cmd (INVENTORY - 0xf0)")
    cmdArr = []
    cmdArr.append(INV_CMD)
    cmdArr.append(EOM_CMD)
    ser.write(cmdArr)


# rcv inventory resp
def rcvInvResp(quiet=False):
    global gen2Addr
    data = getSerialData()
    if not data:
        return 700
    #First byte should be inventory cmd
    index = 1
    if (data[0] != INV_CMD):
        print(data)
        return 101
    if (len(data) < index + 1):
        print("Could not find EOM.")
        return 102
    while (data[index] != EOM_CMD):
        if ((data[index] & CARD_ID_TYPE_MASK) == CARD_ID_GEN2_CARD):
            gen2Addr = data[index]
            currWingCfg.append(0)
            hasMatrix = False
            matrixInpData = [0,0,0,0,0,0,0,0]
        index = index + 1
        if (len(data) < index + 1):
            print("Could not find EOM.")
            return 103
    if not quiet: print("Found a Gen2 board (%s) on port %s" % (hex(gen2Addr), args.port))
    return 0


# send bad cmd (0xfe)
def sendBadCmd(quiet=False):
    if not quiet:
        print("Sending bad cmd (0xfe)")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(0xfe)
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr)
    return 0


# send get sol state cmd (0xe0) with time out
def sendTimeOutCmd(quiet=False):
    if not quiet:
        print("Sending get sol state cmd (GET_SOL_STATE - 0xe0) with time out")
    cmdArr = []
    cmdArr.append(gen2Addr)
    cmdArr.append(GET_SOL_STATE)
    cmdArr.append(0) # sol 0
    cmdArr.append(calcCrc8(cmdArr))
    cmdArr.append(EOM_CMD)
    if debug: dump(cmdArr)
    ser.write(cmdArr[0:-2])
    time.sleep(0.1)
    ser.write(cmdArr[-2:])
    return 0




def test1(nloops):
    print("Press Ctrl+C to end the test or wait %d loops." % nloops)
    bad = False
    for i in range(nloops):
        try:
            sendInvCmd()
            data = getSerialData()
            if not data:
                break;
            if data[0] != INV_CMD:
                print("Bad resp, index = %d, data = %d" % (0, data[0]))
                break;
            if data[1] != 0x20:
                print(repr(data))
                print("Bad resp, index = %d, data = %d" % (1, data[1]))
                break;
            if data[2] != EOM_CMD:
                print(repr(data))
                print("Bad resp, index = %d, data = %d" % (2, data[2]))
                break;
            if bad:
                break;
        except KeyboardInterrupt:
            break
    print("%d loops done" % i)


def test2(nloops):
    print("Press Ctrl+C to end the test.")
    count = 0
    for i in range(nloops):
        try:
            sendReadInpBrdCmd(True)
            rc = rcvReadInpResp()
            if rc:
                print("\nCount = %d" % count)
                proto_error("sendReadInpBrdCmd", rc)
                break
            if hasMatrix:
                sendReadMatrixCmd(True)
                rc = rcvReadMatrixResp()
                if rc:
                    print("\nCount = %d" % count)
                    proto_error("sendReadMatrixCmd", rc)
                    break
            count = count + 1
        except KeyboardInterrupt:
            break
    print("%d loops done" % count)


def assertWing(val): 
    try:
        wing = int(val)
    except ValueError:
        print("wing is integer")
        return -1
    if wing < 0 or wing > 3:
        print("wing is between 0 and 3")
        return -1
    return wing


def assertSol(val): 
    try:
        sol = int(val)
    except ValueError:
        print("solenoid is integer")
        return -1
    if sol < 0 or sol > 15:
        print("solenoid is between 0 and 15")
        return -1
    return sol


def assertExtSol(val): 
    try:
        sol = int(val)
    except ValueError:
        print("solenoid is integer")
        return -1
    if sol < 0 or sol > 31:
        print("solenoid is between 0 and 31")
        return -1
    return sol



def assertSwitch(val): 
    try:
        switch = int(val)
    except ValueError:
        print("switch is integer")
        return -1
    if switch < 0 or switch > 31:
        print("switch is between 0 and 31")
        return -1
    return switch


def assertHexa(hexVal, name):
    try:
        val = int(hexVal, 16)
    except ValueError:
        print("%s must be hexadecimal" % name)
        return -1
    return val


def assertInt(intVal, min, max, name):
    try:
        val = int(intVal)
    except ValueError:
        print("%s must be integer" % name)
        return None
    if val < min or val > max:
        print("%s must be between %d and %d" % (name, min, max))
        return None
    return val


def assertFloat(floatVal, min, max, name):
    try:
        val = float(floatVal)
    except ValueError:
        print("%s must be int or float" % name)
        return None
    if val < min or val > max:
        print("%s must be between %d and %d" % (name, min, max))
        return None
    return val



def decodeSolFlags(flags):
    """ decode solenoid flags separated by +
    """ 
    nflags = 0
    flags = flags.split('+')
    for flag in flags:
        if flag not in SOL_FLAGS:
            print("Unrecognized solenoid flag: %s" % flag)
            return -1
        i = SOL_FLAGS.index(flag)
        if i:
            nflags += 1 << (i - 1)
    # check invalid combinations
    if "DISABLE" in flags and len(flags) != 1:
        print("DISABLE cannot be used with another flag")
        return -1
    return nflags



# high level action functions
# ---------------------------


def find_card(sernum, quiet=True):
    """ open the serial port corresponding to a card serial number.
        return the port name if found or None
    """
    global ser
    found = False
    for file in glob(args.port + '*'):
        statinfo = stat(file)
        if statinfo.st_rdev != 0:
            # timeout should be 0.2 if using a debugger, or 0.1 if not.
            if not quiet: print("Trying port %s" % file)
            open_serial(file)
            if (ser):
                sendInvCmd(True)
                rc = rcvInvResp(True)
                if rc: 
                    ser.close()
                    continue
                # Collect serial number
                sendGetSerNumCmd(True)
                rc = rcvGetSerNumResp(True)
                if rc:
                    ser.close()
                    continue
                if cardSerNum == sernum:
                    return file
                # close before moving to next port
                ser.close()
    return None


def do_config(config):
    global cfgFile
    sendWingCfg = True
    sendInpTable = True
    sendSolCfg = True
    sendSolExtCfg = True
    sendNeoCfg = True
    print("Config file = %s" % config)
    name, ext = splitext(config)
    if ext == '':
        print("Config file must be a .py file")
        return -1
    #Make test num invalid
    testNum = 255
    # import the module relative to the given path
    spec = importlib.util.spec_from_file_location("cfg", config)
    # creates a new module based on spec
    cfgFile = importlib.util.module_from_spec(spec)
    # executes the module in its own namespace when a module is imported or reloaded.
    try:
      spec.loader.exec_module(cfgFile)
    except FileNotFoundError:
        print("Not found")
        return -1
    # WingCfg
    try:
        cfgFile.wingCfg
    except AttributeError:
        sendWingCfg = False
    if sendWingCfg:
        # Send wing config for Gen2 board
        sendWingCfgCmd(args.quiet)
        rc = rcvAckNackResp()
        if rc:
            proto_error("sendWingCfgCmd", rc)
    else:
        print("Skipping sending wing cfg.")
    # InpCfg
    try:
        cfgFile.inpCfg
    except AttributeError:
        sendInpTable = False
    if sendInpTable:
        # we have an input wing, configure input
        sendInpCfgCmd(args.quiet)
        rc = rcvAckNackResp()
        if rc:
            proto_error("sendInpCfgCmd", rc)
    else:
        print("Skipping sending input cfg.")
    # SolCfg
    try:
        cfgFile.solCfg
    except AttributeError:
        sendSolCfg = False
    if sendSolCfg:
        # we have a solenoid wing, configure solenoid
        sendSolCfgCmd(args.quiet)
        rc = rcvAckNackResp()
        if rc:
            proto_error("sendSolCfgCmd", rc)
    else:
        print("Skipping sending solenoid cfg.")
    # NeoCfg
    try:
        cfgFile.neoCfg
    except AttributeError:
        sendNeoCfg = False
    if sendNeoCfg:
        if len(cfgFile.neoCfg) != 7:
            print("Invalid neoCfg")
            return -1
        sendNeoCfgCmd(args.quiet)
        rc = rcvAckNackResp()
        if rc:
            proto_error("sendNeoCfgCmd", rc)
    else:
        print("Skipping sending neopixel cfg.")
    # Collect Wing conf
    sendReadWingCfgCmd(args.quiet)
    rc = rcvReadWingCfgResp()
    if rc:
        proto_error("sendReadWingCfgCmd", rc)
    if not error:
        print("Do not forget to save your config in flash")
    return 0


def do_confsol(sol, flags, init, prm):         
    sol = assertExtSol(sol)
    if sol < 0:
        return -1
    nflags = decodeSolFlags(flags)
    if nflags < 0:
        return -1
    # init kick delay
    init_kick = assertInt(init, 0, 255, "init kick")
    if not init_kick:
        return -1
    # param
    prm = assertHexa(prm, "parameter")
    if prm < 0:
        return -1
    prm &= 0xff
    sendConfIndSol(sol, nflags, init_kick, prm, args.quiet)
    return 0


def do_extconfsol(sol, flags, init, prm, pwm, inputs):         
    sol = assertExtSol(sol)
    if sol < 0:
        return -1
    nflags = decodeSolFlags(flags)
    if nflags < 0:
        return -1
    # init kick delay
    init_kick = assertInt(init, 0, 255, "init kick")
    if not init_kick:
        return -1
    # param
    prm = assertHexa(prm, "parameter")
    if prm < 0:
        exit(1)
    inputs = assertHexa(inputs, "inputs")
    if inputs < 0:
        return -1
    pwm = assertIntint(pwm, 0, 31, "kick pwm")
    if not pwm:
        return -1
    sendExtCfgIndSol(sol, nflags, init_kick, prm, pwm, inputs, args.quiet)
    return 0


def do_confsolinput(sol, action, switch):
    sol = assertExtSol(sol)
    if sol < 0:
        return -1
    if action not in ("SET", "RESET"):
        print("action is SET or RESET")
        return -1
    if action == "RESET":
        sol |= 0x80
    # switch
    switch = assertSwitch(switch)
    if switch < 0:
        return -1
    sendConfSolInput(sol, switch, args.quiet) 
    return 0


def do_confsolpower(sol, pwm): 
    sol = assertExtSol(sol)
    if sol < 0:
        return -1
    try:
        pwm = int(pwm)
    except ValueError:
        print("power is integer")
        return -1
    if pwm < 0 or pwm > 31:
        print("power is between 0 and 31")
        return -1
    sendConfSolPower(sol, pwm, args.quiet)
    return 0


def do_confinput(switch, inptype, debounce):
    # switch
    switch = assertSwitch(switch)
    if switch < 0:
        return -1
    if inptype not in INP_TYPES:
        print("action is " + " or ".join(INP_TYPES))
        return -1
    try:
        debounce = int(debounce)
    except ValueError:
        print("debounce is a short integer")
        return -1
    sendConfInput(switch, INP_TYPES.index(inptype), debounce, args.quiet) 
    return 0


def do_save():
    sendSaveCfgCmd(args.quiet)
    return 0


def do_input():        
    sendReadInpBrdCmd(args.quiet)
    rc = rcvReadInpResp()
    if rc:
        proto_error("sendReadInpBrdCmd", rc)
        return -1
    return 0


def do_inputtime():    
    sendGetInpTsCmd(args.quiet)
    rc = rcvGetInpTsResp()
    if rc:
        proto_error("sendGetInpTsCmd", rc)
        return -1
    return 0

    
def do_kick(sol, action):
    sol = assertSol(sol)
    if sol < 0:
        return -1
    mask = 1 << sol
    if action not in ("ON", "OFF"):
        print("Kick is either ON or OFF")
        return -1
    # sol must be expressed a bit array (here one bit)
    if action == "ON":
        sol =  1 << sol
    else:
        sol = 0
    sendKickSolCmd(sol, mask, args.quiet)
    return 0


def do_extkick(sol, action):
    sol = assertExtSol(sol)
    if sol < 0:
        return -1
    mask = 1 << sol
    if action not in ("ON", "OFF"):
        print("Kick is either ON or OFF")
        return -1
    # sol must be expressed a bit array (here one bit)
    if action == "ON":
        sol =  1 << sol
    else:
        sol = 0
    sendExtKickSolCmd(sol, mask, args.quiet)
    return 0


def do_led(flags, amask, bmask, cmask, dmask):
    nflags = nbflags = nsubflags = 0
    flags = flags.split('+')
    for flag in flags:
        if flag not in INCAND_FLAGS and flag not in INCAND_SET_FLAGS:
            print("Unrecognized incandescent (sub)command: %s" % flag)
            return 1
    for flag in flags:
        if flag in INCAND_FLAGS:
            nbflags += 1
            if flag == "SET":
                nflags = 0x80
            else:
                nflags = INCAND_FLAGS.index(flag)
    if nbflags > 1:
        print("Only one incand command at a time")
        return 1
    for flag in flags:
        if flag in INCAND_SET_FLAGS:
            if nflags != 0x80:
                print("cannot mix sub command and other command")
                exit(1)
            i = INCAND_SET_FLAGS.index(flag)
            nsubflags += 1 << i
    nflags += nsubflags
    a = assertHexa(amask, "port A mask")
    if a < 0:
        return 1
    b = assertHexa(bmask, "port B mask")
    if b < 0:
        return 1
    c = assertHexa(cmask, "port C mask")
    if c < 0:
        return 1
    d = assertHexa(dmask, "port D mask")
    if d < 0:
        return 1
    #print("flags 0x{:x}".format(nflags))
    sendIncandCmd(nflags, a, b, c, d, args.quiet)
    return 0

    
def do_getwingconf(wing):
    wing = assertWing(wing)
    if wing < 0:
        return -1
    # Collect wing config
    sendGetWingCfgCmd(wing, args.quiet)
    rc = rcvGetWingCfgResp()
    if rc:
        proto_error("sendGetGetWingCfgCmd", rc)
        return -1
    return 0


def do_getsolstate(sol):
    sol = assertExtSol(sol)
    if sol < 0:
        return -1
    # Collect sol state
    sendGetSolStateCmd(sol, args.quiet)
    rc = rcvGetSolStateResp()
    if rc:
        proto_error("rcvGetSolStateResp", rc)
        return -1
    return 0        


def do_fade(prms):
    if len(prms) < 5:
        print("requires minimum 5 parameters")
        return -1
    try:
        fade_type = FADE_TYPES.index(prms[0])
    except ValueError:
        print("invalid fade type")
        return -1
    try:
        offset = int(prms[1])
    except ValueError:
        print("invalid offset")
        return -1
    if offset < 0 or offset > 256:
        print("invalid offset")
        return -1
    try:
        nbytes = int(prms[2])
    except ValueError:
        print("invalid number of bytes")
        return -1
    if nbytes < 0:
        print("invalid number of bytes")
        return -1
    # check that number of args is OK
    if len(prms) != nbytes + 4:
        print("inconsistant number of argmuments vs. number of bytes")
        return -1
    try:
        duration = int(prms[3])
    except ValueError:
        print("invalid fade duration")
        return -1
    if duration < 0 or duration > 0xffff:
        print("invalid fade duration")
        return -1
    # check data
    data = list()
    for b in prms[4:]:
        try:
            bi = int(b)
        except ValueError:
            print("invalid data (not integer)")
            return -1
        if bi < 0 or bi > 255:
            print("invalid data (out of range)")
            return -1
        data.append(bi)
    # send command
    sendFadeCmd(fade_type, offset, nbytes, duration, data, args.quiet)
    return 0


def do_getcounters():
    sendGetCountersCmd(args.quiet)
    rc = rcvGetCountersResp()
    if rc:
        proto_error("rcvGetCountersResp", rc)
        return -1
    return 0
    

def do_crcerr():
    sendCrcErrCmd(args.quiet)
    return 0


def do_badcmd():
    sendBadCmd(args.quiet)
    return 0


def do_timeout():
    sendTimeOutCmd(args.quiet)
    return 0

def do_reset():
    sendResetCmd(args.quiet)
    return 0



def do_test(test, loops):
    try:
        test = int(test)
    except ValueError:
        test = 0
    try:
        loops = int(loops)
    except ValueError:
        loops = 10
    if test == 1:
        test1(loops)
    elif test == 2:
        test2(loops)
    return 0


def eval_repeat_cmd(s):
    """ These are commands that may be repeated"""
    cmd = s[0]
    match cmd:
        case 'SLEEP':
            if len(s) != 2:
                print("requires 1 args")
            else:
                f = assertFloat(s[1], 0.01, 600.0, "delay")
                if f is None:
                    return False
                sleep(f)
                return True
        case 'INPUT':
            do_input()
            return True
        case 'INPUTTIME':
            do_inputtime()
        case 'KICK':
            if len(s) != 3:
                print("requires 2 args")
            else:
                do_kick(s[1], s[2])
                return True
        case 'EXTKICK':
            if len(s) != 3:
                print("requires 2 args")
            else:
                do_extkick(s[1], s[2])
                return True
        case 'LED':
            if len(s) != 6:
                print("requires 5 args")
            else:
                do_led(s[1], s[2], s[3], s[4], s[5])
                return True
        case 'GETSOLSTATE':
            if len(s) != 2:
                print("requires 1 arg")
            else:
                do_getsolstate(s[1])
                return True
        case 'GETCOUNTERS':
            do_getcounters()
            return True
        case 'CRCERR':
            do_crcerr()
            return True
        case 'BADCMD':
            do_badcmd()
            return True
        case 'TIMEOUT':
            do_timeout()
            return True
    return False


def enter_shell():
    # command history
    opp_dir = expanduser('~/.local/share/opp')
    hist_file = 'history'
    try:
        makedirs(opp_dir, mode=0o755, exist_ok=True)
        try:
            read_history_file(opp_dir + sep + hist_file)
        except FileNotFoundError:
            pass
    except OSError as e:
        print (e)
    set_history_length(100)
    run = True
    s_last = [] # last repeatable command
    while run:
        try:
            s = input('#%d> ' % cardSerNum).strip().upper().split()
        except EOFError:
            run = False
            print()
            s = []
        if len(s) > 0:
            cmd = s[0]
            match cmd:
                case 'HELP':
                    print("""
    You may use most -- options. For instance to kick solenoid 0, type:
    KICK 0 ON
                            
    Extra commands are:
    QUIT: stop shell
    REPEAT <loops> <delay in s>: repeat the last command <loops> times, making a pause of <delay> in between.
""")
                case 'QUIT':
                    run = False
                case 'VERSION':
                    print(__version__)
                case 'CONFIG':
                    if len(s) != 2:
                        print("requires 1 arg")
                    else:
                        do_config(s[1])
                case 'CONFSOL':
                    if len(s) != 5:
                        print("requires 4 args")
                    else:
                        do_confsol(s[1], s[2], s[3], s[4])
                case 'EXTCONFSOL':
                    if len(s) != 7:
                        print("requires 6 args")
                    else:
                        do_extconfsol(s[1], s[2], s[3], s[4], s[5], s[6])
                case 'CONFSOLINPUT':
                    if len(s) != 4:
                        print("requires 3 args")
                    else:
                        do_confsolinput(s[1], s[2], s[3])
                case 'CONFSOLPOWER':
                    if len(s) != 3:
                        print("requires 2 args")
                    else:
                        do_confsolpower(s[1], s[2])
                case 'CONFINPUT':
                    if len(s) != 4:
                        print("requires 3 args")
                    else:
                        do_confinput(s[1], s[2], s[3])
                case 'SAVE':
                    do_save()
                case 'GETWINGCONF':
                    if len(s) != 2:
                        print("requires 1 arg")
                    else:
                        do_getwingconf(s[1])
                case 'FADE':
                        do_fade(s[1:])
                case 'TEST':
                    if len(s) != 3:
                        print("requires 2 arg")
                    else:
                        do_test(s[1], s[2])
                case 'REPEAT':
                    if len(s) != 3:
                        print("requires 2 arg")
                    elif s_last:
                        try:
                            loops = int(s[1])
                            wait = float(s[2])
                        except ValueError:
                            loops = 1
                            wait = 1.0
                        if loops <= 0 or wait <= 0:
                            loops = 1
                            wait = 1.0
                        print("Press Ctrl+C to abort")
                        try:
                            for i in range(loops):
                               eval_repeat_cmd(s_last)
                               time.sleep(wait)
                        except KeyboardInterrupt:
                            pass
                case _:
                    if not eval_repeat_cmd(s):
                        print("Invalid command or bad arguments")
                    else:
                        # this was a repeatable command
                        s_last = s.copy()
    # command history
    write_history_file(opp_dir + sep + hist_file)


def do_batch(bfile):
    trace = False if args.quiet else True
    try:
        batch = load(bfile.read(), Loader=Loader)
    except YAMLError:
        print("Error loading YAML file")
        return False
    # print(batch)
    if 'name' in batch:
        if trace: print("Batch %s\n" % batch['name'])
    if 'batches' not in batch:
        print("Missing batches entry")
        return False
    if not isinstance(batch['batches'], dict):
        print("Wrong batches format")
        return False
    for bkey, bdata in batch['batches'].items():
        if 'repeat' in bdata:
            loop = assertInt(bdata['repeat'], 0, 10000, "repeat")
            if loop is None:
                return False
            loop += 1
        else:
            loop = 1
        for i in range(loop):
            if trace: print("%s" % bkey)
            if 'steps' not in bdata:
                print("Missing steps entry in %s batch" % bkey)
                return False
            if not isinstance(bdata['steps'], list):
                print("Wrong format for batch %s" % bkey)
                return False
            for s in bdata['steps']:
                if not isinstance(s, dict):
                    print("Wrong format for steps in %s batch", bkey)
                    return False
                if trace: print("\t%s" % s.get('comment', ""))
                if 'cmd' not in s:
                    print("Missing cmd entry in a step for %s batch" % bkey)
                    return False
                cmd = s['cmd'].upper().split()
                if not eval_repeat_cmd(cmd):
                    print("Bad command: %s" % s['cmd'])
                    return False
    return True


# Main code

parser = argparse.ArgumentParser(description='OPP test')
parser.add_argument('--version', action='version', version=__version__)
parser.add_argument('-p', '--port', default='/dev/ttyACM0', help='Serial port, defaults to /dev/ttyACM0')
parser.add_argument('-d', '--debug', action='store_true', default=False, help='Debug serial port')
parser.add_argument('-q', '--quiet', action='store_true', help='Do not print commands sent')
parser.add_argument('-n', '--noquery', action='store_true', default=False, help='Do not query device before processing command')
parser.add_argument('-s', '--serial', nargs=1, type=int, help='program the serial number')
parser.add_argument('--getserial', action='store_true', help='get the serial number or NONE if not set')
parser.add_argument('-c', '--config', nargs='?', const='defaultcfg.py', help='save a cfg on board from given python file (if not supplied the defaultcfg.py is used)')
parser.add_argument('--confsol', nargs=4, help='Configure a single solenoid (0-31) followed by 3 parameters: flags (see CFG_SOL in rs232intf.py), init  kick time, parameter (1 byte in hex)')
parser.add_argument('--extconfsol', nargs=6, help='Extended configure a single solenoid (0-31) followed by 5 parameters: flags (see CFG_SOL in rs232intf.py), init  kick time, parameter (1 byte in hex), kick PWM (1 byte in hex), input (4 bytes in hex)')
parser.add_argument('--confsolinput', nargs=3, help='Configure a solenoid (0-31) SET|RESET input (0-31)')
parser.add_argument('--confsolpower', nargs=2, help='Configure a solenoid (0-31) power (0-31)')
parser.add_argument('--confinput', nargs=3, help='Configure an input (0-31) STATE_INPUT|FALL_EDGE|RISE_EDGE debounce_time|0')
parser.add_argument('--save', action='store_true', help='save the current board config to fllash')
parser.add_argument('-e', '--erase', action='store_true', default=False, help='Erase a cfg on board')
parser.add_argument('-i', '--input', action='store_true', help='Read inputs')
parser.add_argument('--inputtime', action='store_true', help='Read inputs time')
parser.add_argument('-k', '--kick', nargs=2, help='Kick one solenoid (0-15), value (ON|OFF)')
parser.add_argument('--extkick', nargs=2, help='Kick one solenoid (0-31), value (ON|OFF)')
parser.add_argument('-l', '--led', nargs=5, help='Led command (see INCAND cmds in rs322intf.py), followed by 4 hexa byte masks (port A, B, C, D)')
parser.add_argument('--getwingconf', nargs=1, type=int, help='Get wing configuration (0-3)')
parser.add_argument('--getsolstate', nargs=1, type=int, help='Get solenoid state (0-31)')
parser.add_argument('--fade', nargs='+', help='Fade NEO|NEO2|INCAND|MATRIX|SERVO offset byes time data...')
parser.add_argument('--getcounters', action='store_true', help='Get error counters')
parser.add_argument('--crcerr', action='store_true', help='Fake a crc error in commnand')
parser.add_argument('--badcmd', action='store_true', help='Fake a bad commnand')
parser.add_argument('--timeout', action='store_true', help='Fake a timeout while sending commnand')
parser.add_argument('-t', '--test', nargs=2, type=int, help='run a test (from 1 to 2) N times')
parser.add_argument('--shell', action='store_true', help='Run in shell mode')
parser.add_argument('-f', '--find', nargs=1, type=int, help='Find a card by serial number (-p option is dev prefix like /dev/ttyACM)')
parser.add_argument('--reset', action='store_true', help='Reset card')
parser.add_argument('-b', '--batch', type=argparse.FileType('r'), help='Run a batch of commands')


args = parser.parse_args()


if args.debug:
    debug = True

if args.noquery and args.serial:
    print("must query first to program serial number")
    exit(1)

if args.find and args.port == '/dev/ttyACM0':
    args.port = '/dev/ttyACM'

# commands that do not require a connection
if args.find:
    args.port = find_card(args.find[0])
    if not args.port:
        print("Could not find card #%d" % args.find[0])
        exit(1)
    ser.close()
    print(args.port)
    exit(0)
        

open_serial(args.port)
if not ser:
    print("\nCould not open " + args.port)
    sys.exit(1)


if not args.noquery:
    sendInvCmd(args.quiet)
    rc = rcvInvResp()
    if rc: 
        proto_error("sendInvCmd", rc)
    # Collect serial number
    sendGetSerNumCmd(args.quiet)
    rc = rcvGetSerNumResp()
    if rc: 
        proto_error("sendGetSerNumCmd", rc)
    # Collect firmware version
    sendGetVersCmd(args.quiet)
    rc = rcvGetVersResp()
    if rc: 
        proto_error("sendGetVersCmd", rc)
    # Collect Wing conf
    sendReadWingCfgCmd(args.quiet)
    rc = rcvReadWingCfgResp()
    if rc: 
        proto_error("sendReadWingCfgCmd", rc)
    print()

# Actual command processing starts here

error = False

if args.shell:    
    args.quiet = True
    enter_shell()

elif args.batch:
    try:
        from yaml import load, Loader, YAMLError
    except ModuleNotFoundError:
        print("This option requires the yaml Python module installed")
        ser.close()
        exit(1)
    if not do_batch(args.batch):
        error = True

elif args.config:
    if do_config(args.config) < 0:
        error = True

elif args.confsol:
    # Configure a single solenoid: sol# flags init_kick_time(ms) param(byte)
    # eg. --confsol 0 USE_SWITCH+DLY_KICK 100 04
    if do_confsol(args.confsol[0], args.confsol[1], args.confsol[2], args.confsol[3]) < 0:
        error = True

elif args.extconfsol:
    # extended configure a single solenoid: sol# flags init_kick_time(ms) param(byte) kick_pwm(byte) inputs(4 bytes) 
    # eg. --confsol 1 AUTO_CLR 100 04 FF 00020000
    if do_extconfsol(args.extconfsol[0], args.extconfsol[1], args.extconfsol[2], args.extconfsol[3], args.extconfsol[4], args.extconfsol[5]) < 0:
        error = True

elif args.confsolinput:
    # Configure a solenoid input: sol# SET|RESET input#
    # eg. --confsolinput 0 0
    if do_confsolinput(args.confsolinput[0], args.confsolinput[1], args.confsolinput[2]) < 0:
        error = True

elif args.confsolpower:
    # Configure a solenoid power: sol# pwm
    # eg. --confsolpower 0 15
    if do_confsolpower(args.confsolpower[0], args.confsolpower[1]) < 0:
        error = True

elif args.confinput:
    # Configure an input: input# STATE_INPUT|FALL_EDGE|RISE_EDGE debounce_time
    # eg. --confinput 8 FALL_EDGE 0
    if do_confinput(args.confinput[0], args.confinput[1], args.confinput[2]) < 0:
        error = True

elif args.save:
    # Save conf in flash
    if do_save() < 0:
        error = True

elif args.erase:
    #Make test num invalid
    testNum = 255
    #Erase config for Gen2 board
    sendEraseCmd(args.quiet)
    # Collect Wing conf
    sendReadWingCfgCmd(args.quiet)
    rc = rcvReadWingCfgResp()
    if rc: 
        proto_error("sendReadWingCfgCmd", rc)

elif args.serial:
    if cardSerNum != 0xffffffff:
        print("Serial number is already programmed")
        exit(1)
    serial = args.serial[0]
    if serial < 0 or serial > 0xfffffffe:
        print("serial is between 0 and 4294967294")
    else:
        print("Programming serial number")
        print("serNum = %d (%x)" % (serial, serial))
        sendSetSerNumCmd(serial, args.quiet)
        rcvAckNackResp()
        sendGetSerNumCmd(args.quiet)
        rc = rcvGetSerNumResp()
        if rc:
            proto_error("sendGetSerNumCmd", rc)
            error = True

elif args.getserial:
    if not cardSerNum:
        # Collect serial number
        sendGetSerNumCmd(True)
        rc = rcvGetSerNumResp(True)
        if rc:
            proto_error("sendGetSerNumCmd", rc)
            error = True
    print("NONE" if cardSerNum is None else cardSerNum)

elif args.input:
    do_input()

elif args.inputtime:
    do_inputtime()

elif args.kick:
    if do_kick(args.kick[0], args.kick[1]) < 0:
        error = True

elif args.extkick:
    if do_extkick(args.extkick[0], args.extkick[1]) < 0:
        error = True

elif args.led:
    # eg. --led SET+SET_ON+SET_BLINK_FAST 00 00 00 FF
    if do_led(args.led[0], args.led[1], args.led[2], args.led[3], args.led[4]) < 0:
        error = True

elif args.getwingconf:
    if do_getwingconf(args.getwingconf[0]) < 0:
        error = True

elif args.getsolstate:
    if do_getsolstate(args.getsolstate[0]) < 0:
        error = True

elif args.fade:
    if do_fade(args.fade) <0:
        error = True

elif args.getcounters:
    # Collect counters
    if do_getcounters() < 0:
        error = True

elif args.badcmd:
    if do_badcmd() < 0:
        error = True

elif args.crcerr:
    if do_crcerr() < 0:
        error = True

elif args.timeout:
    if do_timeout() < 0:
        error = True

elif args.test:
    if do_test(args.test[0], args.test[1]) < 0:
        error = True

elif args.reset:
    do_reset()


ser.close()
exit(1 if error else 0)
