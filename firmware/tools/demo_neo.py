#!/usr/bin/env python
#

from subprocess import run
from time import sleep

LEDS = 8 # number of LEDs in string
GRB = True # ws2812 chip
FADE_TYPE = 'NEO2'

RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

REDS = (RED,) * LEDS
GREENS = (GREEN,) * LEDS
BLUES = (BLUE,) * LEDS
WHITES = (WHITE,) * LEDS
BLACKS = (BLACK,) * LEDS

def make_rgb(power, led):
    assert power >= 0 and power <= 1
    r, g, b = led
    r = int(r * power)
    g = int(g * power)
    b = int(b * power)
    if GRB:
        return ' ' + str(g) + ' ' + str(r) + ' ' + str(b)
    else:
        return ' ' + str(r) + ' ' + str(g) + ' ' + str(b)


def make_data(power, leds):
    data = ''
    for led in leds:
        data += make_rgb(power, led)
    return data

def make_args(power, ftime, offset, leds):
    assert offset + len(leds) <= LEDS
    assert ftime >= 0 and ftime < 20000
    args = '-q -n --fade ' + FADE_TYPE + ' ' + str(offset * 3) + ' ' + str(len(leds) * 3) + ' ' + str(ftime) + make_data(power, leds)
    return args

def make_cmd(power, ftime, offset, leds):
    cmd = './gen2test.py ' + make_args(power, ftime, offset, leds)
    return cmd    


def fade(power, ftime, offset, leds):
    cmd = make_cmd(power, ftime * 1000, offset, leds)
    print(cmd)
    run(cmd, shell=True)
    sleep(ftime)


while True:
    fade(0.5, 0, 0, BLACKS)
    # shift right
    for i in range(LEDS):
        fade(0.5, 0, i, (WHITE,))
        sleep(0.1)
        fade(0.5, 0, i, (BLACK,))
    # shift left        
    for i in range(LEDS - 1, -1, -1):
        fade(0.5, 0, i, (WHITE,))
        sleep(0.1)
        fade(0.5, 0, i, (BLACK,))
    sleep(1)
    # All LEDs from different colors
    fade(0.5, 1, 0, REDS)
    sleep(2)
    fade(0.5, 1, 0, BLUES)
    sleep(2)
    fade(0.5, 1, 0, GREENS)
    sleep(2)
    fade(0.5, 1, 0, BLACKS)
    sleep(2)
    fade(0.5, 1, 0, WHITES)
    sleep(2)
    fade(0.5, 1, 0, BLACKS)
    sleep(2)
    # Caroussel
    steps = 3
    mult = 255 // steps
    for r in range(steps):
        for g in range(steps):
            for b in range(steps):
                led = (r * mult, g * mult, b * mult)
                leds = (led,) * LEDS
                fade(0.5, 0, 0, leds)
                sleep(0.3)
