#!/usr/bin/env python
#

# Config OPP 32 outputs
# Wings 0-31: WING_INCAND (max 4)


from rs232intf import *

wingCfg = [ WING_INCAND, WING_INCAND, WING_INCAND, WING_INCAND ]

# To play with incansedcent lamps, wing 2 uses bits 6-7 (0xC0) and wing 3 uses bits 0-1 (0x03)
# Put on all configured LEDs
# ./gen2test.py --led LED_SET_ON_OFF 00 00 C0 03
# Put on first configured LED
# ./gen2test.py --led LED_SET_ON_OFF 00 00 40 00
# Rotate right through all configured LEDs
# ./gen2test.py --led ROT_RIGHT 00 00 C0 03
# Blink slow first and third configured LEDs
# ./gen2test.py --led SET+SET_BLINK_SLOW 00 00 40 01
