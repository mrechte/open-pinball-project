#!/usr/bin/env python
#

# Config OPP for a Neopixel board
# Wings 0: WING_NEO
# Wings 1 - 2: WING_UNUSED
# Wings 3: WING_NEO


from rs232intf import *

wingCfg = [ WING_NEO, WING_UNUSED, WING_UNUSED, WING_UNUSED ]
# wingCfg = [ WING_UNUSED, WING_UNUSED, WING_UNUSED, WING_NEO ]

# An GRB WS2812 based LED string (3 pixels), made of 8 LEDs, with initial default color (0xff) (HALF_TONE)
neoCfg = [ 0x00, 0x03, 0x08, 0xff, 0xff, 0xff, 0x00 ]
#neoCfg = [ 0x01, 0x03, 0x08, 0xff, 0xff, 0xff, 0x00 ]
