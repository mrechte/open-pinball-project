#!/usr/bin/env python
#

# Config OPP 16 outputs 16 inputs
# Wings 0, 1: WING_SOL_8
# Wings 2, 3: WING_INP

# For firmware >= 2.4.255.11

from rs232intf import *

wingCfg = [ WING_SOL_8, WING_SOL_8, WING_INP, WING_INP ]

# Config 32 inputs as all state inputs (2 entries per input)
inpCfg = [  CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,

            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0, 

            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,

            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0, 
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0,
            CFG_INP_STATE, 0]


# 5 entries per solenoid: type, initial kick time (0-255 ms), minimum off time & duty cycle, kick pwm (0-31), input switches
# All are AUTO_CLR with 100ms kick.
solCfg = [ CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,

           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,

           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,

           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000,
           CFG_SOL_AUTO_CLR, 100, 0x00, 31, 0x00000000 ]


