/**
 * @file:   fade.c
 * This is the file for fading NeoPixels, incandescent, lamp matrices and servo
 *
 * This file requires a 10ms tick to start the fade processing.
 *
 *===============================================================================
 */
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "stdtypes.h"

#include "fade.h"

#include "mcu.h"
#include "opp.h"

const U16 FADE_USEC_DUR[32] = {
   0,   63,  94,  125, 156, 188, 219, 250,
   281, 313, 344, 375, 406, 438, 469, 500,
   531, 563, 594, 625, 656, 688, 719, 750,
   781, 813, 844, 875, 906, 938, 969, 1000,
};

/*
 * The module can manage one of each of the following fade types. Do not change values are they are used in Fade command 0x40
 */
typedef enum {
	NEO_REC = 0,
	INCAND_REC,
	LAMP_MATRIX_REC,
	SERVO_REC,
	NEO_REC2,
	MAX_FADE_RECS
} FADE_REC_T;



typedef struct {
   U16              totTicks;
   U16              currTick;
} FADE_BYTE_INFO;


/*
 * This structure type maintains the information related to a fade type.
 */
typedef struct {
   BOOL              valid;
   INT               numDataBytes;        /* Number of dataBytes (pixels * bytesPerPixel) */
   U8                *currPxlVal_p;       /* Ptr to array of current pixel values, mallocated and constant */
   U8                *newPxlVal_p;        /* Ptr to array of future pixel values, mallocated and constant */
   FADE_BYTE_INFO    *fadeByte_p;         /* Ptr to array of pixel fade info, mallocated and constant */
   void              (*fadeProc_fp)(UINT currByteOffset, U8 newData);   /* Callback for fade fade value change */
   void              (*endFadeProc_fp)(); /* Callback to inform fade processing is complete */
} FADE_INFO_REC;


/*
 * This structure type is to maintain the whole module fade states for MAX_FADE_RECS systems.
 */
typedef struct {
   BOOL              tickOcc;
   BOOL              fadeRunning;		  // There is at least one of MAX_FADE_RECS fades running
   BOOL              fadeRcvCmdBadHeader; // Indicates an invalid command header received (hence data part will be ignored).
   INT               rcvOffset;            /* Offset, excluding high nibble (because we made already point rcvCmdFadeRec_p to the correct type),
    		for the current fade command to process */
   INT               rcvNumBytes;         /* Number of data bytes for the current fade command to process. This will be decremented
    	each time a data byte (part of the fade command) is received. */
   U16               rcvTotTicks;         /* TimeOfFade / 10 = total ticks received  for the current fade command to process */
   INT               totNumBytes;         // Total number of fadeBytes for the whole module
   INT               maxProcBytes;        /* Max num data bytes to process at a time */
   INT               currByteOffset;       /* Current fade byte offset or ILLEGAL_FADE_OFFSET */
   INT               totBytesProc;
   FADE_INFO_REC     *rcvCmdFadeRec_p;    /* Fade record being updated by rcvd cmd, this points to one of the fadeRec */
   FADE_INFO_REC     *currFadeRec_p;      /* Current fade record being processed */
   FADE_INFO_REC     fadeRec[MAX_FADE_RECS]; // One for each fade type (neo, incand, matrix and servo)
} FADE_INFO;


FADE_INFO fade_info;

/* Illegal current byte fade offset is used to indicate new fade record must be found */
#define ILLEGAL_FADE_OFFSET   0xffff



/*
 * ===============================================================================
 * 
 * Name: fade_init
 * 
 * ===============================================================================
 */
/**
 * Initialize fields in fade info.
 * 
 * @param   None 
 * @return  None
 * 
 * ===============================================================================
 */
void fade_init() {

	/* Init fadeInfo structure */
   memset(&fade_info, 0, sizeof(fade_info));
}

/*
 * ===============================================================================
 *
 * Name: fade_init_rec
 *
 * ===============================================================================
 */
/**
 * Initialize fade record
 *
 * Allocate memory for fade structures.  Mark record as valid.
 *
 * @param   startOffset is one of RS232I_FADE_NEO_OFFSET, RS232I_FADE_INCAND_OFFSET, ... constant. If >> 12, it is converted in FADE_REC_T !
 * @param	numFadeBytes number of bytes to malloc for currPxlVal and newPxlVal plus 4 x numFadeBytes for FADE_BYTE_INFO bytes. So it will malloc a total of 6 x numFadeBytes
 * @param	currPxlVal_pp the address of a pointer to allocated currPxlVal
 * @param	newPxlVal_pp the address of a pointer to allocated newPxlVal
 * @param	fadeProc_fp
 * @param	endFadeProc_fp the address of the function to call when fade is completed
 * @return  None
 *
 * ===============================================================================
 */
void fade_init_rec(
	INT  startOffset,
	INT  numFadeBytes,
	U8   **currPxlVal_pp,
	U8   **newPxlVal_pp,
	void (*fadeProc_fp)(UINT offset, U8 newData),
	void (*endFadeProc_fp)()) {

	U16 fadeRec;
	INT               index;
	FADE_BYTE_INFO *tmpFadeByte_p;
	FADE_INFO_REC *fadeRec_p;

	// we convert startOffset to FADE_REC_T: first 4 bits of offset is fade type (see 0x40 command)
	fadeRec = startOffset >> 12;
	assert(fadeRec < MAX_FADE_RECS);
	fadeRec_p = &fade_info.fadeRec[fadeRec];
	// deallocate first, then reset structure
	if (fadeRec_p->currPxlVal_p != NULL) {
		free(fadeRec_p->currPxlVal_p);
		if (fadeRec_p->newPxlVal_p != NULL)
			free(fadeRec_p->newPxlVal_p);
		if (fadeRec_p->fadeByte_p != NULL)
			free(fadeRec_p->fadeByte_p);
		fade_info.totNumBytes -= numFadeBytes;
	}
	// clear struct
	memset(fadeRec_p, 0, sizeof(FADE_INFO_REC));

	fadeRec_p->numDataBytes = numFadeBytes;
	fade_info.totNumBytes += numFadeBytes;

	// allocate buffer for current pixel values
	fadeRec_p->currPxlVal_p = malloc(numFadeBytes);
	if (fadeRec_p->currPxlVal_p == NULL) {
		Error_Handler(ERR_MALLOC_FAIL);
	}
	*currPxlVal_pp = fadeRec_p->currPxlVal_p; // inform the caller of buffer address

	// allocate buffer for new pixel values
	fadeRec_p->newPxlVal_p = malloc(numFadeBytes);
	if (fadeRec_p->newPxlVal_p == NULL) {
		Error_Handler(ERR_MALLOC_FAIL);
	}
	*newPxlVal_pp = fadeRec_p->newPxlVal_p;  // inform the caller of buffer address

	// allocate buffer for pixel fade info
	fadeRec_p->fadeByte_p = malloc(numFadeBytes * sizeof(FADE_BYTE_INFO));
	if (fadeRec_p->fadeByte_p == NULL) {
		Error_Handler(ERR_MALLOC_FAIL);
	}
	opp_info.haveFade = TRUE; // we have at least one fade type to manage
	fadeRec_p->valid = TRUE;
	// set callbacks
	fadeRec_p->fadeProc_fp = fadeProc_fp;
	fadeRec_p->endFadeProc_fp = endFadeProc_fp;
	// make sure totTicks is 0 for each pixel fade info
	for (index = 0, tmpFadeByte_p = fadeRec_p->fadeByte_p; index < numFadeBytes; index++, tmpFadeByte_p++) {
		tmpFadeByte_p->totTicks = 0;
	}
	/* Calculate maximum number of bytes to process at a time */
	fade_info.maxProcBytes = fade_info.totNumBytes / 16;
	if (fade_info.maxProcBytes < 16) {
		fade_info.maxProcBytes = 16;
	}
}


/*
 * ===============================================================================
 * 
 * Name: fade_10ms_tick
 *
 * ===============================================================================
 */
/**
 * Fade 10 ms tick
 *
 * Set the flag to start fade processing.
 *
 * @param   None
 * @return  None
 *
 * @pre     None
 * @note    None
 *
 * ===============================================================================
 */
void fade_10ms_tick() {
    fade_info.tickOcc = TRUE;
}


/*
 * ===============================================================================
 * 
 * Name: fade_find_next_fade_rec
 *
 * ===============================================================================
 */
/**
 * Find next fade record
 *
 * Find next record or if done mark fadeRunning to false
 *
 * @param   None
 * @return  None
 *
 * @pre     None
 * @note    None
 *
 * ===============================================================================
 */
void fade_find_next_fade_rec() {
	BOOL foundRec = FALSE;

	/* Find next valid fade record */
	while (fade_info.currFadeRec_p < &fade_info.fadeRec[MAX_FADE_RECS]) {
		if (fade_info.currFadeRec_p->valid) {
			foundRec = TRUE;
			break;
		}
		fade_info.currFadeRec_p++;
	}
	if (foundRec) {
		fade_info.currByteOffset = 0;
	} else {
		fade_info.fadeRunning = FALSE;
		fade_info.totBytesProc = 0;
	}
}


/*
 * ===============================================================================
 *
 * Name: fade_process_fade_bytes
 * 
 * ===============================================================================
 */
/**
 * Fade process fade bytes
 * 
 * Process a fade bytes (up to 64 are processed at a time)
 * 
 * @param   None
 * @return  None
 * 
 * @pre     None 
 * @note    None
 * 
 * ===============================================================================
 */
void fade_process_fade_bytes() {
	U8 *src_p;
	U8 *dst_p;
	U8 newData;
	U8 currData;
	U8 diff;
	BOOL yield = FALSE;
	FADE_BYTE_INFO *tmpFadeByte_p;
	FADE_INFO_REC *fadeRec_p;

	if (fade_info.currByteOffset == ILLEGAL_FADE_OFFSET) {
		/* Find next valid fade record */
		fade_find_next_fade_rec();
	}
	if (fade_info.fadeRunning) {
		fadeRec_p = fade_info.currFadeRec_p;
		src_p = fadeRec_p->newPxlVal_p + fade_info.currByteOffset;
		dst_p = fadeRec_p->currPxlVal_p + fade_info.currByteOffset;
		tmpFadeByte_p = fadeRec_p->fadeByte_p + fade_info.currByteOffset;
		while ((fade_info.currByteOffset < fadeRec_p->numDataBytes) && !yield) {
			if (tmpFadeByte_p->totTicks != 0) {
				if (tmpFadeByte_p->currTick >= tmpFadeByte_p->totTicks) {
					newData = *src_p++;
					*dst_p++ = newData;
					fadeRec_p->fadeProc_fp(fade_info.currByteOffset, newData);
					tmpFadeByte_p->totTicks = 0;
				} else {
					newData = *src_p++;
					currData = *dst_p++;
					if (newData > currData) {
						diff = newData - currData;
					} else {
						diff = currData - newData;
					}
					diff = (U8) (((UINT) diff * (UINT) tmpFadeByte_p->currTick) / (UINT) tmpFadeByte_p->totTicks);
					if (newData > currData) {
						currData += diff;
					} else {
						currData -= diff;
					}
					fadeRec_p->fadeProc_fp(fade_info.currByteOffset, currData);
					tmpFadeByte_p->currTick++;
				}
			} else {
				src_p++;
				dst_p++;
			}
			tmpFadeByte_p++;
			fade_info.currByteOffset++;
			fade_info.totBytesProc++;

			/* Check if should yield to reduce main loop latency */
			if ((fade_info.totBytesProc % fade_info.maxProcBytes) == 0) {
				yield = TRUE;
			}
		}
		if (fade_info.currByteOffset >= fadeRec_p->numDataBytes) {
			fadeRec_p->endFadeProc_fp();

			/* Move to next record if exists */
			fade_info.currByteOffset = ILLEGAL_FADE_OFFSET;
			fade_info.currFadeRec_p++;
			fade_find_next_fade_rec();
		}
	}
}


/*
 * ===============================================================================
 *
 * Name: fade_task
 *
 * ===============================================================================
 */
/**
 * Fade task
 *
 * Check if a new fade processing cycle needs to start.
 *
 * @param   None
 * @return  None
 *
 * ===============================================================================
 */
void fade_task() {
	if (opp_info.validCfg && opp_info.haveFade) {
		if (fade_info.tickOcc && !fade_info.fadeRunning) {
			fade_info.tickOcc = FALSE;
			fade_info.currByteOffset = ILLEGAL_FADE_OFFSET;
			fade_info.currFadeRec_p = &fade_info.fadeRec[0];
			fade_info.fadeRunning = TRUE;
		}
		if (fade_info.fadeRunning) {
			/* Process this fade bytes */
			fade_process_fade_bytes();
		}
	}
}


/*
 * ===============================================================================
 *
 * Name: fade_update_rcv_cmd
 *
 * ===============================================================================
 */
/**
 * Fade update command (0x40)
 *
 * Create a new fade command
 * This is called as soon as command header is received, before data is actually sent and check-summed (or not...)
 * TODO even is CRC is bad, command will be processed !
 *
 * @param   offset      [in]        Byte offset for fade update
 * @param   numBytes    [in]        Number of bytes to update
 * @param   fadeTime    [in]        Fade time in ms
 * @return  None
 *
 * ===============================================================================
 */
void fade_update_rcv_cmd(U16 offset, U16 numBytes, U16 fadeTime) {
	U16 fadeRec = offset >> 12;
	fade_info.fadeRcvCmdBadHeader = TRUE; // assume bad first
	fade_info.rcvTotTicks = 0;
	fade_info.rcvNumBytes = numBytes;
	if (fadeRec < MAX_FADE_RECS) {
		// we record the fadeRec currently processed by the command
		// it will be used by the fade_update_rcv_data() when data bytes are sent
		fade_info.rcvCmdFadeRec_p = &fade_info.fadeRec[fadeRec];
		fade_info.rcvOffset = offset & 0xfff; // clear high nibble
		// make sure we do not exceed the buffer size
		if ((fade_info.rcvOffset + numBytes) <= fade_info.rcvCmdFadeRec_p->numDataBytes) {
			// this header is valid
			fade_info.rcvTotTicks = fadeTime / 10;
			fade_info.fadeRcvCmdBadHeader = FALSE;
			// next, fade_update_rcv_data() will process the data bytes
		}
	}
}


/*
 * ===============================================================================
 *
 * Name: fade_update_rcv_data
 *
 * ===============================================================================
 */
/**
 * Fade update command, receive data
 *
 * Update fade command data. This called for each data byte received.
 *
 * @param   data        [in]        Byte of data
 * @return  TRUE if all data bytes received, FALSE if more byte(s) expected
 *
 * @note    Last byte is CRC8, so not saved as data
 *
 * ===============================================================================
 */
BOOL fade_update_rcv_data(U8 data) {
	FADE_INFO_REC *fadeRec_p;
	FADE_BYTE_INFO *tmpFadeByte_p;
	U8 prevNewByte;

	if (fade_info.rcvNumBytes != 0) {
		fade_info.rcvNumBytes--;
		if (!fade_info.fadeRcvCmdBadHeader) {
			fadeRec_p = fade_info.rcvCmdFadeRec_p;
			tmpFadeByte_p = fadeRec_p->fadeByte_p + fade_info.rcvOffset;
			if (fade_info.rcvTotTicks == 0) {
				// immediate update
				*(fadeRec_p->currPxlVal_p + fade_info.rcvOffset) = data;
				tmpFadeByte_p->totTicks = 0;
				fadeRec_p->fadeProc_fp(fade_info.rcvOffset, data);
			} else {
				/* If currently fading, set byte to end value of prev fade */
				if (tmpFadeByte_p->totTicks != 0) {
					prevNewByte = *(fadeRec_p->newPxlVal_p + fade_info.rcvOffset);
					*(fadeRec_p->currPxlVal_p + fade_info.rcvOffset) = prevNewByte;
					fadeRec_p->fadeProc_fp(fade_info.rcvOffset, prevNewByte);
				} else {
					prevNewByte = *(fadeRec_p->currPxlVal_p + fade_info.rcvOffset);
				}

				/* Only fade if prevByte does not match new value */
				if (prevNewByte == data) {
					tmpFadeByte_p->totTicks = 0;
				} else {
					tmpFadeByte_p->totTicks = fade_info.rcvTotTicks;
					tmpFadeByte_p->currTick = 0;
					*(fadeRec_p->newPxlVal_p + fade_info.rcvOffset) = data;
				}
			}
			fade_info.rcvOffset++;
		}
		return (FALSE);
	}
	return (TRUE);
}

