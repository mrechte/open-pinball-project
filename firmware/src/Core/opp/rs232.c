/**
 * This is the serial port processing module.
 * It uses the VCP on USB.
 *
 *===============================================================================
 */

#include <assert.h>
#include <string.h>

#include "stm32f1xx_hal.h"

#include "stdl.h"
#include "timer.h"

#include "opp.h"
#include "rs232.h"
#include "digital.h"
#include "solenoid.h"
#include "incand.h"
#include "swmtrx.h"
#include "neopxl.h"

#define TX_BUF_SIZE           128 	// Including card addr, command, CRC and EOM
#define RX_BUF_SIZE           0x400 	// If overflowed, it will truncate old data.
#define MAX_RCV_CMD_LEN       260    /* Must account space for CRC8, but not for card addr and command.  */

#define INCAND_CMD_OFFSET     0
#define INCAND_MASK_OFFSET    1
#define CONFIG_NUM_OFFSET     0
#define CONFIG_DATA_OFFSET    1

// States of the receive process (rs232proc_task)
typedef enum {
	RS232_WAIT_FOR_CARD_ID_STATE, // initial state
	RS232_WAIT_FOR_CMD_STATE,
	RS232_RCV_DATA_CMD_STATE,
	RS232_WAIT_FOR_EOM_STATE,
	RS232_INVENTORY_CMD_STATE, /* Special case since unknown length */
	RS232_RCV_FADE_HDR_STATE,
	RS232_RCV_FADE_DATA_STATE,
} RS232_STATE_E;

typedef struct {
	RS232_STATE_E state; // current state for the receive process
	RS232_CMD_E currCmd; // current command
	UINT cmdLen; // the length of the current command as per defined in cmdSpec including CRC8 (viz + 1)
	UINT currIndex; // index in receive buffer
	U8 crc8; // used to compute crc8, either for receive and for sending response.
	U16 cmdTimestamp; // Timestamp of received command byte
	U8 *rcvHead_p; // pointer to the RX buffer (any new received byte is inserted here)
	U8 *rcvTail_p; // pointer to the RX buffer (any processed byte is read from here)
	U8 rxBuf[RX_BUF_SIZE]; // RX ring buffer (this automatically fed by the USB VCP)
	U8 rxTmpBuf[MAX_RCV_CMD_LEN]; // RX buffer for the current command, it contains possible parameters according to cmdSpec.len
} RS232_GLOB_T;

RS232_GLOB_T rs232_glob;

// This table indicates the number of input bytes following the command byte (not counting CRC and EOM).
// some special commands are not part of this table, because the command length is not known in advance
struct cmdSpec {
	U8 cmd;
	UINT len;
} cmdSpec[] = {
	{ RS232_GET_SER_NUM, 4 },
	{ RS232_GET_PROD_ID, 4},
	{ RS232_GET_VERS, 4},
	{ RS232_SET_SER_NUM, 4},
	{ RS232_RESET, 0},
	{ RS232_CONFIG_SOL, 256},
	{ RS232_KICK_SOL, 4},
	{ RS232_EXT_KICK_SOL, 8},
	{ RS232_READ_INP, 4},
	{ RS232_CONFIG_INP, 96},
	{ RS232_SAVE_CFG, 0},
	{ RS232_ERASE_CFG, 0},
	{ RS232_GET_CFG, 4},
	{ RS232_SET_CFG, 4},
	{ RS232_CFG_NEOPIXEL, 7},
	{ RS232_INCAND, 5},
	{ RS232_CONFIG_IND_SOL, 4},
	{ RS232_CONFIG_IND_INP, 4},
	{ RS232_SET_IND_NEO, 2},
	{ RS232_SET_SOL_INPUT, 2},
	{ RS232_READ_MATRIX_INP, 8},
	{ RS232_GET_INP_TIMESTAMP, 0},
	{ RS232_SOL_KICK_PWM, 2},
	{ RS232_EXT_CONFIG_IND_SOL, 9},
	{ RS232_GET_WING_CFG, 1},
	{ RS232_GET_SOL_STATE, 1},
	{ RS232_GET_COUNTERS, 0},
};

#define NUM_CMDS sizeof (cmdSpec) / sizeof (struct cmdSpec)

/* Prototypes */
void rs232_rx_buffer(U8 *rcv_p, U32 length);
void rs232_bswap_copy_dest(U32 *data_p, U8 *dst_p);
void rs232_copy_dest(U8 *src_p, U8 *dst_p, UINT length);
void rs232_bswap_data_buf(U32 *data_p, UINT numBytes);

void stdlser_get_xmt_info(U8 **data_pp, U16 *numChar_p);
uint8_t CDC_Transmit_FS(uint8_t *Buf, uint16_t Len);

void fade_update_rcv_cmd(U16 offset, U16 numBytes, U16 fadeTime);
BOOL fade_update_rcv_data(U8 data);

void Error_Handler(int strokes); // in main.c


__attribute__ ((always_inline)) inline U16 htons(U16 host) {
    return __builtin_bswap16(host);
}


__attribute__ ((always_inline)) inline U32 htonl(U32 host) {
    return __builtin_bswap32(host);
}



/*
 * ===============================================================================
 * 
 * Name: rs232proc_init
 * 
 * ===============================================================================
 *
 * Initialize RS232 processing
 * 
 * Initialize serial port link.
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void rs232_init(void) {
	assert(MAX_WING_TYPES <= 32);
	/* Initialize the global structure */
	rs232_glob.state = RS232_WAIT_FOR_CARD_ID_STATE;
	rs232_glob.rcvHead_p = &rs232_glob.rxBuf[0];
	rs232_glob.rcvTail_p = &rs232_glob.rxBuf[0];
} /* End rs232_init */


/*
 * ===============================================================================
 *
 * Name: rs232proc_* protocol commands
 *
 * ===============================================================================
 *
 * rs232 commands
 * Note that txBuf[0] and [1] are already set (cardid, cmd)
 *
 * @param   txBuf a pointer to the transmit buffer
 * @return  None
 * ===============================================================================
 */

// Get Serial Number, 0x00
void rs232_get_ser_num(U8 *txBuf) {
	rs232_bswap_copy_dest((U32*) (&opp_persist_p->serNum), &txBuf[RS232_CMD_HDR]);
	txBuf[6] = 0xff;
	stdlser_calc_crc8(&txBuf[6], 6, &txBuf[0]);
	txBuf[7] = RS232_EOM;
	(void) stdlser_xmt_data(&txBuf[0], 8);
}


// Get Product ID, 0x01
// See Get Cfg, 0x0d


// Get Version, 0x02
void rs232_get_vers(U8 *txBuf) {
	U32 codeVers = OPP_CODE_VERS;

	rs232_bswap_copy_dest((U32*) &codeVers, &txBuf[RS232_CMD_HDR]);
	txBuf[6] = 0xff;
	stdlser_calc_crc8(&txBuf[6], 6, &txBuf[0]);
	txBuf[7] = RS232_EOM;
	(void) stdlser_xmt_data(&txBuf[0], 8);
}


// Set Serial Number, 0x03
void rs232_set_ser_num(U8 *txBuf) {
	/* Check if serial number is blank */
	if (opp_persist_p->serNum != 0xffffffff) {
		/* Already set, so respond with serial number */
		rs232_bswap_copy_dest((U32*) (&opp_persist_p->serNum), &txBuf[RS232_CMD_HDR]);
		stdlser_calc_crc8(&txBuf[6], 6, &txBuf[0]);
		txBuf[7] = RS232_EOM;
		(void) stdlser_xmt_data(&txBuf[0], 8);
	} else {
		opp_info.serNum = (U32) rs232_glob.rxTmpBuf[3] | ((U32) rs232_glob.rxTmpBuf[2] << 8)
				| ((U32) rs232_glob.rxTmpBuf[1] << 16) | ((U32) rs232_glob.rxTmpBuf[0] << 24);
		assert(stdlflash_write((U16* )&opp_info.serNum, (U16* )(&opp_persist_p->serNum), sizeof(U32)) == FALSE);
	}
}


// Reset, 0x04
void rs232_reset() {
	NVIC_SystemReset();
}


// Go Boot, 0x05
// Not supported


// Configure Solenoid Board, 0x06
void rs232_config_sol(U8 *txBuf) {
	U8 *src_p;
	U8 *dst_p;

	assert(opp_info.solDrvCfg_p);
	src_p = &rs232_glob.rxTmpBuf[0];
	dst_p = (U8*) opp_info.solDrvCfg_p;
	assert(dst_p);
	memcpy(dst_p, src_p, sizeof(OPP_SOL_CFG_T));
	solenoid_upd_cfg(0xffff);
}


// Kick Solenoids, 0x07
void rs232_kick_sol(U8 *txBuf) {
	U32 mask;
	U32 tmpU32;

	//DisableInterrupts; // TODO what for ?
	// clear bits which are masked, set bits of solenoids to kick
	// mask indicates the selected solenoids to operate
	mask = ((U16)rs232_glob.rxTmpBuf[2] << 8) | (U16)rs232_glob.rxTmpBuf[3];
	// tmpU32 are the solenoids value 1 = ON, 0 = OFF
	tmpU32 = ((U16)rs232_glob.rxTmpBuf[0] << 8) | (U16)rs232_glob.rxTmpBuf[1];
	solenoid_drive(tmpU32, mask);
	//EnableInterrupts;
}


// Extended Kick Solenoids, 0x27
void rs232_ext_kick_sol(U8 *txBuf) {
	U32 mask;
	U32 tmpU32;

	//DisableInterrupts;
	// clear bits which are masked, set bits of solenoids to kick
	// mask indicates the selected solenoids to operate
	mask = ((U32)rs232_glob.rxTmpBuf[4] << 24) | ((U32)rs232_glob.rxTmpBuf[5] << 16) | ((U32)rs232_glob.rxTmpBuf[6] << 8) | (U32)rs232_glob.rxTmpBuf[7];
	// tmpU32 are the solenoids value 1 = ON, 0 = OFF
	tmpU32 = ((U32)rs232_glob.rxTmpBuf[0] << 24) | ((U32)rs232_glob.rxTmpBuf[1] << 16) | ((U32)rs232_glob.rxTmpBuf[2] << 8) | (U32)rs232_glob.rxTmpBuf[3];
	solenoid_drive(tmpU32, mask);
	//EnableInterrupts;
}



// Read Inputs, 0x08
void rs232_read_inp(U8 *txBuf) {
	U32 tmpU32;

	tmpU32 = digital_input_read();
	rs232_bswap_copy_dest(&tmpU32, &txBuf[RS232_CMD_HDR]);
	txBuf[6] = 0xff;
	stdlser_calc_crc8(&txBuf[6], 6, &txBuf[0]);
	txBuf[7] = RS232_EOM;
	(void) stdlser_xmt_data(&txBuf[0], 8);
}


// Configure Inputs Board, 0x09
void rs232_config_inp(U8 *txBuf) {
	INP_CFG_T *cfg_p;
	UINT index;
	U8 *src_p;

	for (index = 0, src_p = rs232_glob.rxTmpBuf, cfg_p = opp_info.inpCfg_p->inpCfg;
			index < RS232_NUM_INP;
			index++, cfg_p++) {
		cfg_p->cfg = *src_p++;
		// cfg_p->debounce = *src_p++ << 8 | *src_p++; // warning: operation on 'src_p' may be undefined [-Wsequence-point]
		cfg_p->debounce = *src_p++ << 8;
		cfg_p->debounce |= *src_p++;
	}
	solenoid_upd_inp_cfg(0xffff);

}


// Unused, 0xa
// :)


// Save Cfg, 0x0b
void rs232_save_cfg(U8 *txBuf) {
	/* Calculate the CRC */
	opp_info.nvCfgInfo.nvCfgCrc = 0xff;
	stdlser_calc_crc8(&opp_info.nvCfgInfo.nvCfgCrc,
	OPP_NV_PARM_SIZE, &opp_info.nvCfgInfo.wingCfg[0]);
	// flash cfg
	if (stdlflash_write((U16*) &opp_info.nvCfgInfo, (U16*) OPP_CFG_TBL, sizeof(OPP_NV_CFG_T))) {
		// OK we got an error, may be because bank has not be erased first
		stdlflash_sector_erase((U16*) OPP_CFG_TBL);
		// try again but no chance for error now
		assert(stdlflash_write((U16 *)&opp_info.nvCfgInfo, (U16 *)OPP_CFG_TBL, sizeof(OPP_NV_CFG_T)) == FALSE);
		// rewrite serial number because it was cleared by previous operation
		assert(stdlflash_write((U16* )&opp_info.serNum, (U16* )(&opp_persist_p->serNum), sizeof(U32)) == FALSE);
	}
	opp_info.validCfg = TRUE;
}


// Erase Cfg, 0x0c
void rs232_erase_cfg(U8 *txBuf) {
	stdlflash_sector_erase((U16*) OPP_CFG_TBL);
	opp_reset_info();
	opp_info.validCfg = TRUE;
	// clear serial number
	opp_info.serNum = 0xffffffff;
	opp_info.prodId = 0xffffffff;
	// save the serial/prodid if set
	if (opp_info.serNum != 0xffffffff) {
		assert(stdlflash_write((U16* )&opp_info.serNum, (U16* )(&opp_persist_p->serNum), sizeof(U32)) == FALSE);
	}
	if (opp_info.prodId != 0xffffffff) {
		assert(stdlflash_write((U16* )&opp_info.prodId, (U16* )(&opp_persist_p->prodId), sizeof(U32)) == FALSE);
	}
	// reconf
	digital_init();
}


// Get Cfg, 0x0d
void rs232_get_cfg(U8 *txBuf){
	/* Product ID is uniquely identified by wing board cfg */
	rs232_copy_dest(&opp_info.nvCfgInfo.wingCfg[0], &txBuf[RS232_CMD_HDR], RS232_NUM_WING);
	txBuf[6] = 0xff;
	stdlser_calc_crc8(&txBuf[6], 6, &txBuf[0]);
	txBuf[7] = RS232_EOM;
	(void) stdlser_xmt_data(&txBuf[0], 8);
}


// Set Cfg, 0x0e
void rs232_set_cfg(U8 *txBuf) {
	UINT index;

	// check type before acting
	for (index = 0; index < RS232_NUM_WING; index++) {
		if (rs232_glob.rxTmpBuf[index] >= MAX_WING_TYPES) {
			opp_info.counters.badCmd++;
			return;
		}
	}
	/* reset wings config is not a simple task, so we reset the global opp_info: previous flash conf will be lost ! */
	opp_reset_info();
	opp_info.validCfg = TRUE;
	for (index = 0; index < RS232_NUM_WING; index++) {
		opp_info.nvCfgInfo.wingCfg[index] = rs232_glob.rxTmpBuf[index];
	}
	// reconf
	digital_init();
}


// Change Neopixel Cmd, 0x0f
// Not supported


// Change Neopixel Color, 0x10
// Not supported


// Change Neopixel Color Table, 0x11
// Not supported


// Cfg Neopixel, 0x12
void rs232_config_neopxl(U8 *txBuf) {
	U8 chain;

	assert(opp_info.neoCfg_p);
	assert(rs232_glob.cmdLen - 1 ==  sizeof(NEO_CFG_T));  // -1 for CRC8
	/* Command only passes chainType, bytesPerPixel, numPixels, and initial color (4 bytes) */
	chain = rs232_glob.rxTmpBuf[0] & 0x0f; // low nibble is chain #
	if (chain >= RS232_NUM_NEO) return; // invalid chain
	memcpy(&opp_info.neoCfg_p->neoCfg[chain], rs232_glob.rxTmpBuf, sizeof(NEO_CFG_T));
}


// Incandescent Command, 0x13
void rs232_incand_cmd(U8 *txBuf) {
	U32 mask;

	mask = ((U32) rs232_glob.rxTmpBuf[INCAND_MASK_OFFSET] << 24)
			| ((U32) rs232_glob.rxTmpBuf[INCAND_MASK_OFFSET + 1] << 16)
			| ((U32) rs232_glob.rxTmpBuf[INCAND_MASK_OFFSET + 2] << 8)
			| (U32) rs232_glob.rxTmpBuf[INCAND_MASK_OFFSET + 3];
	incand_proc_cmd(rs232_glob.rxTmpBuf[INCAND_CMD_OFFSET], mask);
}


// Configure Individual Solenoid, 0x14
void rs232_config_ind_sol(U8 *txBuf) {
	UINT index;
	U8 *src_p;
	U8 *dst_p;
	SOL_CFG_T *solCfg_p;

	assert(opp_info.solDrvCfg_p);
	/* First byte contains solenoid number [0-31] */
	index = rs232_glob.rxTmpBuf[CONFIG_NUM_OFFSET]; // solenoid
	if (index < RS232_NUM_SOL) {
		solCfg_p = &opp_info.solDrvCfg_p->solCfg[index];
		/* to remain compatible with legacy command, we need to rework that part to default missing values (viz. inpBits and kickPwm).
		 * The new version has added 5 bytes to the original 3 bytes of RS232I_SOL_CFG_T.
		 */
		src_p = &rs232_glob.rxTmpBuf[CONFIG_DATA_OFFSET];
		dst_p = (U8*) solCfg_p;
		// legacy bytes
		for (int i = 0; i < 3; i++) {
			*dst_p++ = *src_p++;
		}
		// skip the missing data in order to keep former conf (if any)
		dst_p += 5;
		assert(dst_p - (U8* )&opp_info.solDrvCfg_p->solCfg[index] == sizeof(SOL_CFG_T));
		//
		solenoid_upd_cfg(1 << index);
	}
}



// Configure Individual Input, 0x15
void rs232_config_ind_inp(U8 *txBuf) {
	INP_CFG_T *cfg_p;
	UINT index;
	U8 *src_p;

	assert(opp_info.inpCfg_p);
	index = rs232_glob.rxTmpBuf[CONFIG_NUM_OFFSET];
	if (index < RS232_NUM_INP) {
		src_p = &rs232_glob.rxTmpBuf[CONFIG_DATA_OFFSET];
		cfg_p = &opp_info.inpCfg_p->inpCfg[index];
		cfg_p->cfg = *src_p++;
		// cfg_p->debounce = *src_p++ << 8 | *src_p; // warning: operation on 'src_p' may be undefined [-Wsequence-point]
		cfg_p->debounce = *src_p++ << 8 ;
		cfg_p->debounce |= *src_p;
		solenoid_upd_inp_cfg(1 << index);
	}
}


// Set Individual Neopixel, 0x16
// Not supported


// Set Solenoid Input, 0x17
void rs232_set_sol_input(U8 *txBuf) {
	UINT index;

	index = rs232_glob.rxTmpBuf[CONFIG_DATA_OFFSET];
	if ((index  & SOL_INP_SOL_MASK) < RS232_NUM_SOL) {
		solenoid_set_input(index,
			rs232_glob.rxTmpBuf[CONFIG_NUM_OFFSET]);
	}
}


// Upgrade Other Board, 0x18
// Not supported


// Read Matrix Input, 0x19
void rs232_read_matrix_inp(U8 *txBuf) {

	/* Only state is supported for switch matrices */
	rs232_copy_dest(swmtrx_get_buf(), &txBuf[RS232_CMD_HDR], RS232_MATRX_COL);
	txBuf[10] = 0xff;
	stdlser_calc_crc8(&txBuf[10], 10, &txBuf[0]);
	txBuf[11] = RS232_EOM;
	(void) stdlser_xmt_data(&txBuf[0], 12);
	/* Clear bits/set bits currently not active.  This means that a bit
	 * that is active won't be cleared until it is read by the processor.
	 */
	swmtrx_set_prev();
}

// Get Input Timestamp, 0x1a
void rs232_get_inp_timestamp(U8 *txBuf) {
	rs232_copy_dest((U8*)digital_get_input_ts_buf(), &txBuf[RS232_CMD_HDR], RS232_TIMESTAMP_BYTES);
	txBuf[RS232_CMD_HDR + RS232_TIMESTAMP_BYTES] = 0xff;
	stdlser_calc_crc8(&txBuf[RS232_CMD_HDR + RS232_TIMESTAMP_BYTES],
			RS232_CMD_HDR + RS232_TIMESTAMP_BYTES, &txBuf[0]);
	txBuf[RS232_CMD_HDR + RS232_TIMESTAMP_BYTES + RS232_CRC8_SZ] = RS232_EOM;
	(void) stdlser_xmt_data(&txBuf[0],
			RS232_CMD_HDR + RS232_TIMESTAMP_BYTES + RS232_CRC8_SZ + 1);
}



// Solenoid Power PWM, 0x1b
void rs232_sol_kick_pwm(U8 *txBuf) {
	UINT index;

	index = rs232_glob.rxTmpBuf[CONFIG_DATA_OFFSET];
	if (index < RS232_NUM_SOL) {
		solenoid_set_kick_pwm(index, rs232_glob.rxTmpBuf[CONFIG_NUM_OFFSET]);
	}
}


// Not used, 0x1c

// Extended Configure Individual Solenoid, 0x1d
void rs232_ext_config_ind_sol(U8 *txBuf) {
	UINT index;
	U8 *src_p;
	U8 *dst_p;

	/* First byte contains solenoid number [0-31] */
	index = rs232_glob.rxTmpBuf[CONFIG_NUM_OFFSET]; // solenoid
	if (index < RS232_NUM_SOL) {
		assert(opp_info.solDrvCfg_p);
		src_p = &rs232_glob.rxTmpBuf[CONFIG_DATA_OFFSET];
		dst_p = (U8*) &opp_info.solDrvCfg_p->solCfg[index];
		memcpy(dst_p, src_p, sizeof(SOL_CFG_T));
		solenoid_upd_cfg(1 << index);
	}
}


// Get Wing Config Command, 0x1e
void rs232_get_wing_cfg(U8 *txBuf) {
	UINT index;
	U8 *src_p;
	U8 *dst_p;
	U8 data;
	U8 crc8 = 0xff;
	U8 len = 0;

	// wing # is first byte
	index = rs232_glob.rxTmpBuf[CONFIG_NUM_OFFSET];
	txBuf[2] = index;
	if (index < RS232_NUM_WING) {
		// wing type
		data = opp_info.nvCfgInfo.wingCfg[index];
		txBuf[3] = data;
		switch (data) {
			case WING_SOL:
			case WING_SOL_8: {
				assert(opp_info.solDrvCfg_p);
				// config length
				if (data == WING_SOL) {
					len = 4 * sizeof(SOL_CFG_T);
					src_p = (U8*) &opp_info.solDrvCfg_p->solCfg[index << 2]; // 4 solenoids / wing
				} else {
					len = 8 * sizeof(SOL_CFG_T);
					src_p = (U8*) &opp_info.solDrvCfg_p->solCfg[index << 3]; // 8 solenoids / wing
				}
				txBuf[4] = len;
				// copy conf and compute CRC
				dst_p = &txBuf[5];
				memcpy(dst_p, src_p, len);
				dst_p += len;
				// CRC
				stdlser_calc_crc8(&crc8, 5 + len, txBuf);
				*dst_p++ = crc8;
				// EOM
				*dst_p++ = RS232_EOM;
				// addr + cmd + wing + type + length + crc + eom = 7 bytes
				(void) stdlser_xmt_data(txBuf, 7 + len);
				break;
			}
			case WING_INP: {
				assert(opp_info.inpCfg_p);
				len = 24;
				txBuf[4] = len;
				src_p = (U8*) &opp_info.inpCfg_p->inpCfg[index << 3];
				// copy conf and compute CRC
				dst_p = &txBuf[5];
				for (int i = 0; i < 8; i++) {
					*dst_p++ = *src_p++; // type
					src_p++; // ignore filler
					*dst_p++ = *(src_p+1); // debonce hton
					*dst_p++ = *src_p; // debounce hton
					src_p += 2;
				}
				// CRC
				stdlser_calc_crc8(&crc8, 5 + len, txBuf);
				*dst_p++ = crc8;
				// EOM
				*dst_p++ = RS232_EOM;
				// addr + cmd + wing + type + length + crc + eom = 7 bytes
				(void) stdlser_xmt_data(txBuf, 7 + len);
				break;
			}
			case WING_NEO:
			case WING_NEO_SOL:
				assert(opp_info.neoCfg_p);
				len = 7;
				txBuf[4] = len;
				src_p = (U8*) &opp_info.neoCfg_p->neoCfg[index == 0 ? 0 : 1];
				// copy conf and compute CRC
				dst_p = &txBuf[5];
				memcpy(dst_p, src_p, len);
				dst_p += len;
				// CRC
				stdlser_calc_crc8(&crc8, 5 + len, txBuf);
				*dst_p++ = crc8;
				// EOM
				*dst_p++ = RS232_EOM;
				// addr + cmd + wing + type + length + crc + eom = 7 bytes
				(void) stdlser_xmt_data(txBuf, 7 + len);
				break;
			default: {
				// no data
				txBuf[4] = len; // 0
				// CRC
				stdlser_calc_crc8(&crc8, 5, txBuf);
				txBuf[5] = crc8;
				// EOM
				txBuf[6] = RS232_EOM;
				(void) stdlser_xmt_data(txBuf, 7);
				break;
			}
		} // switch (wing type)
	}
}



// Neopixel Fade Command, 0x40
// handled directly by the state machine functions (res232proc_neo_color_tbl_state, rs232proc_rcv_neo_hdr_state, rs232proc_rcv_neo_data_state)

// SPI Command, 0x41
// Not supported



// 0xe0 - 0xef: debug commands
// ---------------------------

// Get Solenoid state, 0xe0
void rs232_get_sol_state(U8 *txBuf) {
	UINT index;
	U8 crc8 = 0xff;

	// solenoid # is first byte
	index = rs232_glob.rxTmpBuf[CONFIG_NUM_OFFSET];
	txBuf[2] = index;
	if (index < RS232_NUM_SOL) {
		txBuf[3] = solenoid_get_state(index);
		stdlser_calc_crc8(&crc8, 4, txBuf);
		txBuf[4] = crc8;
		txBuf[5] = RS232_EOM;
		(void) stdlser_xmt_data(txBuf, 6);
	}
}


// Get Counters, 0xe1
void rs232_get_counters(U8 *txBuf) {
	U8 crc8 = 0xff;
	U16 *u16_p;
	U8 *u8_p;

	u16_p = (U16 *)&txBuf[2];
	*u16_p++ = htons(opp_info.counters.crcErr);
	*u16_p++ = htons(opp_info.counters.badCmd);
	*u16_p++ = htons(opp_info.counters.timeOut);
	stdlser_calc_crc8(&crc8, 8, txBuf); // id + cmd + 3 x U16
	u8_p = (U8*)u16_p;
	*u8_p++ = crc8;
	*u8_p++ = RS232_EOM;
	(void) stdlser_xmt_data(txBuf, u8_p - txBuf);
}


// Inventory Command, 0xf0
// handled directly by the state machine function (rs232proc_inventory_cmd_state)


/*
 * ===============================================================================
 *
 * Name: rs232proc_*state
 *
 * ===============================================================================
 *
 * State Machine functions
 *
 * @param	data the data byte received
 * @param   txBuf a pointer to the transmit buffer
 * @return  None
 * ===============================================================================
 */


void rs232_wait_for_card_id_state(U8 data) {
	// In that state, we expect INNVENTORY or card ID. EOM is always echoed.
	if (data == RS232_INVENTORY) {
		rs232_glob.state = RS232_INVENTORY_CMD_STATE;
		// Only allow a certain time window to get the complete command
		rs232_glob.cmdTimestamp = (U16) timer_get_ms_count();
	} else if (data == RS232_EOM) {
		// echo EOM
		(void) stdlser_xmt_data(&data, 1);
	} else if (data == CARD_ID_CARD) {
		rs232_glob.crc8 = 0xff;
		stdlser_calc_crc8(&rs232_glob.crc8, RS232_CRC8_SZ, &data);
		rs232_glob.state = RS232_WAIT_FOR_CMD_STATE;
		// Only allow a certain time window to get the complete command
		rs232_glob.cmdTimestamp = (U16) timer_get_ms_count();
	} else {
		/* Lost synchronization.  Keep looking for a valid card ID */
	}
}


void rs232_wait_for_cmd_state(U8 data, U8 *txBuf) {
	struct cmdSpec *curCmd_p;
	BOOL found = FALSE;
	INT index;

	// We distinguish regular command, from special ones (
	rs232_glob.currIndex = 0;
	for (index = 0; index < NUM_CMDS; index++) {
		curCmd_p = &cmdSpec[index];
		if (curCmd_p->cmd == data) {
			found = TRUE;
			break;
		}
	}
	if (found) {
		// A regular command
		/* Save the current command and the length */
		rs232_glob.currCmd = data;
		rs232_glob.cmdLen = curCmd_p->len + RS232_CRC8_SZ;

		stdlser_calc_crc8(&rs232_glob.crc8, 1, &data);
		txBuf[0] = CARD_ID_CARD;
		txBuf[1] = data;
		/* Wait for command data now */
		rs232_glob.state = RS232_RCV_DATA_CMD_STATE;
	// Special commands
	} else if (data == RS232_FADE) {
		stdlser_calc_crc8(&rs232_glob.crc8, 1, &data);
		// Wait for neo header now
		rs232_glob.state = RS232_RCV_FADE_HDR_STATE;
	} else {
		/* Bad command received, send EOM */
		opp_info.counters.badCmd++;
		data = RS232_EOM;
		(void) stdlser_xmt_data(&data, 1);
		rs232_glob.state = RS232_WAIT_FOR_CARD_ID_STATE;
	}
}


void rs232_inventory_cmd_state(U8 data, U8 *txBuf) {
	if (data == RS232_EOM) {
		txBuf[0] = RS232_INVENTORY;
		txBuf[1] = CARD_ID_CARD;
		txBuf[2] = RS232_EOM;
		(void) stdlser_xmt_data(&txBuf[0], 3);
	}
	rs232_glob.state = RS232_WAIT_FOR_CARD_ID_STATE;
}


void rs232_rcv_data_cmd_state(U8 data, U8 *txBuf) {
	/* Copy the data into the rcv buffer, and calculate CRC */
	rs232_glob.rxTmpBuf[rs232_glob.currIndex++] = data;
	if (rs232_glob.currIndex < rs232_glob.cmdLen) {
		stdlser_calc_crc8(&rs232_glob.crc8, 1, &data);
		return; // more chars to come
	}
	/* Blink status LED */
	opp_info.statusBlink ^= OPP_STAT_TOGGLE_LED;
	*((R32*) OPP_STAT_BSRR_PTR) = opp_info.statusBlink;

	/* Whole command has been received, data is CRC */
	if (data == rs232_glob.crc8) {
		// Next data should be EOM
		rs232_glob.state = RS232_WAIT_FOR_EOM_STATE;
	} else {
		// bad CRC
		opp_info.counters.crcErr++;
		rs232_glob.state = RS232_WAIT_FOR_CARD_ID_STATE;
	}
}



void rs232_rcv_fade_hdr_state(U8 data) {
	// Data = offset (2 bytes), num bytes (2 bytes), time of fade (2 bytes), data..., CRC8
	U16 offset, numBytes, fadeTime;
	stdlser_calc_crc8(&rs232_glob.crc8, 1, &data);
	rs232_glob.rxTmpBuf[rs232_glob.currIndex++] = data;
	if (rs232_glob.currIndex == 6) {
		offset = (rs232_glob.rxTmpBuf[0] << 8) | rs232_glob.rxTmpBuf[1];
		numBytes = (rs232_glob.rxTmpBuf[2] << 8) | rs232_glob.rxTmpBuf[3];
		fadeTime = (rs232_glob.rxTmpBuf[4] << 8) | rs232_glob.rxTmpBuf[5];
		fade_update_rcv_cmd(offset, numBytes, fadeTime);
		rs232_glob.state = RS232_RCV_FADE_DATA_STATE;
	}
}


void rs232_rcv_fade_data_state(U8 data) {
	BOOL done;

	done = fade_update_rcv_data(data);
	if (!done) {
		stdlser_calc_crc8(&rs232_glob.crc8, 1, &data);
		// more bytes to come
	} else {
		/* Blink status LED */
		opp_info.statusBlink ^= OPP_STAT_TOGGLE_LED;
		*((R32*) OPP_STAT_BSRR_PTR) = opp_info.statusBlink;
		if (data != rs232_glob.crc8) {
			opp_info.counters.crcErr++;
		}
		/* Whole command has been rcvd, now wait for next cmd */
		rs232_glob.state = RS232_WAIT_FOR_CARD_ID_STATE;
	}
}

/*
 * ===============================================================================
 * 
 * Name: rs232proc_task
 * 
 * ===============================================================================
 *
 * Task for rs232 commands
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void rs232_task(void) {
	U8 data;
	U8 txBuf[RS232_MAX_TX_BUF_SZ];
	uint8_t *xmtData_p;
	uint16_t xmtLength;
	BOOL processCommand = FALSE;

	/* Check if received a char */
	while (rs232_glob.rcvTail_p != rs232_glob.rcvHead_p) {
		if (processCommand)
			break; // pass hand to main loop in order to process pending command
		// Should we not be be in initial state (WAIT_FOR_CARD_ID), check there is no timeout
		if (rs232_glob.state != RS232_WAIT_FOR_CARD_ID_STATE) {
			if ((U16) timer_get_ms_count() - rs232_glob.cmdTimestamp > RS232_CMD_TIMEOUT) {
				// too late, let us consider this byte is a new command
				rs232_glob.state = RS232_WAIT_FOR_CARD_ID_STATE;
				opp_info.counters.timeOut++;
			}
		}
		data = *rs232_glob.rcvTail_p++;
		if (rs232_glob.rcvTail_p >= &rs232_glob.rxBuf[RX_BUF_SIZE]) {
			rs232_glob.rcvTail_p = &rs232_glob.rxBuf[0];
		}
		switch (rs232_glob.state) {

			case RS232_WAIT_FOR_CARD_ID_STATE: // initial state
				rs232_wait_for_card_id_state(data);
				break;

			case RS232_INVENTORY_CMD_STATE:
				rs232_inventory_cmd_state(data, txBuf);
				break;

			case RS232_WAIT_FOR_CMD_STATE:
				// determine command received
				rs232_wait_for_cmd_state(data, txBuf);
				break;

			case RS232_RCV_DATA_CMD_STATE:
				// command is known, now we expect its parameters
				rs232_rcv_data_cmd_state(data, txBuf);
				break;

			case RS232_WAIT_FOR_EOM_STATE: {
				// we expect now EOM, if not we have a problem
				if (data == RS232_EOM) {
					/* Do command processing. Note that txBuf[0] and [1] are already set */
					switch (rs232_glob.currCmd) {

						case RS232_GET_SER_NUM:
							rs232_get_ser_num(txBuf);
							break;
						case RS232_GET_PROD_ID:
						case RS232_GET_CFG:
							rs232_get_cfg(txBuf);
							break;
						case RS232_GET_VERS:
							rs232_get_vers(txBuf);
							break;
						case RS232_SET_SER_NUM:
							rs232_set_ser_num(txBuf);
							break;
						case RS232_READ_INP:
							rs232_read_inp(txBuf);
							break;
						case RS232_CFG_NEOPIXEL:
							rs232_config_neopxl(txBuf);
							break;
						case RS232_READ_MATRIX_INP:
							rs232_read_matrix_inp(txBuf);
							break;
						case RS232_GET_INP_TIMESTAMP:
							rs232_get_inp_timestamp(txBuf);
							break;
						case RS232_RESET:
							rs232_reset();
							break;
						case RS232_GO_BOOT:
							/* This command resets the processor
							   STM32 doesn't support bootloading */
							break;
						case RS232_CONFIG_SOL:
							rs232_config_sol(txBuf);
							break;
						case RS232_KICK_SOL:
							rs232_kick_sol(txBuf);
							break;
						case RS232_EXT_KICK_SOL:
							rs232_ext_kick_sol(txBuf);
							break;
						case RS232_CONFIG_INP:
							rs232_config_inp(txBuf);
							break;
						case RS232_SAVE_CFG:
							rs232_save_cfg(txBuf);
							break;
						case RS232_ERASE_CFG:
							rs232_erase_cfg(txBuf);
							break;
						case RS232_SET_CFG:
							rs232_set_cfg(txBuf);
							break;
						case RS232_INCAND:
							rs232_incand_cmd(txBuf);
							break;
						case RS232_CONFIG_IND_SOL:
							rs232_config_ind_sol(txBuf);
							break;
						case RS232_EXT_CONFIG_IND_SOL:
							rs232_ext_config_ind_sol(txBuf);
							break;
						case RS232_CONFIG_IND_INP:
							rs232_config_ind_inp(txBuf);
							break;
						case RS232_SET_SOL_INPUT:
							rs232_set_sol_input(txBuf);
							break;
						case RS232_SOL_KICK_PWM:
							rs232_sol_kick_pwm(txBuf);
							break;
						case RS232_GET_WING_CFG:
							rs232_get_wing_cfg(txBuf);
							break;
						case RS232_GET_SOL_STATE:
							rs232_get_sol_state(txBuf);
							break;
						case RS232_GET_COUNTERS:
							rs232_get_counters(txBuf);
							break;
						default:
							/* Invalid cmd for RS232_RCV_DATA_CMD_STATE, send EOM */
							data = RS232_EOM;
							(void) stdlser_xmt_data(&data, 1);
							opp_info.counters.badCmd++;
							break;
					} // switch (rs232_glob.currCmd)
					processCommand = TRUE;
				} // EOM received, if something else is received, we just ignore it and do not process the command
				/* Whole command has been received */
				rs232_glob.state = RS232_WAIT_FOR_CARD_ID_STATE;
				break;
			} // RS232_WAIT_FOR_EOM_STATE

			case RS232_RCV_FADE_HDR_STATE:
				// receive first 6 bytes
				rs232_rcv_fade_hdr_state(data);
				break;

			case RS232_RCV_FADE_DATA_STATE:
				// receive data part (length indicated by header)
				rs232_rcv_fade_data_state(data);
				break;

		} // switch (rs232_glob.state)

	} // received char

	// do we have some data to pass to USB VCP ?
	stdlser_get_xmt_info(&xmtData_p, &xmtLength);
	if (xmtLength) {
		// we do not care about the return value, if BUSY, the packet is lost.
		CDC_Transmit_FS(xmtData_p, xmtLength);
	}
} /* End rs232proc_task */


/*
 * ===============================================================================
 * 
 * Name: rs232proc_rx_ser_char
 * 
 * ===============================================================================
 *
 * Receive a buffer
 * 
 * Receive a data buffer from USB.  Save the buffer, length, and mark the flag.
 * 
 * @param   rcv_p - received buffer pointer
 * @param   length - amount of data received
 * @return  TRUE if buffer overflow occurs
 * ===============================================================================
 */
void rs232_rx_buffer(U8 *rcv_p, U32 length) {
	U8 *nxt_p;

	nxt_p = rs232_glob.rcvHead_p;
	while (length != 0) {
		/* Verify write won't overflow the queue */
		nxt_p++;
		if (nxt_p >= &rs232_glob.rxBuf[RX_BUF_SIZE]) {
			nxt_p = &rs232_glob.rxBuf[0];
		}
		if (nxt_p != rs232_glob.rcvTail_p) {
			*rs232_glob.rcvHead_p = *rcv_p++;
			rs232_glob.rcvHead_p = nxt_p;
			length--;
		} else {
			break;
		}
	}
} /* End rs232proc_rx_buffer */


/*
 * ===============================================================================
 * 
 * Name: rs232proc_bswap_copy_dest
 * 
 * ===============================================================================
 *
 * Byte swap 32 bit data and copy to destination. It converts endianness.
 * 
 * @param   data_p pointer o the source data
 * @param	dst_p pointer to the destination buffer (U8 *)
 * @return  None
 * ===============================================================================
 */
void rs232_bswap_copy_dest(U32 *data_p, U8 *dst_p) {
	U32 data;
	//U8 *src_p;
	//UINT index;

	data = htonl(*data_p);
	/* data = ((data >> 24) & 0xff) | ((data << 8) & 0xff0000) | ((data >> 8) & 0xff00) | ((data << 24) & 0xff000000);
	for (src_p = (U8*) &data, index = 0; index < sizeof(U32); index++) {
		*dst_p++ = *src_p++;
	}
	*/
	*(U32 *)dst_p = data;
}


/*
 * ===============================================================================
 * 
 * Name: rs232proc_copy_dest
 * 
 * ===============================================================================
 *
 * Byte copy to destination
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void rs232_copy_dest(U8 *src_p, U8 *dst_p, UINT length) {
	UINT index;

	for (index = 0; index < length; index++) {
		*dst_p++ = *src_p++;
	}
} /* End rs232proc_copy_dest */

/*
 * ===============================================================================
 * 
 * Name: rs232proc_bswap_data_buf
 * 
 * ===============================================================================
 *
 * Byte swap received data buffer to move from little endian to big endian
 * 
 * This converts from received bytes stream from little endian to big endian.
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void rs232_bswap_data_buf(U32 *data_p, UINT numBytes) {
	U32 data;
	UINT index;

	for (index = 0; index < numBytes; index += sizeof(U32)) {
		data = *data_p;
		*data_p++ = ((data >> 24) & 0xff) | ((data << 8) & 0xff0000) | ((data >> 8) & 0xff00) | ((data << 24) & 0xff000000);
	}
} /* End rs232proc_bswap_data_buf */

/* [] END OF FILE */
