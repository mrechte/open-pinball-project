#ifndef SOLENOID_H
#define SOLENOID_H



/* Solenoid configuration saved in flash, part of cfgData
 * This is a 8-byte long structure
 * Min off time is 0-7 times the initial kick time.  If initial kick
 * is 20 ms and min off is 5, the solenoid will be forced off for 100 ms
 * Do not change field order (protocol bound)
 */
typedef struct {
   RS232_CFG_SOL_TYPE_E       cfg;
   U8                         initKick; // in ms
   RS232I_DUTY_E              minOffDuty; // is composed of 2 nibbles: min off time (MIN_OFF_MASK) and hold duty cycle (DUTY_CYCLE_MASK)
   U8						  kickPwm; // kick PWM from 0 (min) to Ox1f (100%)
   U32                        inpBits; // input bits
} SOL_CFG_T; // intentionally not packed



UINT solenoid_get_state(U8 solIndex);

void solenoid_upd_cfg(U32 updMask);

void solenoid_set_input(U8 solIndex, U8 inpIndex);

void solenoid_set_kick_pwm(U8 solIndex, U8 kickPwm);

void solenoid_upd_inp_cfg(U32 updMask);

void solenoid_drive(U32 value, U32 solMask);

void solenoid_sol_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);

void solenoid_sol_8_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);

void solenoid_task(U32 inputs, U32 updFilterLow);

#endif
