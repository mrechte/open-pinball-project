/**
 * @file:   stdlflash.c
 * This is the flash utility, it contains erase and write functions for the Flash.
 *
 *===============================================================================
 */ 
#include "mcu.h"
#include "stdl.h"
#include "stdtypes.h"   /* include peripheral declarations */


/*
 * ===============================================================================
 * 
 * Name: stdlflash_unlock_flash
 *
 * ===============================================================================
 */
/**
 * Unlock flash for erase/write
 *
 * @param   None
 * @return  None
 *
 * @pre None
 * @note None
 *
 * ===============================================================================
 */
void stdlflash_unlock_flash()
{

#define KEY1                  0x45670123
#define KEY2                  0xCDEF89AB

    if (flashBase_p->CR & FLSH_CR_LOCK) {
	   /* Write key values to unlock flash erase/writes */
	   flashBase_p->KEYR = KEY1;
	   flashBase_p->KEYR = KEY2;
    }

} /* End stdlflash_unlock_flash */

/*
 * ===============================================================================
 *
 * Name: stdlflash_sector_erase
 * 
 * ===============================================================================
 */
/**
 * Erase flash sector (in this case row)
 * 
 * Blocking erase sector (row)
 * 
 * @param   dest_p      [in]    ptr to Flash sector address to erase
 * @return  FALSE if no error occurs
 * 
 * @pre None 
 * @note None
 * 
 * ===============================================================================
 */
BOOL stdlflash_sector_erase( 
   U16                        *dest_p)      /* ptr to sector addr in flash */
{

   
   /* Verify flash isn't busy */
   if (flashBase_p->SR & FLSH_SR_BSY)
   {
      return TRUE;
   }

   stdlflash_unlock_flash();

   // reset tested flags
   flashBase_p->SR = FLSH_SR_PGERR | FLSH_SR_WRPRTERR;
   
   flashBase_p->CR = FLSH_CR_PER;
   flashBase_p->AR = (U32)dest_p;
   flashBase_p->CR = FLSH_CR_PER | FLSH_CR_STRT;

   while (flashBase_p->SR & FLSH_SR_BSY);
   if ((flashBase_p->SR & (FLSH_SR_PGERR | FLSH_SR_WRPRTERR)) != 0)
   {
      return TRUE;
   }
   return (FALSE);
   
} /* End stdlflash_sector_erase */

/*
 * ===============================================================================
 * 
 * Name: stdlflash_write
 * 
 * ===============================================================================
 */
/**
 * Write to flash
 * 
 * Blocking call to write to flash.
 * 
 * @param   src_p       [in]    ptr to source of data
 * @param   dest_p      [in]    ptr to destination of data in flash
 * @param   numBytes    [in]    number of bytes to write
 * @return  FALSE if no error occurs
 * 
 * @pre None 
 * @note User must insure that the data is erased first.
 * 
 * ===============================================================================
 */
BOOL stdlflash_write( 
   U16                        *src_p,       /* ptr to source of data */
   U16                        *dest_p,      /* ptr to destination of data in flash */
   INT                        numBytes)     /* number of bytes */
{

   /* Verify flash isn't busy */
   if (flashBase_p->SR & FLSH_SR_BSY)
   {
      return TRUE;
   }

   stdlflash_unlock_flash();

   // reset tested flags
   flashBase_p->SR = FLSH_SR_PGERR | FLSH_SR_WRPRTERR;

   flashBase_p->CR = FLSH_CR_PG;
   for (INT index = 0; index < numBytes; index += sizeof(U16))
   {
	   *dest_p++ = *src_p++;
	   // flashBase_p->CR = FLSH_CR_STRT;
	   while (flashBase_p->SR & FLSH_SR_BSY);
       if ((flashBase_p->SR & (FLSH_SR_PGERR | FLSH_SR_WRPRTERR)) != 0)
       {
          return TRUE;
       }
   }
   return (FALSE);
} /* End stdlflash_write */
