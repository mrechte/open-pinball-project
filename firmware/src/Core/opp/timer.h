#ifndef TIMER_H
#define TIMER_H

void timer_init();

void timer_overflow_isr();

UINT timer_get_ms_count();

U16 timer_get_us_count();

UINT timer_get_msus_count();

#endif
