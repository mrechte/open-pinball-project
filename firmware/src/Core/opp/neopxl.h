#ifndef NEOPXL_H
#define NEOPXL_H

#include "stdtypes.h"

/*
 * Neo LED configuration saved in flash, part of cfgData
 * This is a 7-byte long structure
 */
typedef struct {
   U8		chainType; // low nibble is chain 0 (legacy on wing 0) or 1 (on wing 3). High nibble is type is 0 for Neopixel, 1 for SPIpixel
   U8       bytesPerPixel; // either 3 for RGB, or 4 for RGBW
   U8       numPixel; // number of pixels in the chain (0 stands for 256)
   U8		initColor[4]; // 3 or 4 bytes, initial pixel color (ff stands for default, 0 is off). If initColor[3] is 0xa5 and numPixel = 3, it indicates SPI LEDs
} __attribute__((packed)) NEO_CFG_T;



void neopxl_neo_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);
void neopxl_neo_deinit(UINT wing);
void neopxl_task();

#endif
