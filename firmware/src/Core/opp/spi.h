/**
 * @file:   spiintf.h
 * This is the SPI interface file.
 *
 *===============================================================================
 */
#include "opp.h"
#include "stdtypes.h"

/* Prototypes */
void spi_init();
void spi_task();
void spi_proc_spi_cmd(INT offset, U8 *data_p, INT numBytes);

/* [] END OF FILE */
