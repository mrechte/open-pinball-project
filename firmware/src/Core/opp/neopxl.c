/**
 * neopxl.c
 * This is for driving Neopixels using SPI IP (optionally using SCK line).
 * It uses DMA to send the data.
 * Neopixel data is presented 8 bits of green, then red, then blue with msb first (WS2812).
 *
 * This file requires a 10ms tick to start the neopixel processing.

SPI pin mapping on STM32F103C8T6

        SPI1    SPI2
NSS     PA4     PB12
SCK     PA5     PB13
MISO    PA6     PB14
MOSI    PA7     PB15

Remark: no alternate remapping possible

SPI clock is set to 48MHz/16 = 3MHz or 333ns/tick
For SpiPixel (true SPI), it seems SPI clock is doubled to 6MHz

SPI uses MOSI and possibly SCK (for true SPI) lines (no NSS, nor MISO).
This will affect wing 0 (legacy) pin 3 (MOSI) and possibly wing 0 pin 1 (SCK), using SP2.
This will affect wing 3 pin 3 (MOSI) and possibly wing 2 pin 1 (SCK), using SP1.
See also digital.c

This module works with the fade system, using 2 fade types: NEO (legacy) and NEO2. This is the fade cmd 0x40 that dictates which fade is affected.

The principle is that the fade system will update the fade buffer using the relevant neopxl_[spi_]fade_procN().
The neopxl_task() will trigger the start_dma_spiX() to update the LED string. This is a continuous process, even if no fade cmd is sent.

 *===============================================================================
 */

#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "neopxl.h"

#include "fade.h"
#include "mcu.h"
#include "opp.h"
#include "stdtypes.h"

#define SPI_BYTES_PER_NEO_BIT     4  /* 4 SPI bytes are needed for a single NeoPixel byte using NEO_BYTE_EXPAND table */

#define MAX_NEOPIXELS            256

// State machine values
#define STATE_DMA_DATA            0	// Transferring data to LED string using DMA
#define STATE_WAIT_FADE_DONE      1
#define STATE_WAIT_FOR_TICK       2
#define STATE_DISABLED            0xff // Initial state

#define PIXEL_HALF_ON            0x80
#define PIXEL_OFF                0x00

/*  SPI2 uses MCU GIPIO Port B bit 15 (MOSI) and bit 13 (SCK), which tallies to Wing 0 - pin 4 and Wing 0 - pin 2
Therefore input bits (starting from b0) values are MOSI (0x00000010 = input bit b4) and SCK (0x00000008 = input bit b2)
 */
#define SPI2_MOSI_INP_MASK 0x00000010
#define SPI2_SCK_INP_MASK  0x00000008

/*  SPI1 uses MCU GIPIO PA7 (MOSI) and PA5 (SCK), which tallies to Wing 3 - pin 27 and Wing 3 - pin 25
Therefore input bits (starting from b0) values are MOSI (0x08000000 = input bit b27) and SCK (0x02000000 = input bit b25) */
#define SPI1_MOSI_INP_MASK 0x08000000
#define SPI1_SCK_INP_MASK  0x02000000


struct NEO_STRING {
	UINT stringNum; // 0 or 1
	void (*fadeProc)(UINT, U8); // fade proc
	void (*fadeEndProc)(); // fade end proc
	void (*startDmaProc)(struct NEO_STRING *); // start DMA proc
	UINT offset; // RS232_FADE_NEO_OFFSET or RS232_FADE_NEO2_OFFSET
	U8 state; /* State */
	BOOL tickOcc; /* 10 ms tick occurred */
	BOOL fadeDone; /* Fade processing is done */
	BOOL neoPxls; /* TRUE if neopixels vs SPI LEDs */
	INT numDataBytes; /* Number of dataBytes (pixels * bytesPerPixel), as sent by fade cmd 0x40 */
	INT numXferBytes; /* Number of DMA bytes to transfer at each cycle */
	U8 *dmaBuffer; // The DMA buffer comprising a header followed by the pixel data
	U32 *dma_p; /* Pointer to pixel data start in be DMA buffer, each pixel is made of 3 or 4 U32 elements */
};

typedef struct NEO_STRING NEO_STRING;

typedef struct {
	NEO_STRING strings[RS232_NUM_NEO];
} NEO_INFO;

NEO_INFO neo_info;

/* Note:  This is in little endian data format since DMA is little endian */
const U32 NEO_BYTE_EXPAND[256] = {
	0x88888888, 0x8c888888, 0xc8888888, 0xcc888888, 0x888c8888, 0x8c8c8888, 0xc88c8888, 0xcc8c8888,	/* 0x00 - 0x07 */
    0x88c88888, 0x8cc88888, 0xc8c88888, 0xccc88888, 0x88cc8888, 0x8ccc8888, 0xc8cc8888, 0xcccc8888,	/* 0x08 - 0x0f */
    0x88888c88, 0x8c888c88, 0xc8888c88, 0xcc888c88, 0x888c8c88, 0x8c8c8c88, 0xc88c8c88, 0xcc8c8c88,	/* 0x10 - 0x17 */
    0x88c88c88, 0x8cc88c88, 0xc8c88c88, 0xccc88c88, 0x88cc8c88, 0x8ccc8c88, 0xc8cc8c88, 0xcccc8c88,	/* 0x18 - 0x1f */
    0x8888c888, 0x8c88c888, 0xc888c888, 0xcc88c888, 0x888cc888, 0x8c8cc888, 0xc88cc888, 0xcc8cc888,	/* 0x20 - 0x07 */
    0x88c8c888, 0x8cc8c888, 0xc8c8c888, 0xccc8c888, 0x88ccc888, 0x8cccc888, 0xc8ccc888, 0xccccc888,	/* 0x28 - 0x2f */
    0x8888cc88, 0x8c88cc88, 0xc888cc88, 0xcc88cc88, 0x888ccc88, 0x8c8ccc88, 0xc88ccc88, 0xcc8ccc88,	/* 0x30 - 0x37 */
    0x88c8cc88, 0x8cc8cc88, 0xc8c8cc88, 0xccc8cc88, 0x88cccc88, 0x8ccccc88, 0xc8cccc88, 0xcccccc88,	/* 0x38 - 0x3f */
    0x8888888c, 0x8c88888c, 0xc888888c, 0xcc88888c, 0x888c888c, 0x8c8c888c, 0xc88c888c, 0xcc8c888c,	/* 0x40 - 0x47 */
    0x88c8888c, 0x8cc8888c, 0xc8c8888c, 0xccc8888c, 0x88cc888c, 0x8ccc888c, 0xc8cc888c, 0xcccc888c,	/* 0x48 - 0x4f */
    0x88888c8c, 0x8c888c8c, 0xc8888c8c, 0xcc888c8c, 0x888c8c8c, 0x8c8c8c8c, 0xc88c8c8c, 0xcc8c8c8c,	/* 0x50 - 0x57 */
    0x88c88c8c, 0x8cc88c8c, 0xc8c88c8c, 0xccc88c8c, 0x88cc8c8c, 0x8ccc8c8c, 0xc8cc8c8c, 0xcccc8c8c,	/* 0x58 - 0x5f */
    0x8888c88c, 0x8c88c88c, 0xc888c88c, 0xcc88c88c, 0x888cc88c, 0x8c8cc88c, 0xc88cc88c, 0xcc8cc88c,	/* 0x60 - 0x67 */
    0x88c8c88c, 0x8cc8c88c, 0xc8c8c88c, 0xccc8c88c, 0x88ccc88c, 0x8cccc88c, 0xc8ccc88c, 0xccccc88c,	/* 0x68 - 0x6f */
    0x8888cc8c, 0x8c88cc8c, 0xc888cc8c, 0xcc88cc8c, 0x888ccc8c, 0x8c8ccc8c, 0xc88ccc8c, 0xcc8ccc8c,	/* 0x70 - 0x77 */
    0x88c8cc8c, 0x8cc8cc8c, 0xc8c8cc8c, 0xccc8cc8c, 0x88cccc8c, 0x8ccccc8c, 0xc8cccc8c, 0xcccccc8c,	/* 0x78 - 0x7f */
    0x888888c8, 0x8c8888c8, 0xc88888c8, 0xcc8888c8, 0x888c88c8, 0x8c8c88c8, 0xc88c88c8, 0xcc8c88c8,	/* 0x80 - 0x87 */
    0x88c888c8, 0x8cc888c8, 0xc8c888c8, 0xccc888c8, 0x88cc88c8, 0x8ccc88c8, 0xc8cc88c8, 0xcccc88c8,	/* 0x88 - 0x8f */
    0x88888cc8, 0x8c888cc8, 0xc8888cc8, 0xcc888cc8, 0x888c8cc8, 0x8c8c8cc8, 0xc88c8cc8, 0xcc8c8cc8,	/* 0x90 - 0x97 */
    0x88c88cc8, 0x8cc88cc8, 0xc8c88cc8, 0xccc88cc8, 0x88cc8cc8, 0x8ccc8cc8, 0xc8cc8cc8, 0xcccc8cc8,	/* 0x98 - 0x9f */
    0x8888c8c8, 0x8c88c8c8, 0xc888c8c8, 0xcc88c8c8, 0x888cc8c8, 0x8c8cc8c8, 0xc88cc8c8, 0xcc8cc8c8,	/* 0xa0 - 0xa7 */
    0x88c8c8c8, 0x8cc8c8c8, 0xc8c8c8c8, 0xccc8c8c8, 0x88ccc8c8, 0x8cccc8c8, 0xc8ccc8c8, 0xccccc8c8,	/* 0xa8 - 0xaf */
    0x8888ccc8, 0x8c88ccc8, 0xc888ccc8, 0xcc88ccc8, 0x888cccc8, 0x8c8cccc8, 0xc88cccc8, 0xcc8cccc8,	/* 0xb0 - 0xb7 */
    0x88c8ccc8, 0x8cc8ccc8, 0xc8c8ccc8, 0xccc8ccc8, 0x88ccccc8, 0x8cccccc8, 0xc8ccccc8, 0xccccccc8,	/* 0xb8 - 0xbf */
    0x888888cc, 0x8c8888cc, 0xc88888cc, 0xcc8888cc, 0x888c88cc, 0x8c8c88cc, 0xc88c88cc, 0xcc8c88cc,	/* 0xc0 - 0xc7 */
    0x88c888cc, 0x8cc888cc, 0xc8c888cc, 0xccc888cc, 0x88cc88cc, 0x8ccc88cc, 0xc8cc88cc, 0xcccc88cc,	/* 0xc8 - 0xcf */
    0x88888ccc, 0x8c888ccc, 0xc8888ccc, 0xcc888ccc, 0x888c8ccc, 0x8c8c8ccc, 0xc88c8ccc, 0xcc8c8ccc,	/* 0xd0 - 0xd7 */
    0x88c88ccc, 0x8cc88ccc, 0xc8c88ccc, 0xccc88ccc, 0x88cc8ccc, 0x8ccc8ccc, 0xc8cc8ccc, 0xcccc8ccc,	/* 0xd8 - 0xdf */
    0x8888c8cc, 0x8c88c8cc, 0xc888c8cc, 0xcc88c8cc, 0x888cc8cc, 0x8c8cc8cc, 0xc88cc8cc, 0xcc8cc8cc,	/* 0xe0 - 0xe7 */
    0x88c8c8cc, 0x8cc8c8cc, 0xc8c8c8cc, 0xccc8c8cc, 0x88ccc8cc, 0x8cccc8cc, 0xc8ccc8cc, 0xccccc8cc,	/* 0xe8 - 0xef */
    0x8888cccc, 0x8c88cccc, 0xc888cccc, 0xcc88cccc, 0x888ccccc, 0x8c8ccccc, 0xc88ccccc, 0xcc8ccccc,	/* 0xf0 - 0xf7 */
    0x88c8cccc, 0x8cc8cccc, 0xc8c8cccc, 0xccc8cccc, 0x88cccccc, 0x8ccccccc, 0xc8cccccc, 0xcccccccc,	/* 0xf8 - 0xff */
  };


/*
 * ===============================================================================
 *
 * Name: neo_fade_proc
 *
 * ===============================================================================
 *
 * Neopixel fade processing
 *
 * Special processing for NeoPixels.  Used to fill out DMA data stream.
 *
 * @param   offset
 * @param   newData
 * @return  None
 * ===============================================================================
 */
void neopxl_fade_proc0(UINT offset, U8 newData) {
	NEO_STRING *string_p = &neo_info.strings[0];

	*(string_p->dma_p + offset) = NEO_BYTE_EXPAND[newData];
}

void neopxl_fade_proc1(UINT offset, U8 newData) {
	NEO_STRING *string_p = &neo_info.strings[1];

	*(string_p->dma_p + offset) = NEO_BYTE_EXPAND[newData];
}



/*
 * ===============================================================================
 *
 * Name: neo_spi_fade_proc
 *
 * ===============================================================================
 *
 * SPI fade processing
 *
 * Special processing for SPI LEDs.  Used to fill out DMA data stream.
 *
 * @param   offset
 * @param   newData
 * @return  None
 * ===============================================================================
 */
void neopxl_spi_fade_proc0(UINT offset, U8 newData) {
	NEO_STRING *string_p = &neo_info.strings[0];
	INT wordOffs = offset / 3;
	INT byteOffs = (offset % 3) + 1;
	U8 *byte_p;

	byte_p = (U8*) &string_p->dma_p[wordOffs];
	byte_p[byteOffs] = newData;
}


void neopxl_spi_fade_proc1(UINT offset, U8 newData) {
	NEO_STRING *string_p = &neo_info.strings[1];
	INT wordOffs = offset / 3;
	INT byteOffs = (offset % 3) + 1;
	U8 *byte_p;

	byte_p = (U8*) &string_p->dma_p[wordOffs];
	byte_p[byteOffs] = newData;
}



/*
 * ===============================================================================
 *
 * Name: neo_end_fade_proc
 *
 * ===============================================================================
 *
 * Neopixel end fade processing
 *
 * Special processing occurs when fade processing is completed.
 *
 * @param   None
 * @return  None
 * ===============================================================================
 */
void neopxl_end_fade_proc0() {
	NEO_STRING *string_p = &neo_info.strings[0];

	string_p->fadeDone = TRUE;
}

void neopxl_end_fade_proc1() {
	NEO_STRING *string_p = &neo_info.strings[1];

	string_p->fadeDone = TRUE;
}


void neopxl_neo_deinit(UINT wing) {
	NEO_STRING *string_p;

	assert(wing == 0 || wing == 3);

	string_p = wing == 0 ? &neo_info.strings[0] : &neo_info.strings[1];
	if (string_p->dmaBuffer != NULL)
		free(string_p->dmaBuffer);
	memset(string_p, 0, sizeof(NEO_STRING));
}


void start_dma_spi2(NEO_STRING *string_p) {
	// Start sending data using DMA
	// DMA_CCRx - DMA channel x configuration register
	// 	MINC: Memory increment mode (enabled)
	//	DIR: Data transfer direction (read from memory)
	//	EN: Channel enable
	dma1Base_p->CCR5 = DMAx_CCR_MINC | DMAx_CCR_DIR; // disable DMA
    // DMA_CNDTRx - DMA channel x number of data register
	dma1Base_p->CNDTR5 = string_p->numXferBytes;
	dma1Base_p->CCR5 = DMAx_CCR_MINC | DMAx_CCR_DIR | DMAx_CCR_EN;
}


void start_dma_spi1(NEO_STRING *string_p) {
	// Start sending data using DMA
	// DMA_CCRx - DMA channel x configuration register
	// 	MINC: Memory increment mode (enabled)
	//	DIR: Data transfer direction (read from memory)
	//	EN: Channel enable
	dma1Base_p->CCR3 = DMAx_CCR_MINC | DMAx_CCR_DIR; // disable DMA
    // DMA_CNDTRx - DMA channel x number of data register
	dma1Base_p->CNDTR3 = string_p->numXferBytes;
	dma1Base_p->CCR3 = DMAx_CCR_MINC | DMAx_CCR_DIR | DMAx_CCR_EN;
}



void neopxl_init_spi2(NEO_STRING *string_p, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {

	if (string_p->neoPxls) {
		/* Setup SPI2 GPIO port */
		// GPIOB_CRH - Port Configuration Register High => conf bit 15 (MOSI)
		gpioBBase_p->CRH &= ~0xf0000000; // reset conf to 0
		gpioBBase_p->CRH |= 0xb0000000;  // MODE = 11 (Output mode, max speed 50 MHz) CNF = 10 (Alternate function output Push-pull)
		// GPIOB_BSRR - Bit Set/Reset Register => reset ODR (Out Data Register) bit 15
		gpioBBase_p->BSRR = 0x80000000;
		// these bits are now private
		*outputMask_p &= ~SPI2_MOSI_INP_MASK;
		*inputMask_p &= ~SPI2_MOSI_INP_MASK;
		*inputPDMask_p &= ~SPI2_MOSI_INP_MASK;
	} else {
		/* Setup SPI2 GPIO port */
		// GPIOB_CRH - Port Configuration Register High => conf bit 15 (MOSI) and bit 13 (SCK)
		gpioBBase_p->CRH &= ~0xf0f00000;  // reset conf to 0
		gpioBBase_p->CRH |= 0xb0b00000;  // MODE = 11 (Output mode, max speed 50 MHz) CNF = 10 (Alternate function output Push-pull)
		// GPIOB_BSRR - Bit Set/Reset Register => reset ODR (Out Data Register) bit 15 and bit 13
		gpioBBase_p->BSRR = 0xa0000000;
		// these bits are now private
		*outputMask_p &= ~(SPI2_MOSI_INP_MASK | SPI2_SCK_INP_MASK);
		*inputMask_p &= ~(SPI2_MOSI_INP_MASK | SPI2_SCK_INP_MASK);
		*inputPDMask_p &= ~(SPI2_MOSI_INP_MASK | SPI2_SCK_INP_MASK);
	}

	/* Enable clocks to DMA1 and SPI2 */
    // RCC_AHBENR - AHB Peripheral Clock enable register
    rccBase_p->AHBENR |= 0x00000001;  // DMA1EN
    // RCC_APB1ENR - APB1 peripheral clock enable register
    rccBase_p->APB1ENR |= 0x00004000;  // SPI2EN: SPI 2 clock enable

    /* Set up SPI2 */
	if (string_p->neoPxls) {
		// SPI_CR1 - SPI control register 1
		//  SPE: SPI enable
		//  BR: Baud rate control (010 = fPCLK/8)
		//  MSTR: Master selection
		//  SSM: Software slave management (NSS pin is not configured)
		//  SSI: Internal slave select (NSS software pin to slave to avoid master Mode Fault see p.721 reference manual)
		spi2Base_p->CR1 = SPIx_CR1_SPE | SPIx_CR1_BR_DIV8 | SPIx_CR1_MSTR  | SPIx_CR1_SSM | SPIx_CR1_SSI;
	} else {
		// SPI_CR1 - SPI control register 1
		//  SPE: SPI enable
		//  BR: Baud rate control (000 = fPCLK/2)
		//  MSTR: Master selection
		//  SSM: Software slave management (NSS pin is not configured)
		//  SSI: Internal slave select (NSS software pin to slave to avoid master Mode Fault see p.721 reference manual)
		//  CPOL: Clock polarity
		//  CPHA: Clock phase
		spi2Base_p->CR1 = SPIx_CR1_SPE | SPIx_CR1_BR_DIV2 | SPIx_CR1_MSTR | SPIx_CR1_SSM | SPIx_CR1_SSI |
				SPIx_CR1_CPOL | SPIx_CR1_CPHA;
	}

	// SPI_CR2 - SPI control register 2
	// 	TXDMAEN: Tx buffer DMA enable
	spi2Base_p->CR2 = SPIx_CR2_TXDMAEN;

    /* Set up DMA */
    // DMA1 has 7 channels (1-7)
    // DMA_CPARx - DMA channel x peripheral address register
    dma1Base_p->CPAR5 = (R32)&spi2Base_p->DR; // SPI2 Data Register
    // DMA_CMARx - DMA channel x memory address register
    dma1Base_p->CMAR5 = (R32)string_p->dmaBuffer; // DMA buffer start

    start_dma_spi2(string_p);

}


void neopxl_init_spi1(NEO_STRING *string_p, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {

	if (string_p->neoPxls) {
		/* Setup SPI1 GPIO port */
		// GPIOA_CRL - Port Configuration Register Low => conf bit 7 (MOSI)
		gpioABase_p->CRL &= ~0xf0000000; // reset conf to 0
		gpioABase_p->CRL |=  0xb0000000;  // MODE = 11 (Output mode, max speed 50 MHz) CNF = 10 (Alternate function output Push-pull)
		// GPIOA_BSRR - Bit Set/Reset Register => reset ODR (Out Data Register) bit 7
		gpioABase_p->BSRR = 0x00800000;
		// these bits are now private
		*outputMask_p &= ~SPI1_MOSI_INP_MASK;
		*inputMask_p &= ~SPI1_MOSI_INP_MASK;
		*inputPDMask_p &= ~SPI1_MOSI_INP_MASK;
	} else {
		/* Setup SPI1 GPIO port */
		// GPIOA_CRH - Port Configuration Register High => conf bit 7 (MOSI) and bit 5 (SCK)
		gpioABase_p->CRH &= ~0xf0f00000;  // reset conf to 0
		gpioABase_p->CRH |= 0xb0b00000;  // MODE = 11 (Output mode, max speed 50 MHz) CNF = 10 (Alternate function output Push-pull)
		// GPIOA_BSRR - Bit Set/Reset Register => reset ODR (Out Data Register) bit 7 and bit 5
		gpioABase_p->BSRR = 0x00a00000;
		// these bits are now private
		*outputMask_p &= ~(SPI1_MOSI_INP_MASK | SPI1_SCK_INP_MASK);
		*inputMask_p &= ~(SPI1_MOSI_INP_MASK | SPI1_SCK_INP_MASK);
		*inputPDMask_p &= ~(SPI1_MOSI_INP_MASK | SPI1_SCK_INP_MASK);
	}

	/* Enable clocks to DMA1 and SPI1 */
    // RCC_AHBENR - AHB Peripheral Clock enable register
    rccBase_p->AHBENR |= 0x00000001;  // DMA1EN
    // RCC_APB2ENR - APB2 peripheral clock enable register
    rccBase_p->APB2ENR |= 0x00001000;  // SPI1EN: SPI 1 clock enable (b12)

    /* Set up SPI1. Note that APB2 bus is configured with twice the speed of APB1, hence different BR here */
	if (string_p->neoPxls) {
		// SPI_CR1 - SPI control register 1
		//  SPE: SPI enable
		//  BR: Baud rate control (101 = fPCLK/64)
		//  MSTR: Master selection
		//  SSM: Software slave management (NSS pin is not configured)
		//  SSI: Internal slave select (NSS software pin to slave to avoid master Mode Fault see p.721 reference manual)
		spi1Base_p->CR1 = SPIx_CR1_SPE | SPIx_CR1_BR_DIV16 | SPIx_CR1_MSTR | SPIx_CR1_SSM | SPIx_CR1_SSI;
	} else {
		// SPI_CR1 - SPI control register 1
		//  SPE: SPI enable
		//  BR: Baud Rate control (001 = fPCLK/4)
		//  MSTR: Master selection
		//  SSM: Software slave management (NSS pin is not configured)
		//  SSI: Internal slave select (NSS software pin to slave to avoid master Mode Fault see p.721 reference manual)
		//  CPOL: Clock polarity
		//  CPHA: Clock phase
		spi1Base_p->CR1 = SPIx_CR1_SPE | SPIx_CR1_BR_DIV4 | SPIx_CR1_MSTR | SPIx_CR1_SSM | SPIx_CR1_SSI |
				SPIx_CR1_CPOL | SPIx_CR1_CPHA;
	}

	// SPI_CR2 - SPI control register 2
	// 	TXDMAEN: Tx buffer DMA enable
	spi1Base_p->CR2 = SPIx_CR2_TXDMAEN;

    /* Set up DMA */
    // DMA1 has 7 channels (1-7)
    // DMA_CPARx - DMA channel x peripheral address register
    dma1Base_p->CPAR3 = (R32)&spi1Base_p->DR; // SPI1 Data Register
    // DMA_CMARx - DMA channel x memory address register
    dma1Base_p->CMAR3 = (R32)string_p->dmaBuffer; // DMA buffer start

    start_dma_spi1(string_p);

}



void neopxl_init_spi(NEO_STRING *string_p, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	if (string_p->stringNum == 0) neopxl_init_spi2(string_p, inputMask_p, inputPDMask_p, outputMask_p);
	else neopxl_init_spi1(string_p, inputMask_p, inputPDMask_p, outputMask_p);
}



UINT neopxl_get_dma_data_num(NEO_STRING *string_p) {
	// DMA_CNDTRx - DMA channel x number of data register
	return string_p->stringNum == 0 ? dma1Base_p->CNDTR5: dma1Base_p->CNDTR3;
}


/*
 * ===============================================================================
 *
 * Name: neo_init
 * 
 * ===============================================================================
 *
 * Initialize neopixel processing
 * 
 * Turn pixels to default output, allocate memory, and reset state machine.
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void neopxl_neo_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	NEO_STRING *string_p; // pointer to string info
	NEO_CFG_T *neoCfg_p; // pointer to string conf
	U8 type; // work var
	INT index; // work var
	INT offset; // work var
	U8 *srcData_p; // work var
	U8 color; // work var
	BOOL dfltOutput = TRUE;
	UINT numPixels;
	UINT bytesPerPixel;
	U8 *currPxlVal_p; /* Ptr to array of current pixel values */
	U8 *newPxlVal_p; /* Ptr to array of future pixel values */

	assert(wing == 0 || wing == 3);

	string_p = wing == 0 ? &neo_info.strings[0] : &neo_info.strings[1];
	neoCfg_p = wing == 0 ? &opp_info.neoCfg_p->neoCfg[0] : &opp_info.neoCfg_p->neoCfg[1];

	// deallocate first, then reset structure
	neopxl_neo_deinit(wing); // TODO danger if fade.c is already running callbacks !!!!!

	string_p->stringNum = wing == 0 ? 0 : 1;

	bytesPerPixel = neoCfg_p->bytesPerPixel;
	if (bytesPerPixel > 4) return; // BAD value

	numPixels = neoCfg_p->numPixel;
	if (numPixels == 0) {
		numPixels = MAX_NEOPIXELS;
	}

	type = neoCfg_p->chainType >> 4; // high nibble
	if (type > 1) return; // BAD value
	string_p->neoPxls = type == 0;

	if (string_p->stringNum == 0) {
		string_p->fadeProc = string_p->neoPxls ? neopxl_fade_proc0 : neopxl_spi_fade_proc0;
		string_p->fadeEndProc = neopxl_end_fade_proc0;
		string_p->offset = RS232_FADE_NEO_OFFSET;
		string_p->startDmaProc = start_dma_spi2;
	} else {
		string_p->fadeProc = string_p->neoPxls ? neopxl_fade_proc1 : neopxl_spi_fade_proc1;
		string_p->fadeEndProc = neopxl_end_fade_proc1;
		string_p->offset = RS232_FADE_NEO2_OFFSET;
		string_p->startDmaProc = start_dma_spi1;
	}

	/* Initialize the state machine to turn off all the LEDs, set indices to 0 */
	opp_info.haveNeo = TRUE;
	string_p->state = STATE_DISABLED;
	string_p->tickOcc = FALSE;
	string_p->fadeDone = FALSE;
	string_p->neoPxls = TRUE;

	/* Calculate maximum number of bytes to process at a time */
	string_p->numDataBytes = numPixels * bytesPerPixel;
	// Prepare the fade engine for the given fade type (NEO). See fade.c
	fade_init_rec(string_p->offset, string_p->numDataBytes, &currPxlVal_p, &newPxlVal_p, string_p->fadeProc, string_p->fadeEndProc);

	// compute the number of DMA bytes to transfer to LED strings, according to protocol and number of LEDs
	if (string_p->neoPxls) {
		/* for neopixel, allocate four extra bytes set to 0.
		First transmitted bytes must not be data since first bits will be extended to match clock.
		Thhen follows the pixel data, terminated by an extra 0 byte to end the string */
		string_p->numXferBytes = (string_p->numDataBytes * SPI_BYTES_PER_NEO_BIT) + sizeof(U32) + 1;
	} else {
		/* for SPI LED DMA, allocate 4 bytes for start frame,
		 *  and 16 bytes as worst case for the stop frame
		 * Each pixel needs 4 bytes instead of 3 for global field
		 */
		string_p->numXferBytes = (numPixels + 5) * sizeof(U32);
	}

	// allocate the DMA buffer
	string_p->dmaBuffer = malloc(string_p->numXferBytes);
	if (string_p->dmaBuffer == NULL) {
		Error_Handler(ERR_MALLOC_FAIL);
	}

	// Initialize our dma buffer
	/* First byte is zero to synch with clock, since serial LEDs, end of cycle always low, so no extra data */
	/* For SPI pixels, first 32 bits is always 0 to indicate start frame */
	string_p->dma_p = (U32 *)string_p->dmaBuffer;
	*string_p->dma_p = 0;
	/* Make buf_p point to first U32 of pixel data */
	string_p->dma_p++;
	if (string_p->neoPxls) {
		/* Zero byte turns off PWM at end of string */
		*((U8*) string_p->dma_p + (string_p->numDataBytes * SPI_BYTES_PER_NEO_BIT)) = 0;
		// other bytes (pixel) will be set later, see call to neo_fill_out_dma_data(), when initial fade values are known
	} else {
		/* Fill all global and start bits of all frames plus end frames with 0xffffffff */
		for (index = 0; index < numPixels + 4; index++) {
			string_p->dma_p[index] = 0xffffffff;
		}
	}

	// Init the fade buffer according to init colors or default
	// If all initial colors (RGB or RGBW) are 0xff, it means use default (PIXEL_HALF_ON)
	for (index = 0; index < bytesPerPixel; index++) {
		if (neoCfg_p->initColor[index] != 0xff) {
			dfltOutput = FALSE;
		}
	}
	for (index = 0; index < numPixels; index++) {
		for (offset = 0; offset < bytesPerPixel; offset++) {
			color = dfltOutput ? PIXEL_HALF_ON : neoCfg_p->initColor[offset];
			*(currPxlVal_p + ((index * bytesPerPixel) + offset)) = color;
			*(newPxlVal_p + ((index * bytesPerPixel) + offset)) = color;
		}
	}

	// Prepare DMA buffer for our first output
	for (index = 0, srcData_p = currPxlVal_p; index < string_p->numDataBytes; index++, srcData_p++) {
		string_p->fadeProc(index, *srcData_p);
	}

	neopxl_init_spi(string_p, inputMask_p, inputPDMask_p, outputMask_p);

    string_p->state = STATE_DMA_DATA;

}



/*
 * ===============================================================================
 * 
 * Name: neo_10ms_tick
 *
 * ===============================================================================
 *
 * Neopixel 10 ms tick
 *
 * Set the flag to start another Neopixel DMA.
 *
 * @param   None
 * @return  None
 * ===============================================================================
 */
void neopxl_10ms_tick() {
	NEO_STRING *string_p = neo_info.strings;
	UINT i;

	for (i = 0; i < RS232_NUM_NEO; i++, string_p++) {
		string_p->tickOcc = TRUE;
	}
}


/*
 * ===============================================================================
 *
 * Name: neo_task
 * 
 * ===============================================================================
 *
 * Neopixel task
 * 
 * Check if a new neopixel cycle needs to start.  If so, clear indices, create
 * data for two neopixels, and fill the SPI buffer.  Otherwise check if the
 * buffer needs more data.
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void neopxl_task() {
	NEO_STRING *string_p = neo_info.strings;
	UINT i;

	if (opp_info.validCfg && opp_info.haveNeo) {
		for (i = 0; i < RS232_NUM_NEO; i++, string_p++) {
			if ((string_p->state == STATE_DMA_DATA) && (neopxl_get_dma_data_num(string_p) == 0)) {
				// DMA transfer completed (no more byte to transfer)
				string_p->state = STATE_WAIT_FADE_DONE;
			}
			if ((string_p->state == STATE_WAIT_FADE_DONE) && string_p->fadeDone) {
				string_p->fadeDone = FALSE;
				string_p->state = STATE_WAIT_FOR_TICK;
			}
			if ((string_p->state == STATE_WAIT_FOR_TICK) && string_p->tickOcc) {
				string_p->tickOcc = FALSE;
				string_p->state = STATE_DMA_DATA;
				assert(string_p->startDmaProc);
				string_p->startDmaProc(string_p);
			}
		}
	}
}

