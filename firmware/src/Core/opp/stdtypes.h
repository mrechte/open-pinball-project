/**
 * @file:   stdtypes.h
 * This is the standard types file.  It has processor agnostic definitions
 * so code is more portable.
 *
 *===============================================================================
 */
#ifndef STDTYPES_H
#define STDTYPES_H

typedef unsigned char U8;
typedef unsigned short U16;
typedef unsigned long U32;
typedef int INT;
typedef unsigned int UINT;
typedef volatile unsigned long R32;

#define NULL ((void *)0)

typedef enum
{
   FALSE                   = 0,
   TRUE                    = !FALSE
} __attribute__((packed)) BOOL;
   
#endif
    
/* [] END OF FILE */
