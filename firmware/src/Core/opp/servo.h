#ifndef SERVO_H
#define SERVO_H

void servo_servo_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);

#endif
