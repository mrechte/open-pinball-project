#include <assert.h>
#include <string.h>

#include "opp.h"
#include "rs232.h"
#include "solenoid.h"
#include "digital.h"
#include "swmtrx.h"
/*
 * states of a solenoid.
 */
typedef enum {
	SOL_STATE_IDLE = 0x00, // solenoid is off
	SOL_INITIAL_KICK = 0x01, // in the initial kick phase (usually short, but powerful)
	SOL_SUSTAIN_PWM = 0x02, // in the hold state (usually long, but with less power)
	SOL_MIN_TIME_OFF = 0x03, // in the rest state (is allowed to fire again until a rest period)
	SOL_WAIT_BEFORE_KICK = 0x04, // in delay state (for DLY_KICK_SOL solenoids) before kick.
	SOL_FULL_ON_SOLENOID = 0x05, // solenoid is on (for ON_OFF_SOL solenoids).
} SOL_STATE_E;



/*
 * This structure maintains the information required to manage a solenoid.
 */
typedef struct {
	BOOL clearRcvd; // for AUTO_CLR solenoids
	SOL_STATE_E solState; // current solenoid state
	U8 offCnt; // used for SOL_MIN_TIME_OFF counting time
	U32 kickIntenMask; // chosen PWM_MASK for initial kick
	U32 holdIntenMask; // chosen PWM_MASK for hold phase
	U32 bit; // solenoid output bit (only 1 of the 32 bits is set)
	INT startMs; // Timestamp from SOL_STATE_IDLE state to SOL_INITIAL_KICK or SOL_WAIT_BEFORE_KICK state
	U32 inpBits; // solenoid trigger input bits (several of the 32 bits may be set)
	BOOL inputCtl; // Record the fact it was energized by a switch, not by software
} SOL_STATE_T;


/*
 * This structure maintains the conf and state of solenoids.
 */
typedef struct {
	BOOL init; // TRUE if module initialized
	U32 solMask; // mask of the present solenoids (1 bit per solenoid), depending on wing inventory.
	// Note that legacy WING_SOL is limited to 16 solenoids (b0-b15 only) and that it does not necessarily reflects the output bit mask (see outputMask).
	U32 solOutMask; // output bits (32) configured as solenoid outputs (this is *not* equal to solMask for legacy solenoids)
	SOL_STATE_T solState[RS232_NUM_SOL]; // 32 solenoids
	SOL_CFG_T savedSolCfg[RS232_NUM_SOL]; // Solenoid configuration at time of use (configuration may change during the game), used by solenoid_task
	U32 solDrvProcCtl; // used to drive solenoids by software command (see RS232_KICK_SOL, solenoid_proc, swmtrx_proc)
} SOL_GLOB_T;

SOL_GLOB_T sol_info;

/*
 * This the PWM mask consisting of 32 PWM values (0: minimum, 31: 100% power), where each bit represents a 0.5ms sub-period (viz on a 16ms period = 62.5 Hz).
 * Each bit set, represents an ON output, while 0 represents an OFF output.
 * The digital loop computes a 32-bit currMsMask that has one bit set according to the current clock, viz. it is shifted left every 0.5ms.
 * If the corresponding bit in the solenoid mask is set, the solenoid is put ON, else put OFF.
 * So to get an idea of the PWM pulse train for one of the 32 possibles given values here, just convert the value in binary format.
 * The bitMask was created to minimize the sound harmonics.
 */
const U32 PWM_MASK[32] = { 0x00000001, 0x00100001, 0x01004001, 0x41002001, 0x08120201, 0x14240201, 0x41084481, 0x22A21081,
		0x608A2121, 0x32A28421, 0x62259421, 0x5125CA11, 0x6296C451, 0x63B28A91, 0x6ADA3911, 0xABA92E51, 0x7763A4A9, 0x76AEB239,
		0x75DC96B9, 0x77AC5B75, 0x7BD65BB9, 0x7BDEBAB3, 0x7B7BAEF9, 0x7EF7BABB, 0x7EDDEF7D, 0x7FBFDBD7, 0x7FBFB7EF, 0x7FFBFF7D,
		0x7FFDFF7F, 0x7FFFF7FF, 0x7FFFFFFF, 0xFFFFFFFF, };


// Usable solenoid bits, on a given SOL legacy wing (max 4 solenoids)
#define SOL_MASK              0x0f
// Usable solenoid output bits, on a given SOL legacy wing
#define SOL_OUTP_BIT_MASK     0xf0
// Usable solenoid input bits (USE_SWITCH), on a given SOL legacy wing
#define SOL_INP_BIT_MASK      0x0f

// Usable solenoid bits on a given SOL_8 wing  (max 8 solenoids)
#define SOL_8_MASK            0xff
// Usable solenoid outputs bits, on a given SOL_8 wing
#define SOL_8_OUTP_BIT_MASK   0xff


#define MIN_OFF_INC           0x10

// in timer.c
INT timer_get_ms_count();
U16 timer_get_us_count();


void solenoid_init() {
	SOL_STATE_T *solState_p;
	INT index;

	memset(&sol_info, 0, sizeof(sol_info));
	// initialize the state of the 16 solenoids, even if not used
	for (solState_p = &sol_info.solState[0], index = 0;
			index < RS232_NUM_SOL;
			index++, solState_p++) {
		solState_p->solState = SOL_STATE_IDLE;
		// This weird computation sets the output bit of WING_SOL (legacy type): 1 << ( (index / 4) x 8 + (index % 4) )
		// solState_p->bit = 1 << (((index >> 2) << 3) + (index & 0x03) + 4);
		// since there SOL_8 type, one can no longer use this formula. This will be done later.
		solState_p->kickIntenMask = 0xffffffff; // max power
	}
	sol_info.init = TRUE;
}



// init legacy wing
void solenoid_sol_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	INT sol;
	SOL_STATE_T *solState_p;
	INP_CFG_T *inpCfg_p;
	U32 mask;
	U32 currBit;

	assert(wing < RS232_NUM_WING);

	if (!sol_info.init) solenoid_init();

	// prepare the output bit mask
	mask = SOL_OUTP_BIT_MASK << (wing << 3);
#if OPP_DEBUG_PORT != 0
	mask &= DBG_MASK;
#endif
	*outputMask_p |= mask;
	// make sure these bits are no longer inputs
	*inputMask_p &= ~mask;
	*inputPDMask_p &= ~mask;

	sol_info.solMask |= (SOL_MASK << (wing << 2)); // note that numbering does not tally output pin

	/* Set up bit mask of valid inputs */
	mask = SOL_INP_BIT_MASK << (wing << 3);
#if OPP_DEBUG_PORT != 0
	mask &= DBG_MASK;
#endif
	*inputMask_p |= mask;

	// make sure these bits are no longer outputs
	*outputMask_p &= ~mask;

	/* Force input as STATE_INPUT for the solenoids pins */
	for (inpCfg_p = &opp_info.inpCfg_p->inpCfg[wing << 3]; inpCfg_p < &opp_info.inpCfg_p->inpCfg[(wing << 3) + 8];
			inpCfg_p++) {
		if (inpCfg_p->cfg < SERVO_OUTPUT_THRESH) {
			inpCfg_p->cfg = STATE_INPUT;
		}
	}
	// configure output bit and kick PWM
	sol = wing << 2; // wing x 4 (0, 4, 8, 12)
	solState_p = &sol_info.solState[sol];
	currBit = 1 << ((wing << 3) + 4); // 1 << wing x 8 + 4 (4, 12, 20, 28)
	for (int i = 0; i < 4; i++) { // this type of wing has 4 solenoid outputs
		// output
		solState_p->bit = currBit;
		if (sol_info.solMask & currBit) {
			sol_info.solOutMask |= currBit;
		}
		// next
		sol++;
		solState_p++;
		currBit <<= 1;
	}

#if OPP_DEBUG_PORT != 0
	sol_info.solOutMask &= DBG_MASK;
#endif

	digital_config_wing_gpio(wing, inputMask_p, inputPDMask_p, outputMask_p);

}


// init sol 8 wing
void solenoid_sol_8_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	U32 mask;
	INT sol;
	SOL_STATE_T *solState_p;
	U32 currBit;

	assert(wing < RS232_NUM_WING);

	if (!sol_info.init) solenoid_init();

	mask = SOL_8_OUTP_BIT_MASK << (wing << 3); // shift 8-bit mask by 8 x wing index (0-3)
#if OPP_DEBUG_PORT != 0
	mask &= DBG_MASK;
#endif
	*outputMask_p |= mask;

	// make sure these bits are no longer inputs
	*inputMask_p &= ~mask;
	*inputPDMask_p &= ~mask;

	sol_info.solMask |= mask;
	// configure output bit and kick PWM
	sol = wing << 3; // solenoid number = wing index x 8 (0, 8, 16, 24)
	solState_p = &sol_info.solState[sol];
	currBit = 1 << sol; // first bit of wing
	for (int i = 0; i < 8; i++) { // this type of wing has 8 solenoid outputs
		// output
		solState_p->bit = currBit;
		if (sol_info.solMask & currBit) {
			sol_info.solOutMask |= currBit;
		}
		// next
		sol++;
		solState_p++;
		currBit <<= 1;
	}

#if OPP_DEBUG_PORT != 0
	sol_info.solOutMask &= DBG_MASK;
#endif

	digital_config_wing_gpio(wing, inputMask_p, inputPDMask_p, outputMask_p);

}




/*
 * ===============================================================================
 *
 * Name: solenoid_sol_to_wing
 *
 * ===============================================================================
 *
 * Retrieve the wing number for a given solenoid number
 *
 * @param   sol - Solenoid number.
 * @return  Wing number or -1 if not found.
 * ===============================================================================
 */
int solenoid_sol_to_wing(UINT sol) {
	INT wing;
	RS232_WING_TYPE_E *wingCfg_p;

	if (sol >= RS232_NUM_SOL) return -1;
	// let us assume a WING_SOL_8 conf
	wing = sol >> 3; // sol / 8
	wingCfg_p = opp_info.nvCfgInfo.wingCfg + wing;
	if (*wingCfg_p == WING_SOL_8) return wing;
	// so it must be a WING_SOL
	if (sol >= RS232_NUM_SOL_LEGACY) return -1;
	wing = sol >> 2; // sol / 4
	wingCfg_p = opp_info.nvCfgInfo.wingCfg + wing;
	if (*wingCfg_p == WING_SOL) return wing;
	return -1;
}




/*
 * ===============================================================================
 *
 * Name: solenoid_set_kick_pwm
 *
 * ===============================================================================
 *
 * Set a solenoid's power during the kick portion.  PWM is a number from 0-32
 * where 0 is off, and 32 is 100% power.
 * If PWM is 0xff, this will take the PWM already configured (used by the init code).
 *
 * @param   solIndex index of solenoid (0-31)
 * @param   kickPwm value from 0-32 or x0ff for use conf
 * @return  None
 * ===============================================================================
 */
void solenoid_set_kick_pwm(U8 solIndex, U8 kickPwm) {
	SOL_CFG_T *solCfg_p;

	if (solIndex < RS232_NUM_SOL) {
		solCfg_p = &opp_info.solDrvCfg_p->solCfg[solIndex];
		if (kickPwm == 0xff) {
			kickPwm = solCfg_p->kickPwm;
		} else {   // save to conf
			solCfg_p->kickPwm = kickPwm;
		}
		sol_info.solState[solIndex].kickIntenMask = PWM_MASK[kickPwm & 0x1f];
	}
}




/*
 * ===============================================================================
 *
 * Name: solenoid_upd_cfg
 *
 * ===============================================================================
 *
 * Update a solenoid configuration, regarding input bits, kick pwm and state.
 * This is called at init time (for all possible solenoids), and by some protocol commands during the game
 *
 * @param   updMask - Mask of solenoids to be updated.
 * @return  None
 * ===============================================================================
 */
void solenoid_upd_cfg(U32 updMask) {
	INT index;
	INT wing;
	INT currBit;
	INT inputBitIndex;
	SOL_STATE_T *solState_p;
	SOL_CFG_T *solDrvCfg_p;
	U32 inputBit;


	/* Clear the solenoid state machines */
	updMask &= sol_info.solMask; // only configured solenoid may be updated
	for (index = 0, solState_p = &sol_info.solState[0], currBit = 1;
			index < RS232_NUM_SOL;
			index++, solState_p++, currBit <<= 1) {
		if ((updMask & currBit) != 0) {
			// Solenoid should be part of a WING_SOL/WING_SOL_8 type wing ?
			wing = solenoid_sol_to_wing(index);
			if (wing == -1) continue;
			solDrvCfg_p = &opp_info.solDrvCfg_p->solCfg[index];
			// honor USE_SWITCH only for WING_SOL type
			if (opp_info.nvCfgInfo.wingCfg[wing] == WING_SOL) {
				// On this type of wing, low nibble are input pins, high nibble output pins. So inputBitIndex is
				// inputBitIndex = wing x 8 + (index modulo 4)
				// remember that index < 16 here
				// legacy formula: inputBitIndex = ((index & 0x0c) << 1) + (index & 0x03);
				inputBitIndex = (wing << 3) + (index & 0b11);
				inputBit = 1 << inputBitIndex;
				if (solDrvCfg_p->cfg & USE_SWITCH) {
					digital_input_state_reset(inputBitIndex);
					/* Don't set certain input bits for NeoSol and SPI clock if configured
					  	TODO: disSolInp is supposed to contain input bits used to operate solenoids, but in fact
					  	it also affects solenoid output for SPI2/MOSI which is on the output nibble, not input !*/
					if (digital_is_input(inputBitIndex)) {
						// some pin may have been re-assigned (like debug, neo, etc)
						solState_p->inpBits |= inputBit;
					}
				} else {
					solState_p->inpBits &= ~inputBit;
				}
			}
			// Add configured input bit if any
			solenoid_set_input(index, 0xff); // use the configured value
			// Set configured PWM
			solenoid_set_kick_pwm(index, 0xff);
		}
	}
}


/*
 * ===============================================================================
 *
 * Name: solenoid_set_input
 *
 * ===============================================================================
 *
 * Set a solenoid input.  To disable a solenoid, the input solenoid number can be
 * set to SOL_INP_CLEAR_SOL. 0xff is used to reset inputs to the saved conf (used by init code).
 * If multiple inputs are used to fire a solenoid, the inputs are logically OR'd together.
 *
 * @param   solIndex index of solenoid (0-31)
 * @param   inpIndex index of (0-31) for regular inputs, (32-95) for matrix switch, and 0xff for use conf.
 * @return  None
 * ===============================================================================
 */
void solenoid_set_input(U8 solIndex, U8 inpIndex) {
	U32 sol;
	SOL_STATE_T *solState_p;
	SOL_CFG_T *solCfg_p;

	sol = solIndex & SOL_INP_SOL_MASK;
	if (sol >= RS232_NUM_SOL) return;

	solState_p = &sol_info.solState[sol];
	solCfg_p = &opp_info.solDrvCfg_p->solCfg[sol];
	if (inpIndex == 0xff) {
		// use the conf value (add, do not replace. see digital_upd_sol_cfg())
		solState_p->inpBits |= solCfg_p->inpBits;
	} else if (inpIndex < RS232_NUM_INP) {
		// a normal switch
		if (digital_is_input(inpIndex)) {
			// some pin may have been re-assigned (like debug, neo, etc)
			if ((solIndex & SOL_INP_CLEAR_SOL) == 0) {
				solState_p->inpBits |= (1 << inpIndex);
			} else {
				solState_p->inpBits &= ~(1 << inpIndex);
			}
			// save to conf
			solCfg_p->inpBits = solState_p->inpBits;
		}
	} else if ((inpIndex - RS232_NUM_INP) < RS232_SW_MATRX_INP) {
		/* Inputs 32 to 96 are from the switch matrix (8x8), first verify
		 * there is a switch matrix.
		 */
		if ((opp_info.typeWingBrds & (1 << WING_SW_MATRIX_IN)) != 0) {
			if (solIndex & SOL_INP_CLEAR_SOL) {
				swmtrx_set_sol_info(inpIndex - RS232_NUM_INP, 0);
			} else {
				swmtrx_set_sol_info(inpIndex - RS232_NUM_INP, MATRIX_FIRE_SOL | sol);
			}
			// TODO save to conf
		}
	}
}



/*
 * ===============================================================================
 *
 * Name: solenoid_upd_inp_cfg
 *
 * ===============================================================================
 *
 * Update an input configuration.
 *
 * @param   updMask - Mask of inputs to be updated.
 * @return  None
 * ===============================================================================
 */
void solenoid_upd_inp_cfg(U32 updMask) {
	INT index;
	INT currBit;
	INP_CFG_T *inpCfg_p;

	/* Clear the input counts */
	for (index = 0, inpCfg_p = &opp_info.inpCfg_p->inpCfg[0], currBit = 1;
			index < RS232_NUM_INP;
			index++, inpCfg_p++, currBit <<= 1) {
		if ((updMask & currBit) != 0) {
			// inpState_p->cnt = 0;
			digital_input_reset_state(index);
			if (inpCfg_p->cfg < SERVO_OUTPUT_THRESH) {
				if (inpCfg_p->cfg == STATE_INPUT) {
					// dig_info.stateMask |= currBit;
					digital_input_state_mask_set(currBit);
				} else {
					//dig_info.stateMask &= ~currBit;
					digital_input_state_mask_clear(currBit);
				}
			}
		}
	}
}


// Drive a solenoid by software (rather than by a switch)
void solenoid_drive(U32 value, U32 solMask) {
	value &= solMask; // solenoids to kick should be within masked bits !
	sol_info.solDrvProcCtl = (sol_info.solDrvProcCtl & ~solMask) | value;
}


// This function was introduced for rs232proc debug command: Get Solenoid state, 0xe0
UINT solenoid_get_state(U8 solIndex) {
	SOL_STATE_T *solState_p;

	assert(solIndex < RS232_NUM_SOL);
	solState_p = &sol_info.solState[solIndex];
	return solState_p->solState;
}



/*
 * ===============================================================================
 *
 * Name: solenoid_task
 *
 * ===============================================================================
 *
 * Solenoid processing
 *
 * @param   inputs the current input bits (32)
 * @param   updFilterLow the filtered inputs (debounced) that have become Low (viz. active)
 * @return  None
 * ===============================================================================
 */
void solenoid_task(U32 inputs, U32 updFilterLow) {
	U32 currMsMask;
	SOL_STATE_T *solState_p;
	SOL_CFG_T *solCfg_p; // pointer to a copy of the solenoid config at state changed from IDLE.
	INT index;
	U32 currBit;
	BOOL isDrvProcCtl;
	BOOL isIntputCtl;
	U32 updateMask;
	U32 updateValue;

	INT elapsedTimeMs;

	currMsMask = 1 << ((timer_get_ms_count() & 0xf) * 2);
	if (timer_get_us_count() >= 500) {
		currMsMask <<= 1;
	}
	/* Update sol output bits every time */
	updateMask = sol_info.solOutMask;
	updateValue = 0;
	for (index = 0, currBit = 1, solState_p = &sol_info.solState[0], solCfg_p = sol_info.savedSolCfg;
			index < RS232_NUM_SOL;
			index++, currBit <<= 1, solState_p++, solCfg_p++) {

		switch (solState_p->solState) {

			case SOL_STATE_IDLE:
				// Note that solCfg_p points to a config copy (not necesseraly initialized yet)
				/* Check if processor is requesting a kick, or an input changed */
				isDrvProcCtl = (sol_info.solDrvProcCtl & currBit) != 0;
				isIntputCtl = (updFilterLow & solState_p->inpBits) != 0;
				if (isDrvProcCtl || isIntputCtl) {
					// Save the configuration, because it may change before the state returns to IDLE (though reconfiguration by host)
					*solCfg_p = opp_info.solDrvCfg_p->solCfg[index];
					// Record how we are triggered
					solState_p->inputCtl = isIntputCtl; // priority given to input over soft. This is important.
					/* Check if processor is kicking normal solenoid */
					if ((solCfg_p->cfg & (ON_OFF_SOL | DLY_KICK_SOL)) == 0) {
						/* Start the solenoid kick */
						solState_p->solState = SOL_INITIAL_KICK;
						solState_p->startMs = timer_get_ms_count();
						updateValue |= solState_p->bit;
					} else if ((solCfg_p->cfg & ON_OFF_SOL) != 0) {
						solState_p->solState = SOL_FULL_ON_SOLENOID;
						updateValue |= solState_p->bit;
					} else if ((solCfg_p->cfg & DLY_KICK_SOL) != 0) {
						solState_p->solState = SOL_WAIT_BEFORE_KICK;
						solState_p->startMs = timer_get_ms_count();
					}
					// For AUTO_CLR, if the command is from software, we must reset the software command,
					// to allow the coil be turned off, obviously by timer
					if ((solCfg_p->cfg & AUTO_CLR) && (sol_info.solDrvProcCtl & currBit)) {
						sol_info.solDrvProcCtl &= ~currBit; // stop software command
						solState_p->clearRcvd = TRUE; // to abort SOL_SUSTAIN_PWM state ?
					} else {
						solState_p->clearRcvd = FALSE;
					}
				}
				break;

			case SOL_INITIAL_KICK:
				if ((solCfg_p->cfg & CAN_CANCEL) != 0) {
					// CAN_CANCEL indicates, for solenoids configured with inputs, that if inputs are not active, one must cancel the action, viz. putting it to IDLE.
					if ((sol_info.solDrvProcCtl & currBit) == 0) { // not driven ON by software
							if (solState_p->inpBits && ((solState_p->inpBits & inputs) == solState_p->inpBits)) { // input configured and none is active (remember active low)
								/* Switch is inactive, move to idle */
								solState_p->solState = SOL_STATE_IDLE;
							}
					}
				}
				if (solState_p->solState == SOL_INITIAL_KICK) {
					/* Check if elapsed time is over initial kick time */
					elapsedTimeMs = timer_get_ms_count() - solState_p->startMs;
					if (elapsedTimeMs >= solCfg_p->initKick) {
						/* If this is a normal solenoid (not ON_OFF, nor a DLY_KICK, nor USE_MATRIX_INP) */
						// At this stage, we do not bother whether SOL_MIN_TIME_OFF stage will do something or not
						//if ((solCfg_p->cfg & (ON_OFF_SOL | DLY_KICK_SOL | USE_MATRIX_INP)) == 0) {
						if ((solCfg_p->cfg & (DLY_KICK_SOL | USE_MATRIX_INP)) == 0) {
							/* See if this has a sustaining PWM */
							if (solCfg_p->minOffDuty & DUTY_CYCLE_MASK) {
								/* Make sure the input continues to be set */
								if (solState_p->clearRcvd) {
									solState_p->solState = SOL_MIN_TIME_OFF;
									solState_p->offCnt = 0;
								} else {
									/* Grab holdIntenMask */
									solState_p->solState = SOL_SUSTAIN_PWM;
									solState_p->holdIntenMask = PWM_MASK[(((solCfg_p->minOffDuty & DUTY_CYCLE_MASK) - 1) * 2) + 1];
									if (currMsMask & solState_p->holdIntenMask) {
										updateValue |= solState_p->bit;
									}
								}
							} else {
								solState_p->solState = SOL_MIN_TIME_OFF;
								solState_p->offCnt = 0;
							}
						} else if ((solCfg_p->cfg & (DLY_KICK_SOL | USE_MATRIX_INP)) != 0) {
							// A DLY_KICK_SOL or USE_MATRIX_INP solenoid
							solState_p->solState = SOL_MIN_TIME_OFF;
							solState_p->offCnt = 0;
						}
						// For all solenoid types: restart the timer for next state
						solState_p->startMs = timer_get_ms_count();
					} // initKick not expired
					  else if (currMsMask & solState_p->kickIntenMask) { // TODO explain test
						updateValue |= solState_p->bit; // keep it ON
					}
				}
				break;

			case SOL_SUSTAIN_PWM:
				if (((sol_info.solDrvProcCtl & currBit) == 0)
						&& ((solState_p->inpBits == 0) || ((solState_p->inpBits & inputs) == solState_p->inpBits))) {
					solState_p->clearRcvd = TRUE;
				}
				if (!solState_p->clearRcvd) {
					/* Do faster PWM function by testing us timer */
					if (currMsMask & solState_p->holdIntenMask) {
						updateValue |= solState_p->bit;
					}
				} else {
					/* Switch is inactive, move to idle */
					solState_p->solState = SOL_STATE_IDLE;
					sol_info.solDrvProcCtl &= ~currBit; // cancel any software ON command, may be pending
				}
				break;

			case SOL_MIN_TIME_OFF:
				/* Check if an off time increment has happened */
				elapsedTimeMs = timer_get_ms_count() - solState_p->startMs;
				if (elapsedTimeMs >= solCfg_p->initKick) {
					solState_p->offCnt += MIN_OFF_INC;
					if (solState_p->offCnt >= (solCfg_p->minOffDuty & MIN_OFF_MASK)) {
						solState_p->solState = SOL_STATE_IDLE;
						sol_info.solDrvProcCtl &= ~currBit; // cancel any software ON command, may be pending
					} else {
						solState_p->startMs = timer_get_ms_count();
					}
				}
				break;

			case SOL_WAIT_BEFORE_KICK:
				/* Check if elapsed time is over the wait time
				 * (stored in duty cycle nibble * 2)
				 */
				elapsedTimeMs = timer_get_ms_count() - solState_p->startMs;
				if (elapsedTimeMs >= ((solCfg_p->minOffDuty & DUTY_CYCLE_MASK) << 1)) {
					/* Start the solenoid kick */
					solState_p->solState = SOL_INITIAL_KICK;
					solState_p->startMs = timer_get_ms_count();
					updateValue |= solState_p->bit;
				}
				break;

			case SOL_FULL_ON_SOLENOID:
				if (solState_p->inputCtl) { // This was activated by input
					if ((solState_p->inpBits & inputs) == solState_p->inpBits) { // input all active
						/* Switch is inactive, move to idle */
						solState_p->solState = SOL_STATE_IDLE;
						sol_info.solDrvProcCtl &= ~currBit; // cancel any software ON command, may be pending
					} else if (currMsMask & solState_p->kickIntenMask) { // TODO explain test
						updateValue |= solState_p->bit;
					}
				} else { // Activated by soft
					if ((sol_info.solDrvProcCtl & currBit) == 0) { // not driven ON by software
						solState_p->solState = SOL_STATE_IDLE;
					} else if (currMsMask & solState_p->kickIntenMask) { // TODO explain test
						updateValue |= solState_p->bit;
					}
				}
				break;
		}
	}
	digital_output_upd(updateValue, updateMask);
}

