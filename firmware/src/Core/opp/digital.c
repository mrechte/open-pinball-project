/**
 * This module controls the digital configuration of all wing type boards.
 * It directly manages switch inputs and regular outputs.
 * Specialized outputs like neopixel, servo are managed by their own modules.
 * GPIO pin are configured for all wing type boards either by this module, and or by a module
 * This module also run modules proc if necessary.
 * TODO the loop duration depends on the number of proc run, therefore one should not account for a constant loop period (like for instance to debounce a switch)
 *
 * There are 4 wings (0-3), each having 8 logical pins, totalizing 32 *logical* pins.
 * Wing 0 bears bits 0 - 7
 * Wing 1 bears bits 8 - 15
 * Wing 2 bears bits 16 - 23
 * Wing 3 bears bits 24 - 31
 *
 * The below schema is a rough view of the pin position (knowing that pin 0 and 16 are not at the right location)
 *
 * Wing   0 PA13 -+ +--+-----+--+ +- 31 PB11 Wing
 * (0/A)  1 PB12  | |  | USB |  | |  30 PB10 (3/D)
 *        2 PB13  | |  +-----+  | |  29 PB01
 *        3 PB14  | |           | |  28 PB00
 *        4 PB15  | |   BLUE    | |  27 PA07
 *        5 PA00  | |   PILL    | |  26 PA06
 *        6 PA09  | |           | |  25 PA05
 *        7 PA10 -+ |           | +- 24 PA04
 * Wing   8 PA15 -+ |           | +- 23 PA03 Wing
 * (1/B)  9 PB03  | |           | |  22 PA02 (2/C)
 *       10 PB04  | |           | |  21 PA01
 *       11 PB05  | |           | |  20 PA00
 *       12 PB06  | |           | |  19 PC15
 *       13 PB07  | |           | |  18 PC14
 *       14 PB08  | |           | |  17 PC13
 *       15 PB09 -+ +-----------+ +- 16 PA01
 *
 * Some wings have specialized functions:
 * Wing 0: may be configured as WING_NEO or WING_NEO_SOL or WING_LAMP_MATRIX_COL
 * Wing 1: one pin may be configured as servo output although type is WING_INP or WING_SOL. It may be configured as WING_LAMP_MATRIX_ROW
 * Wing 2: may be configured as WING_SW_MATRIX_OUT[_LOW]
 * Wing 3: may be configured as WING_SW_MATRIX_IN or WING_NEO
 *
 * Warning: if OPP_DEBUG_PORT is TRUE, pin 0 (wing 0) and 16 (wing 2) should be disabled (remove jumpers)
 *
 * WING_INP
 * --------
 * This is 8-input wing configured for switches
 *
 * WING_SW_MATRIX_IN & WING_SW_MATRIX_OUT (WING_SW_MATRIX_OUT_LOW)
 * ---------------------------------------------------------------
 * This is an 8x8 input switch matrix. It requires 2 wings (2 and 3).
 *
 * WING_LAMP_MATRIX_COL & WING_LAMP_MATRIX_ROW
 * -------------------------------------------
 * This is an 8x8 output lamp matrix. It requires 2 wings (0 and 1).
 *
 * WING_INCAND
 * -----------
 * This is 8-output wing configured for lamp drive.
 *
 * WING_SOL
 * --------
 * This is combo of 4 inputs (pins 0 - 3) and 4 outputs (pins 4 - 7)
 * When configured as USE_SWITCH:
 * - input pin 0 drives output pin 4
 * - input pin 1 drives output pin 5
 * - input pin 2 drives output pin 6
 * - input pin 3 drives output pin 7
 * Note that this assignment may be changed if required using RS232I_SET_SOL_INPUT command.
 *
 * WING_SOL_8
 * ----------
 * This is an 8-output wing configured for solenoid drive.
 * There is no preallocated input pins (USE_SWITCH is meaningless for this wing type).
 * Input assignment has to be done using RS232I_SET_SOL_INPUT command.
 * Note that the same hardware may configured as WING_INCAND if intend use is to drive lamps.
 *
 * A note regarding solenoid numbering:
 * - Legacy WING_SOL use number 0-15, according to wing location, which means it does not respect the SWITCH or INCAND *logical* numbering
 *	 convention (usually printed on the PCB).
 * - WING_SOL_8 use number 0-31, using same convention than SWITCH and INCAND.
 *
 * WING_NEO
 * --------
 * This is used to drive a 3-wire neopixel LED string based on WS2811/WS2812 chips.
 * It may optionally be used for 4-wire true SPI LEDs based on SK9822/APA102 chips.
 * This type may be used on wing 0 (using SPI2 IP) or wing 3 (SPI1).
 * Same as WING_INP, except that pin 4 (or pin 3 wing 3 on SPI1) is configured as an output to drive the LED string.
 * Optionally pin 2 (or pin 1 wing 3 on SPI1) may be configured as an output (clock) for true SPI LED string.
 * All other wing pins are configured as inputs.
 *
 * WING_NEO_SOL
 * ------------
 * Same principle than WING_NEO, except that other pins are configured as legacy WING_SOL.
 *
 * WING_SPI
 * --------
 * TODO
 *
 *
 *===============================================================================
 */

#include <stdlib.h>
#include <string.h>

#include "stm32f1xx_hal.h"

#include "stdtypes.h"
#include "timer.h"

#include "opp.h"

#include "digital.h"
#include "swmtrx.h"
#include "lampmtrx.h"
#include "solenoid.h"
#include "incand.h"
#include "mcu.h"         /* for EnableInterrupts macro */
#include "neopxl.h"
#include "servo.h"
#include "stdl.h"

// Usable bits as input without / with debug, on a given INPUT wing
#define INPUT_BIT_MASK        0xff

// switch debounce time default value (in us).
#define SWITCH_THRESH         5000


typedef struct {
	GPIO_TypeDef *port_p;
	uint16_t GPIO_Pin;
} DIG_PIN_INFO_T;

const DIG_PIN_INFO_T dig_pinInfo[RS232_NUM_INP] =	{
	// Wing 0
	{ GPIOA, GPIO_PIN_13 }, { GPIOB, GPIO_PIN_12 }, { GPIOB, GPIO_PIN_13 }, { GPIOB, GPIO_PIN_14 },
	{ GPIOB, GPIO_PIN_15 }, { GPIOA, GPIO_PIN_8 }, { GPIOA, GPIO_PIN_9 }, { GPIOA, GPIO_PIN_10 },
	// Wing 1
	{ GPIOA, GPIO_PIN_15 }, { GPIOB, GPIO_PIN_3 }, { GPIOB, GPIO_PIN_4 }, { GPIOB, GPIO_PIN_5 },
	{ GPIOB, GPIO_PIN_6 }, { GPIOB,	GPIO_PIN_7 }, { GPIOB, GPIO_PIN_8 }, { GPIOB, GPIO_PIN_9 },
	// Wing 2
	{ GPIOA, GPIO_PIN_14 }, { GPIOC, GPIO_PIN_13 }, { GPIOC, GPIO_PIN_14 }, { GPIOC, GPIO_PIN_15 },
	{ GPIOA, GPIO_PIN_0 }, { GPIOA,	GPIO_PIN_1 }, { GPIOA, GPIO_PIN_2 }, { GPIOA, GPIO_PIN_3 },
	// Wing 3
	{ GPIOA, GPIO_PIN_4 }, { GPIOA, GPIO_PIN_5 }, { GPIOA, GPIO_PIN_6 }, { GPIOA, GPIO_PIN_7 },
	{ GPIOB, GPIO_PIN_0 }, { GPIOB,	GPIO_PIN_1 }, { GPIOB, GPIO_PIN_10 }, { GPIOB, GPIO_PIN_11 }
};


/*
 * This structure maintains the information required to manage an input
 */
typedef struct {
	UINT debounceTS; // timestamp (msus) of state change stabilization (debounce feature).
} DIG_INP_STATE_T;


/*
 * This structure maintains the conf and state of digital input / output bits.
 * There are 32 logical pins on a board. Out of these 32 pins, 16 may be defined as solenoid pins.
 * This is the wing type that governs which pin may be an input or output (the so called masks).
 */
typedef struct {
	// digital module manages 2 inputs bit types (with pull up or pull down resistor) and one output bit type
	// other specialized modules may repurpose some of these bits, in that case they will be cleared from these mask
	U32 inputMask;  // logical pins configured as inputs with Pull Up resistor
	U32 inputPDMask;  // logical pins configured as inputs with Pull Down resistor (matrix switches)
	U32 outputMask;  // logical pins configured as outputs

	U32 prevInputs; // previous input state. If an input state changes, it needs to be confirmed first (debounced)
	U32 filtInputs;  // last input read state (including matrix input) of 32 logical bits (output bits remains 0)
	U32 validSwitch; // switches that have been detected, but not yet reported to the end user (when configured as FALL_EDGE or RISE_EDGE)
	U16 validTS[RS232_NUM_INP]; // timestamp (ms) of input change state of validSwitch
	U32 updateMask; // bits (32) for output write (reset after each write)
	U32 updateValue; //  values (32) for each bit to write (reset after each write)
	U32 stateMask; // TBC input bits that trigger solenoid
	DIG_INP_STATE_T inpState[RS232_NUM_INP]; // 32 inputs

} DIG_GLOB_T;

DIG_GLOB_T dig_info;




void digital_input_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);
void digital_sol_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);
void digital_neo_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);
void digital_neo_sol_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);
void digital_set_solenoid_input(RS232_SET_SOL_INP_E inputIndex, U8 solIndex);
void digital_set_kick_pwm(U8 kickPwm, U8 solIndex);
void digital_upd_sol_cfg(U32 updMask);
void digital_upd_inp_cfg(U32 updMask);
void digital_output_upd(U32 value, U32 mask);


/*
 * Pointers to wing init function (called once per used wing)
 * Function to call at wing configuration time (command 0x0e) and wing init time (boot).
 */
void (*DIG_WING_INIT_FP[MAX_WING_TYPES])(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) = {
	NULL,                       /* WING_UNUSED */
    digital_sol_init,          	/* WING_SOL */
	digital_input_init,         /* WING_INP */
    incand_neo_init,         	/* WING_INCAND */
	swmtrx_wing_out_init,		/* WING_SW_MATRIX_OUT */
	swmtrx_wing_in_init, 		/* WING_SW_MATRIX_IN */
    digital_neo_init,           /* WING_NEO */
    incand_neo_init,         	/* WING_HI_SIDE_INCAND */
	digital_neo_sol_init,       /* WING_NEO_SOL */
	NULL,                       /* TODO WING_SPI */
	swmtrx_wing_out_init, 		/* WING_SW_MATRIX_OUT_LOW */
	NULL,                		/* TODO WING_LAMP_MATRIX_COL */
	NULL,                       /* TODO WING_LAMP_MATRIX_ROW */
    solenoid_sol_8_init    		/* WING_SOL_8 */
};


/*
 * Pointers to wing de-init function (called once per used wing)
 * Function to call at wing configuration time (command 0x0e) and erase config (command 0x0d).
 */
void (*DIG_WING_DEINIT_FP[MAX_WING_TYPES])(UINT wing) = {
	NULL,                       /* WING_UNUSED */
    NULL,          				/* WING_SOL */
	NULL,         				/* WING_INP */
	NULL,         				/* WING_INCAND */
	NULL,						/* WING_SW_MATRIX_OUT */
	NULL, 						/* WING_SW_MATRIX_IN */
    neopxl_neo_deinit,     		/* WING_NEO */
	NULL,         				/* WING_HI_SIDE_INCAND */
	neopxl_neo_deinit,       	/* WING_NEO_SOL */
	NULL,                       /* TODO WING_SPI */
	NULL, 						/* WING_SW_MATRIX_OUT_LOW */
	NULL,                		/* TODO WING_LAMP_MATRIX_COL */
	NULL,                       /* TODO WING_LAMP_MATRIX_ROW */
	NULL    					/* WING_SOL_8 */
};


/*
 * ===============================================================================
 *
 * Name: digital_*_init functions
 *
 * ===============================================================================
 *
 * Initialize function according to wing type
 *
 * Initialize digital I/O port, and the other digital control signals.
 *
 * @param   wing wing index (0-3)
 * @param   inputMask_p pointer to inputMask. Will set pins that have to be configured as inputs (32 bit)
 * @param   inputPDMask_p pointer to inputPDMask. Will set pins that have to be configured as inputs (32 bit) with pull down resistor
 * @param   outputMask_p pointer to outputMask. Will set pins that have to be configured as outputs (32 bit)
 * @return  None
 * ===============================================================================
 */


void digital_input_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	U32 mask;

	assert(wing < RS232_NUM_WING);
	if (!opp_info.validCfg) return;

	/* Set up bit mask of valid inputs */
	mask = INPUT_BIT_MASK << (wing << 3);
#if OPP_DEBUG_PORT != 0
	mask &= DBG_MASK;
#endif
	*inputMask_p |= mask;

	// make sure these bits are no longer outputs
	*outputMask_p &= ~mask;

	digital_config_wing_gpio(wing, inputMask_p, inputPDMask_p, outputMask_p);

	// may also drive a servo => TODO not to move to a special wing type ?
	if (wing == 1) servo_servo_init(wing, inputMask_p, inputPDMask_p, outputMask_p);
}


// Legacy solenoid (with input switches)
void digital_sol_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {

	solenoid_sol_init(wing, inputMask_p, inputPDMask_p, outputMask_p);

	// may also drive a servo => TODO not to move to a special wing type ?
	if (wing == 1) servo_servo_init(wing, inputMask_p, inputPDMask_p, outputMask_p);
}


// A combination of inputs and neo LED string on WING 0
void digital_neo_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	assert(wing == 0 || wing == 3);

	digital_input_init(wing, inputMask_p, inputPDMask_p, outputMask_p);

	neopxl_neo_init(wing, inputMask_p, inputPDMask_p, outputMask_p);
}




// A combination of legacy solenoid and neo LED string on WING 0
void digital_neo_sol_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	assert(wing == 0);

	solenoid_sol_init(wing, inputMask_p, inputPDMask_p, outputMask_p);

	neopxl_neo_init(wing, inputMask_p, inputPDMask_p, outputMask_p);
}



void digital_config_wing_gpio(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	INT bitIndex, i;
	GPIO_InitTypeDef pinCfg;
	BOOL usedBit;

	assert(wing < RS232_NUM_WING);

	for (bitIndex = wing << 3, i = 0; i < 8; bitIndex++, i++) {

		usedBit = FALSE;
		memset(&pinCfg, 0, sizeof(pinCfg)); // reset structure
		pinCfg.Pin = dig_pinInfo[bitIndex].GPIO_Pin;
		if ((*inputMask_p & (1 << bitIndex)) != 0) {
			pinCfg.Mode = GPIO_MODE_INPUT;
			pinCfg.Pull = GPIO_PULLUP;
			usedBit = TRUE;
		} else if ((*inputPDMask_p & (1 << bitIndex)) != 0) {
			pinCfg.Mode = GPIO_MODE_INPUT;
			pinCfg.Pull = GPIO_PULLDOWN;
			usedBit = TRUE;
		} else if ((*outputMask_p & (1 << bitIndex)) != 0) {
			pinCfg.Speed = GPIO_SPEED_FREQ_LOW;
			pinCfg.Mode = GPIO_MODE_OUTPUT_PP;
			usedBit = TRUE;
		}
		if (usedBit) {
			HAL_GPIO_Init(dig_pinInfo[bitIndex].port_p, &pinCfg);
		}
	}
}


/*
 * ===============================================================================
 * 
 * Name: digital_init
 * 
 * ===============================================================================
 *
 * Initialize the digital I/O port
 * 
 * Initialize digital I/O port, and the other digital control signals.
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void digital_init(void) {
	INT index;
	RS232_WING_TYPE_E wingType;
	BOOL foundSol = FALSE; // whether we have found a WING_SOL type wing
	BOOL foundSol8 = FALSE; // whether we have found a WING_SOL_8 type wing
	U32 outputMask = 0; // out of the 32 logical pins, indicate which one are outputs
	U32 inputMask = 0; // out of the 32 logical pins, indicate which one are inputs with Pull Up resistor
	U32 inputPDMask = 0; // out of the 32 logical pins, indicate which one are inputs with Pull Down resistor

	// Reset structure
	memset(&dig_info, 0, sizeof(dig_info));

	// all modules should be inited when wing conf function is called

	/* Set the location of the input configuration data
	 opp_info.freeCfg_p was initialized to &opp_info.nvCfgInfo.cfgData[0] in main_copy_flash_to_ram()
	 remember that cfgData is U8[0xf0] of variable config data: input configs, followed by solenoid configs, followed by color table.
	 opp_info.inpCfg_p points into cfgData pertaining to input cfg
	 then will follow opp_info.solDrvCfg_p for data pertaining to solenoid cfg
	 finally will follow opp_info.neoCfg_p for color table */

	opp_info.freeCfg_p = &opp_info.nvCfgInfo.cfgData[0];

	/* Set the location of input configuration data */
	opp_info.inpCfg_p = (OPP_INP_CFG_T*) opp_info.freeCfg_p;
	opp_info.freeCfg_p += sizeof(OPP_INP_CFG_T);

	/* Set the location of solenoid configuration data
	 *  opp_info.solDrvCfg_p, should not be left null, because one may send a CONFIG_SOL cmd after a SET_CFG that changes the Wings type.
	 *  we also need the solenoid configuration data now
	 */
	opp_info.solDrvCfg_p = (OPP_SOL_CFG_T*) opp_info.freeCfg_p;
	opp_info.freeCfg_p += sizeof(OPP_SOL_CFG_T);

	opp_info.neoCfg_p = (OPP_NEO_CFG_T*) opp_info.freeCfg_p;
	opp_info.freeCfg_p += sizeof(OPP_NEO_CFG_T);

	/* Set up digital ports, walk through wing boards */
	for (index = 0; index < RS232_NUM_WING; index++) {
		wingType = opp_info.nvCfgInfo.wingCfg[index];

		switch (wingType) {


			case WING_SOL:
				/* this wing board is a solenoid driver (legacy type) */
				// one cannot have a mix of legacy / S0L_8 types
				if (!foundSol8) {
					foundSol = TRUE;
				} else {
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;

			case WING_SOL_8:
				/* this wing board is a solenoid driver (8 drivers type) */
			// one cannot have a mix of legacy / S0L_8 types
				if (!foundSol) {
					foundSol8 = TRUE;
				} else {
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;

			case WING_SW_MATRIX_OUT:
			case WING_SW_MATRIX_OUT_LOW:
				/* if wing 2 is WING_SW_MATRIX_OUT[_LOW], check wing 3 is WING_SW_MATRIX_IN.
				 *   Other positions are not allowed. */
				if ((index != 2) || (opp_info.nvCfgInfo.wingCfg[3] != WING_SW_MATRIX_IN)) {
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;

			case WING_NEO:
				/* Neo wing can only be wing 0 or 3 */
				if ((index != 0) && (index != 3)){
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;

			case WING_NEO_SOL:
				/* NeoSol wing can only be wing 0 */
				if (index == 0) {
					foundSol = TRUE;
				} else {
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;


			case WING_LAMP_MATRIX_COL:
				/* if wing 0 is WING_LAMP_MATRIX_COL, check wing 1 is WING_LAMP_MATRIX_ROW.
				 *   Other positions are not allowed. */
				if ((index == 0)  && (opp_info.nvCfgInfo.wingCfg[1] == WING_LAMP_MATRIX_ROW)) {
					// TODO digital_init_wing_incand(index, &updateMask); + lampmtrx_init() + WING_LAMP_MATRIX_ROW
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				} else {
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;

			case WING_LAMP_MATRIX_ROW:
				if ((index == 1)  && (opp_info.nvCfgInfo.wingCfg[0] == WING_LAMP_MATRIX_COL)) {
					// TODO digital_init_wing_incand(index, &updateMask);  + lampmtrx_init() + WING_LAMP_MATRIX_COL
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				} else {
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;


			case WING_SW_MATRIX_IN:
				if ((index != 3) || ! (opp_info.nvCfgInfo.wingCfg[2] == WING_SW_MATRIX_OUT || opp_info.nvCfgInfo.wingCfg[2] == WING_SW_MATRIX_OUT_LOW)) {
					// reset to WING_UNUSED
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;

			case WING_SPI: // Not tested / supported
				opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				break;

			default:
				// Just check it is a valid wing type
				if (wingType < 0 || wingType >= MAX_WING_TYPES) {
					opp_info.nvCfgInfo.wingCfg[index] = WING_UNUSED;
				}
				break;

		}
	}

    /* Walk through the wing boards and create bit mask of wing board types */
	// some wing type may have been reset to WING_UNUSED, so wait here to set opp_info.typeWingBrds
	opp_info.typeWingBrds = 0;
    for (index = 0; index < RS232_NUM_WING; index++) {
       if (opp_info.nvCfgInfo.wingCfg[index] != WING_UNUSED) {
      	// we keep track of all sorts of wing type we have to manage in this config
          opp_info.typeWingBrds |= (1 << opp_info.nvCfgInfo.wingCfg[index]);
       }
    }

    // call the wing init function
    // note that each call will add or remove bits to inputMask, updateMask
    // one has to pay attention for instance on a WING_SOL which reconfigure some inputs
    // or WING_NEO which reconfigure an input as an output, which may no longer be used as an input for WING_SOL...
    // at the end those two masks should reflect bits configured as input and output, not that 2 bits may be missing if debug is on.
	// Note also these functions have to take into consideration debug pins using DBG_MASK
    for (index = 0; index < RS232_NUM_WING; index++) {
    	wingType = opp_info.nvCfgInfo.wingCfg[index];
		if (DIG_WING_INIT_FP[wingType]) DIG_WING_INIT_FP[wingType](index, &inputMask, &inputPDMask, &outputMask);
    }

	dig_info.inputMask = inputMask;
	dig_info.inputPDMask = inputPDMask;
	dig_info.outputMask = outputMask;

	// Initial read of all pin configured as input
	dig_info.filtInputs = stdldigio_read_all_ports(inputMask | inputPDMask);


	/* Set up the initial state */
	solenoid_upd_cfg(0xffffffff);
	solenoid_upd_inp_cfg(inputMask);

}


void digital_deinit(void) {
	INT index;
	RS232_WING_TYPE_E wingType;

	if (!opp_info.validCfg) return;

    for (index = 0; index < RS232_NUM_WING; index++) {
    	wingType = opp_info.nvCfgInfo.wingCfg[index];
		if (DIG_WING_DEINIT_FP[wingType]) DIG_WING_DEINIT_FP[wingType](index);
    }
}


/*
 * ===============================================================================
 *
 * Name: digital_input_proc
 *
 * ===============================================================================
 *
 * Input processing
 *
 * This is called first, in order to debounce inputs
 *
 * @param   inputs the current input bits (32)
 * @param   updFilterLow_p pointer to the filtered inputs (debounced) that have become Low
 * @param   updFilterHi_p pointer to the filtered inputs (debounced) that have become High
 * @return  None
 * ===============================================================================
 */
void digital_input_task(U32 inputs, U32 *updFilterHi_p, U32 *updFilterLow_p) {
	DIG_INP_STATE_T *inpState_p;
	INP_CFG_T *cfg_p;
	U32 changedBits;
	INT debounce; // in us
	INT index;
	U32 currBit;
	UINT currentTS, delta_ms;
	INT delta_us; // indeed UINT

	/* See what bits have changed */
	changedBits = (dig_info.prevInputs ^ inputs) & dig_info.inputMask;
	*updFilterHi_p = 0;
	*updFilterLow_p = 0;
	currentTS = timer_get_msus_count();
	/* Perform input processing for both input and solenoid boards */
	for (index = 0, currBit = 1, inpState_p = &dig_info.inpState[0];
			index < RS232_NUM_INP;
			index++, currBit <<= 1, inpState_p++) {
		if (currBit & dig_info.inputMask) {
			/* Check if this count has changed */
			if (changedBits & currBit) {
				// yes it changed, reset debounce timestamp
				inpState_p->debounceTS = currentTS;
			} else if (inpState_p->debounceTS != 0) { // not yet debounced
				delta_ms = (currentTS >> 16) - (inpState_p->debounceTS >> 16);
				delta_us = (currentTS & 0xffff) - (inpState_p->debounceTS & 0xffff);
				if (delta_us < 0) {
					delta_us += 1000;
					assert(delta_us >= 0);
					delta_ms++;
				}
				delta_us += 1000*delta_ms;
				cfg_p = &opp_info.inpCfg_p->inpCfg[index];
				debounce = cfg_p->debounce * 100;
				if (! debounce) debounce = SWITCH_THRESH; // default value if not set
				if (delta_us >= debounce) {
					// switch state is now debounced
					inpState_p->debounceTS = 0;

					if (inputs & currBit) {
						// switch is in high state
						*updFilterHi_p |= currBit;
						dig_info.filtInputs |= currBit;
						if (cfg_p->cfg == RISE_EDGE) {
							//DisableInterrupts;
							dig_info.validSwitch |= currBit;
							//EnableInterrupts;
							dig_info.validTS[index] = (U16) timer_get_ms_count();
						}
					} else {
						// switch is in low state
						*updFilterLow_p |= currBit;
						dig_info.filtInputs &= ~currBit;
						if (cfg_p->cfg == FALL_EDGE) {
							// DisableInterrupts;
							dig_info.validSwitch |= currBit;
							// EnableInterrupts;
							dig_info.validTS[index] = (U16) timer_get_ms_count();
						}
					}
				}
			}
		}
	}
}



U16 *digital_get_input_ts_buf() {
	return dig_info.validTS;
}

// for solenoid.c
void digital_input_reset_state(U8 input) {
	dig_info.inpState[input].debounceTS = 0;
}

// for solenoid.c
void digital_input_state_mask_set(INT bit) {
	dig_info.stateMask |= bit;
}

// for solenoid.c
void digital_input_state_mask_clear(INT bit) {
	dig_info.stateMask &= ~bit;
}

// for solenoid.c
void digital_input_state_reset(UINT index) {
	DIG_INP_STATE_T *inpState_p;

	assert(index < RS232_NUM_INP);

	inpState_p = &dig_info.inpState[index];
	inpState_p->debounceTS = 0;
}

/*
 * Checks whether bit index is an input (not accounting for inputs configured as pull down
 */
BOOL digital_is_input(UINT index) {
	assert(index < RS232_NUM_INP);
	return (dig_info.inputMask & (1 << index)) != 0;
}



U32 digital_input_read() {
	U32 tmpU32;

	//DisableInterrupts;
	tmpU32 = dig_info.validSwitch;
	dig_info.validSwitch = 0;
	//EnableInterrupts;
	return tmpU32;
}

/*
 * ===============================================================================
 *
 * Name: digital_upd_outputs
 *
* ===============================================================================
*
 * Update outputs
 *
 * Update output bits in temporary registers.
 *
 * @param   value - Value of outputs to be updated.
 * @param   mask - Mask of outputs to be updated.
 * @return  None
 * ===============================================================================
 */
void digital_output_upd(U32 value, U32 mask) {
	dig_info.updateValue |= value;
#if OPP_DEBUG_PORT != 0
	mask &= DBG_MASK;
#endif
	dig_info.updateMask |= mask;
}


/*
 * ===============================================================================
 *
 * Name: digital_write_outputs
 *
 * ===============================================================================
 *
 * Write outputs to pins
 *
 * Write cached output updates to pins.
 *
 * @param   None
 * @return  None
 * ===============================================================================
 */
void digital_output_flush() {
	stdldigio_write_all_ports(dig_info.updateValue, dig_info.updateMask);
	dig_info.updateValue = 0;
	dig_info.updateMask = 0;
}

/*
 * ===============================================================================
 *
 * Name: digital_task
 *
 * ===============================================================================
 *
 * Task for polling inputs
 *
 * @param   None
 * @return  None
 * ===============================================================================
 */
void digital_task(void) {
	U32 inputs;
	U32 updFilterHi;
	U32 updFilterLow;

	if (opp_info.validCfg) {
		/* Grab the inputs */
		inputs = stdldigio_read_all_ports(dig_info.inputMask);

		// input specific processing
		if ((opp_info.typeWingBrds & ((1 << WING_INP) | (1 << WING_SOL) | (1 << WING_SOL_8))) != 0) {
			digital_input_task(inputs, &updFilterHi, &updFilterLow);
		}

		// input switch matrix specific processing
		if ((opp_info.typeWingBrds & (1 << WING_SW_MATRIX_IN)) != 0) {
			swmtrx_task();
		}

		// solenoid specific processing
		if ((opp_info.typeWingBrds & ((1 << WING_SOL) | (1 << WING_SOL_8))) != 0) {
			solenoid_task(inputs, updFilterLow);
		}

		// TODO: explain this block
		if ((opp_info.typeWingBrds & ((1 << WING_INP) | (1 << WING_SOL) | (1 << WING_SOL_8))) != 0) {
			//DisableInterrupts;
			dig_info.validSwitch = (dig_info.validSwitch & ~dig_info.stateMask) | (dig_info.filtInputs & dig_info.stateMask);
			//EnableInterrupts;
			dig_info.prevInputs = inputs;
		}

		if ((opp_info.typeWingBrds & (1 << WING_INCAND)) != 0) {
	      incand_task();
		}

		if ((opp_info.typeWingBrds & (1 << WING_LAMP_MATRIX_COL)) != 0) {
			lampmtrx_task();
		}

		if ((opp_info.typeWingBrds & ((1 << WING_NEO) | (1 << WING_NEO_SOL) )) != 0) {
		    neopxl_task();
		}

        digital_output_flush();
	}
}

