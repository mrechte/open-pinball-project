#ifndef FADE_H
#define FADE_H

/* Convert 32 levels of intensity to pulse width in usec of 1 ms tick */
extern const U16 FADE_USEC_DUR[32];

void fade_init();

void fade_start();

void fade_task();

void fade_init_rec(
	INT  startOffset,
	INT  numFadeBytes,
	U8   **currPxlVal_pp,
	U8   **newPxlVal_pp,
	void (*fadeProc_fp)(UINT offset, U8 newData),
	void (*endFadeProc_fp)()
);

#endif
