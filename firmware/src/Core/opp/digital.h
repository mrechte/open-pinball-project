#ifndef DIGITAL_H
#define DIGITAL_H

// TODO this define is used by both solenoid.c and swmtrx.c
#define MATRIX_FIRE_SOL       0x80


/* Input configuration saved in flash, part of cfgData
 * This is a 4-byte long structure
 */
typedef struct {
	RS232_CFG_INP_TYPE_E cfg;
	char filler; // to get netx filed aligned
	U16	debounce; // expressed in ms x 10 (viz. 10 is 1 ms)
} INP_CFG_T; // intentionally not packed


void digital_task(void);

void digital_init(void);

void digital_deinit();

void digital_config_wing_gpio(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);

BOOL digital_is_input(UINT index);

void digital_output_upd(U32 value, U32 mask);

void digital_sw_mtrx_set_sol_info(U8 input, U32 sol);

void digital_input_reset_state(U8 input);

void digital_input_state_mask_set(INT bit);

void digital_input_state_mask_clear(INT bit);

void digital_input_state_reset(UINT index);

U32 digital_input_read();

void digital_sw_mtrx_set_prev();

U8 *digital_sw_mtrx_get_buf();

U16 *digital_get_input_ts_buf();

#endif
