#ifndef OPP_H
#define OPP_H
 
#include "stdl.h"
#include "stdtypes.h"

#include "rs232.h"
#include "digital.h"
#include "solenoid.h"
#include "neopxl.h"

/*
 * If set to 1, two STM32 pins (PA13 and PA14) are used for debugging through a ST/Link adapter.
 * Therefore these pins must be excluded from OPP use.
 */
#define OPP_DEBUG_PORT      0	// 0: no debug port (for release) 1: debug port (input 0 and 16 should not be connected in that case)
#define OPP_CODE_VERS       0x0204ff0b // 02MMmmBB where 02 is for Gen 2 card, MM Major, mm Minor, BB is for build #. mm = FF is reserved for dev of next major.

#define OPP_CFG_TBL         0x0800fc00 // flash address (last 1KB page #63) for configuration table (OPP_NV_CFG_T)
#define OPP_PERSIST_TBL     0x0800fff0 // flash address (16 last bytes of page #63) reserved for OPP_PERSIST_T (12 bytes)
#define OPP_FLASH_SECT_SZ   0x400
#define OPP_NV_PARM_SIZE    (sizeof(OPP_NV_CFG_T) - 4) // sizeof(cfgData) + sizeof(wingCfg) = size of the flash for CRC computation (counting from wingCfg field, so does not include nvCfgCrc and res1 fields)

#define OPP_STAT_BLINK_SLOW_ON       0x01
#define OPP_STAT_FADE_SLOW_DEC       0x01
#define OPP_STAT_BLINK_FAST_ON       0x02
#define OPP_STAT_FADE_FAST_DEC       0x02
#define OPP_MAX_STATE_NUM            32      /* State num goes from 0 - 31 */

#define OPP_STAT_LED_ON              0x00000004
#define OPP_STAT_TOGGLE_LED          0x00040004
#define OPP_STAT_BSRR_PTR            0x40010c10

#define OPP_SERVO_FIRST_INDX         8
#define OPP_SERVO_NUM_INP_WING_SERVO 8
#define OPP_SERVO_NUM_SOL_WING_SERVO 4

// Mask to apply to inputs / outputs if debug is set
#define DBG_MASK				0xfffefffe


// Possible fatal errors (see Error_Handler in main.c)
typedef enum {
   NO_ERRORS                 	= 0,
   ERR_HARD_INIT			 	= 1,
   ERR_ASSERT					= 2,
   ERR_HARD_FAULT				= 3,
   ERR_FLASH					= 4,
   ERR_MALLOC_FAIL            	= 5,
} OPP_ERROR_E;


// Miscellaneous counters for debug purpose
typedef struct {
	U16	crcErr;
	U16 badCmd; // bad command or invalid parameter
	U16 timeOut;
} OPP_COUNTERS;


/*
* Inputs configuration (saved in flash, part of cfgData)
* This is a 128-byte long structure (4*32)
*/
typedef struct {
  INP_CFG_T      inpCfg[RS232_NUM_INP];
} OPP_INP_CFG_T;


/*
* Solenoid configuration (saved in flash, part of cfgData)
* This is a 256-byte long structure
*/
typedef struct {
   SOL_CFG_T           solCfg[RS232_NUM_SOL];
} OPP_SOL_CFG_T;


/*
 * Neo LED configuration (saved in flash, part of cfgData)
 * This is a 16-byte long structure
 */
typedef struct {
	NEO_CFG_T           	neoCfg[RS232_NUM_NEO];
} OPP_NEO_CFG_T; // intentionally not packed


/* OPP_NV_CFG_T Non-volatile configuration stored in flash on Gen2 boards.
* Requires 410 bytes of flash. TBC this could be extended at 1KB, as flash pages are 1KB and one cannot erase less than one page.
* cfgData currently holds 402 bytes (CFG_DATA_SIZE):
* - 128 bytes (OPP_INP_CFG_T) for input conf
* - 256 bytes (OPP_SOL_CFG_T) for solenoid conf
* - 16 bytes (OPP_NEO_CFG_T) for neopixel conf
* OPP_NV_CFG_T hold a total of 410 bytes: cfgData + 8 bytes for the preceding fields (4 + 3 + 1)
* opp_nv_cfg_p is a pointer to the flash area where the config is stored.
* There is a copy of this struct in RAM (opp_info.nvCfgInfo), and to facilitate the access to cfgData fields, 3 pointers are defined:
* - inpCfg_p
* - solDrvCfg_p
* - neoCfg_p
* The last 2 fields totalize OPP_NV_PARM_SIZE bytes.
*
*/
#define CFG_DATA_SIZE 	(sizeof(OPP_INP_CFG_T) + sizeof(OPP_SOL_CFG_T) + sizeof(OPP_NEO_CFG_T))
typedef struct {
  U8                        nvCfgCrc;   /* CRC from wingCfg to end of cfgData */
  U8                        res1[3];    // not used
  RS232_WING_TYPE_E			wingCfg[RS232_NUM_WING]; // this defines the 4 wings' type
  U8                        cfgData[CFG_DATA_SIZE];  // modules specific configuration
} OPP_NV_CFG_T;


/*
* Pointer to config in flash. Note that there is another copy in RAM.
*/
extern OPP_NV_CFG_T *opp_nv_cfg_p;


/*
* Non volatile serial number stored in flash on boards.
*/
typedef struct {
  U32                        res1[2]; // reserved
  U32                        serNum; // serial number
  U32                        prodId; // not used
} OPP_PERSIST_T;


/*
* Pointer to serial number in flash.
* This is located in the same flash block than config, but kept apart, in order to be able to write again this block after a flash erase (we do not want to loose it).
*/
extern OPP_PERSIST_T *opp_persist_p;


/*
 * The main structure containing the whole system state.
 * Note that some modules (like digital.c) maintain their own internal structure, which may duplicate some of these fields.
 */
typedef struct {
   BOOL                 validCfg; // config from flash memory is OK (CRC OK).
   BOOL                 haveNeo;
   BOOL                 haveSpi;
   BOOL                 haveFade;
   BOOL                 haveLampMtrx;

   U8                   ledStateNum;   /* 0 - 31 counter used to fade/blink LEDs */
   U8                   ledStatus;     /* If blinking LED is on/fading LED is brighter */

   U32                  typeWingBrds;  /* Bit mask of types of populated wing boards */

   OPP_COUNTERS         counters; // Error counters

   U32                  prodId; // not used
   U32                  serNum; // card serial number (it not set contains 0xffffffff)

   U32                  statusBlink; // blue LED status

   OPP_NV_CFG_T         nvCfgInfo; // A copy of the flash memory configuration
   OPP_INP_CFG_T        *inpCfg_p; // A pointer to the input configuration within cfgData member.
   OPP_SOL_CFG_T		*solDrvCfg_p; // A pointer to the solenoid configuration within cfgData member.
   OPP_NEO_CFG_T       	*neoCfg_p; // A pointer to neopixel configuration within cfgData member.
   U8                   *freeCfg_p; // A work pointer to cfgData (=inpCfg_p), then solDrvCfg_p, then neoCfg_p, then nothing, used in digital.c and neopxl.c init code.
} OPP_INFO;

extern OPP_INFO opp_info;


void Error_Handler(int strokes);

void opp_copy_flash_to_ram(void);

void opp_main();

void opp_reset_info(void);

#endif
