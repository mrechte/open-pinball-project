#include <assert.h>
#include <string.h>

#include "opp.h"
#include "rs232.h"
#include "digital.h"
#include "solenoid.h"

#define MTRX_INPUT_BIT_MASK   0xff000000
#define MTRX_OUTPUT_BIT_MASK  0x00ff0000

#define MATRIX_SOL_MASK       0x07
#define MATRIX_WAIT_CNT_THRESH   2
#define MATRIX_DEBOUNCE_THRESH   4
#define MATRIX_COL_OFFS       16
#define MATRIX_COL_MASK       0x00ff0000



typedef struct {
	U8 cnt;
	U8 sol;
} SWMTRX_BIT_INFO_T;

typedef struct {
	U8 column;
	U8 waitCnt;
	U8 mtrxWaitCntThresh;
	U8 mtrxDebounceThresh;
	BOOL mtrxActHigh;
	SWMTRX_BIT_INFO_T info[RS232_SW_MATRX_INP];
} SWMTRX_DATA_T;


typedef struct {
	BOOL init; // TRUE if module initialized
	SWMTRX_DATA_T mtrxData; // 64 matrix inputs
	U32 mtrxInpMask; // mask of configured matrix input bits (excluding dig_info.inpMask)
	BOOL switchMtrxActHigh; // Indicates if pulldown (1) or pullup (0) for matrix inputs
	U8 matrixInp[RS232_MATRX_COL]; /* Note:  written in reverse column order to match Bally numbering. */
	U8 matrixPrev[RS232_MATRX_COL]; // matrixInp previous value

} SWMTRX_GLOB_T;

SWMTRX_GLOB_T swmtrx_info;


void swmtrx_init() {
	memset(&swmtrx_info, 0, sizeof(swmtrx_info));
}

void swmtrx_wing_out_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {

	assert(wing == 2);

	if (!swmtrx_info.init) swmtrx_init();

	swmtrx_info.mtrxData.column = 0;
	swmtrx_info.mtrxData.waitCnt = 0;

	*outputMask_p |= MTRX_OUTPUT_BIT_MASK;
#if OPP_DEBUG_PORT != 0
	*outputMask_p &= DBG_MASK;
#endif

	// make sure these bits are no longer inputs
	*inputMask_p &= ~MTRX_OUTPUT_BIT_MASK;
	*inputPDMask_p &= ~MTRX_OUTPUT_BIT_MASK;

	// TODO Why do we need to store input bits ? MTRX_INPUT_BIT_MASK would be sufficient.
	// input bits will be initialized in corresponding wing.
	swmtrx_info.mtrxInpMask |= MTRX_INPUT_BIT_MASK;

	// Determine whether pull down or pull up resistor for input
	if (opp_info.nvCfgInfo.wingCfg[wing] == WING_SW_MATRIX_OUT) {
		swmtrx_info.switchMtrxActHigh = TRUE;
	}

	digital_config_wing_gpio(wing, inputMask_p, inputPDMask_p, outputMask_p);

}


void swmtrx_wing_in_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	U32 mask;
	INT index;
	SOL_CFG_T *solCfg_p;
	INP_CFG_T *inpCfg_p;

	assert(wing == 3);

	if (!swmtrx_info.init) swmtrx_init();

	// set input bits
	mask = swmtrx_info.mtrxInpMask;
#if OPP_DEBUG_PORT != 0
	mask &= DBG_MASK;
#endif
	*(swmtrx_info.switchMtrxActHigh ? inputPDMask_p : inputMask_p) |= mask;

	// make sure these bits are no longer outputs
	*outputMask_p &= ~mask;

	/* Grab mtrxWaitCntThresh from config if set */
	inpCfg_p = &opp_info.inpCfg_p->inpCfg[RS232_MTRX_WAIT_THRESH_INDX];
	if (inpCfg_p->cfg == 0) {
		swmtrx_info.mtrxData.mtrxWaitCntThresh = MATRIX_WAIT_CNT_THRESH;
	} else {
		swmtrx_info.mtrxData.mtrxWaitCntThresh = inpCfg_p->cfg;
	}

	/* Grab mtrxDebounceThresh from config if set */
	inpCfg_p = &opp_info.inpCfg_p->inpCfg[RS232_MTRX_DEBOUNCE_THRESH_INDX];
	if (inpCfg_p->cfg == 0) {
		swmtrx_info.mtrxData.mtrxDebounceThresh = MATRIX_DEBOUNCE_THRESH;
	} else {
		swmtrx_info.mtrxData.mtrxDebounceThresh = inpCfg_p->cfg;
	}

	/* Initialize the counts and solenoid info */
	for (index = 0; index < RS232_SW_MATRX_INP; index++) {
		swmtrx_info.mtrxData.info[index].cnt = 0;
		swmtrx_info.mtrxData.info[index].sol = 0;
	}
	/* Set up the solenoids to autofire using the switch matrix */
	for (index = 0, solCfg_p = &opp_info.solDrvCfg_p->solCfg[0]; index < RS232_NUM_SOL_LEGACY; index++, solCfg_p++) {
		if (solCfg_p->cfg & USE_MATRIX_INP) {
			swmtrx_info.mtrxData.info[solCfg_p->minOffDuty].sol = MATRIX_FIRE_SOL | index;
		}
	}
	/* Initialize matrix input for rs232 reports */
	if (!swmtrx_info.switchMtrxActHigh) {
		for (index = 0; index < RS232_MATRX_COL; index++) {
			swmtrx_info.matrixInp[index] = 0xff;
		}
	}

	digital_config_wing_gpio(wing, inputMask_p, inputPDMask_p, outputMask_p);

}


// for rs232proc.c
U8 *swmtrx_get_buf() {
	return swmtrx_info.matrixInp;
}

// for rs232proc.c
void swmtrx_set_prev() {
	UINT index;
	U8 *src_p;
	U8 *dst_p;

	for (index = 0, dst_p = &swmtrx_info.matrixInp[0], src_p = &swmtrx_info.matrixPrev[0];
			index < RS232_MATRX_COL; index++, src_p++, dst_p++) {
		if (swmtrx_info.switchMtrxActHigh) {
			*dst_p &= *src_p;
		} else {
			*dst_p |= *src_p;
		}
	}
}

// for solenoid.c
void swmtrx_set_sol_info(U8 input, U32 sol) {
	swmtrx_info.mtrxData.info[input].sol = sol;
}


/*
 * ===============================================================================
 *
 * Name: digital_mtrx_switch_proc
 *
 * ===============================================================================
 *
 * Matrix switch processing
 *
 * @return  None
 * ===============================================================================
 */
void swmtrx_task(void) {
	SWMTRX_BIT_INFO_T *matrixBitInfo_p;
	U8 data;
	U8 chngU8;
	INT index;
	U32 currBit;

	if (swmtrx_info.mtrxData.waitCnt >= swmtrx_info.mtrxData.mtrxWaitCntThresh) {
		swmtrx_info.mtrxData.waitCnt = 0;
		data = (U8) (stdldigio_read_all_ports(swmtrx_info.mtrxInpMask) >> 24);
		chngU8 = data ^ swmtrx_info.matrixPrev[swmtrx_info.mtrxData.column];
		swmtrx_info.matrixPrev[swmtrx_info.mtrxData.column] = data;
		matrixBitInfo_p = &swmtrx_info.mtrxData.info[swmtrx_info.mtrxData.column * 8];
		for (index = 0, currBit = 1; index < 8; index++, currBit <<= 1, matrixBitInfo_p++) {
			/* If bit has changed, reset count */
			if ((currBit & chngU8) != 0) {
				matrixBitInfo_p->cnt = 0;
			} else {
				if (matrixBitInfo_p->cnt <= swmtrx_info.mtrxData.mtrxDebounceThresh) {
					matrixBitInfo_p->cnt++;
				}
				/* Just passed the threshold so update data bit */
				if (matrixBitInfo_p->cnt == swmtrx_info.mtrxData.mtrxDebounceThresh) {
					if (((data & currBit) && swmtrx_info.switchMtrxActHigh)
							|| (((data & currBit) == 0) && !swmtrx_info.switchMtrxActHigh)) {
						/* Set or clear bit depending if active high or low */
						if (swmtrx_info.switchMtrxActHigh) {
							swmtrx_info.matrixInp[swmtrx_info.mtrxData.column] |= currBit;
						} else {
							swmtrx_info.matrixInp[swmtrx_info.mtrxData.column] &= ~currBit;
						}
						if (matrixBitInfo_p->sol != 0) {
							solenoid_drive(1 << (matrixBitInfo_p->sol & MATRIX_SOL_MASK), 1 << MATRIX_SOL_MASK);
						}
					}
				}
			}
		}
	}
	if (swmtrx_info.mtrxData.waitCnt == 0) {
		/* Move to next column */
		swmtrx_info.mtrxData.column++;
		if (swmtrx_info.mtrxData.column >= RS232_MATRX_COL) {
			swmtrx_info.mtrxData.column = 0;
		}
		/* Reverse column numbering to match Bally documentation */
		if (swmtrx_info.switchMtrxActHigh) {
			digital_output_upd(1 << (MATRIX_COL_OFFS + RS232_MATRX_COL - 1 - swmtrx_info.mtrxData.column), MTRX_OUTPUT_BIT_MASK);
		} else {
			digital_output_upd((~(1 << (MATRIX_COL_OFFS + RS232_MATRX_COL - 1 - swmtrx_info.mtrxData.column))) & MATRIX_COL_MASK, MTRX_OUTPUT_BIT_MASK);
		}
	}
	swmtrx_info.mtrxData.waitCnt++;

	if (swmtrx_info.mtrxData.waitCnt >= swmtrx_info.mtrxData.mtrxWaitCntThresh) {
		swmtrx_info.mtrxData.waitCnt = 0;
		data = (U8) (stdldigio_read_all_ports(swmtrx_info.mtrxInpMask) >> 24);
		chngU8 = data ^ swmtrx_info.matrixPrev[swmtrx_info.mtrxData.column];
		swmtrx_info.matrixPrev[swmtrx_info.mtrxData.column] = data;
		matrixBitInfo_p = &swmtrx_info.mtrxData.info[swmtrx_info.mtrxData.column * 8];
		for (index = 0, currBit = 1; index < 8; index++, currBit <<= 1, matrixBitInfo_p++) {
			/* If bit has changed, reset count */
			if ((currBit & chngU8) != 0) {
				matrixBitInfo_p->cnt = 0;
			} else {
				if (matrixBitInfo_p->cnt <= swmtrx_info.mtrxData.mtrxDebounceThresh) {
					matrixBitInfo_p->cnt++;
				}
				/* Just passed the threshold so update data bit */
				if (matrixBitInfo_p->cnt == swmtrx_info.mtrxData.mtrxDebounceThresh) {
					if (((data & currBit) && swmtrx_info.switchMtrxActHigh)
							|| (((data & currBit) == 0) && !swmtrx_info.switchMtrxActHigh)) {
						/* Set or clear bit depending if active high or low */
						if (swmtrx_info.switchMtrxActHigh) {
							swmtrx_info.matrixInp[swmtrx_info.mtrxData.column] |= currBit;
						} else {
							swmtrx_info.matrixInp[swmtrx_info.mtrxData.column] &= ~currBit;
						}
						if (matrixBitInfo_p->sol != 0) {
							solenoid_drive(1 << (matrixBitInfo_p->sol & MATRIX_SOL_MASK), 1 << MATRIX_SOL_MASK);
						}
					}
				}
			}
		}
	}
	if (swmtrx_info.mtrxData.waitCnt == 0) {
		/* Move to next column */
		swmtrx_info.mtrxData.column++;
		if (swmtrx_info.mtrxData.column >= RS232_MATRX_COL) {
			swmtrx_info.mtrxData.column = 0;
		}
		/* Reverse column numbering to match Bally documentation */
		if (swmtrx_info.switchMtrxActHigh) {
			digital_output_upd(1 << (MATRIX_COL_OFFS + RS232_MATRX_COL - 1 - swmtrx_info.mtrxData.column), MTRX_OUTPUT_BIT_MASK);
		} else {
			digital_output_upd((~(1 << (MATRIX_COL_OFFS + RS232_MATRX_COL - 1 - swmtrx_info.mtrxData.column))) & MATRIX_COL_MASK, MTRX_OUTPUT_BIT_MASK);
		}
	}
	swmtrx_info.mtrxData.waitCnt++;
}


