#ifndef INCAND_H
#define INCAND_H

void incand_task();

void incand_proc_cmd(U8 cmd, U32 mask);

void incand_neo_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);

#endif
