#ifndef SWMTRX_H
#define SWMTRX_H

void swmtrx_wing_out_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);

void swmtrx_wing_in_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p);

U8 *swmtrx_get_buf();

void swmtrx_set_prev();

void swmtrx_set_sol_info(U8 input, U32 sol);

void swmtrx_task(void);

#endif
