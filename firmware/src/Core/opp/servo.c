/**
 * @file:   servo.c
 * This is the file for servos using a pins on wing 1.  This uses a
 * TIM2, TIM3 and TIM4 to send PWM outputs to control servo motors.
 *
 * This file requires a 10ms tick to start the servo processing.
 *
 *===============================================================================
 */
#include <stdlib.h>
#include "stdtypes.h"

#include "servo.h"
#include "fade.h"
#include "mcu.h"
#include "opp.h"

#define MAX_SERVOS               8

#define SERVO_WAIT_FADE_DONE     1
#define SERVO_WAIT_FOR_TICK      2
#define SERVO_DISABLED           0xff

#define SERVO_TIM2          0x10
#define SERVO_TIM3          0x20
#define SERVO_TIM4          0x40
#define SERVO_COUNTER_MASK  0x0f
#define SERVO_TIM_MASK      0xf0

const U8 SERVO_TIM_OFFS[MAX_SERVOS] = {
		SERVO_TIM2, SERVO_TIM2 | 1,
		SERVO_TIM3, SERVO_TIM3 | 1,
		SERVO_TIM4,
		SERVO_TIM4 | 1,
		SERVO_TIM4 | 2,
		SERVO_TIM4 | 3
};

typedef struct {
	BOOL init; // TRUE if module initialized
    U8 servoMask; // pins configured as servo output => move out from here (used by servo.c)
} SERVO_GLOB_T;

SERVO_GLOB_T servo_info;


/* Prototypes */
void servo_move_proc(UINT offset, U8 newData);
void servo_end_move_proc();

/*
 * ===============================================================================
 * 
 * Name: servo_servo_init
 * 
 * ===============================================================================
 *
 * Initialize servo processing
 * 
 * Initialize timers and set to default output, allocate memory, and reset state machine.
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void servo_servo_init(UINT wing, U32 *inputMask_p, U32 *inputPDMask_p, U32 *outputMask_p) {
	INT index;
	INP_CFG_T *cfg_p;
	U8 *currPwmVal_p; /* Ptr to array of current PWM values */
	U8 *newPwmVal_p; /* Ptr to array of future PWM values */
	U8 counter;
	BOOL useTim2 = FALSE;
	BOOL useTim3 = FALSE;
	BOOL useTim4 = FALSE;
	BOOL resetTim = FALSE;

	assert(wing == 1);
	for (index = OPP_SERVO_FIRST_INDX; index < OPP_SERVO_FIRST_INDX + OPP_SERVO_NUM_INP_WING_SERVO; index++) {
		cfg_p = &opp_info.inpCfg_p->inpCfg[index];
		if (cfg_p->cfg >= SERVO_OUTPUT_THRESH) {
			// this pin is no longer an input
			*inputMask_p &= ~(1 << index);
			servo_info.servoMask |= (1 << (index - OPP_SERVO_FIRST_INDX));
			/* TODO according to what is say in digital.c preamble this should not be reported as an output pin ?
			// This foo is called after bsic wing config, so GPIO should be programmed here
			// *outputMask_p |= (1 << input);
			// code moved from original digital_init_gpio
			pinCfg.Speed = GPIO_SPEED_FREQ_LOW;
			// Check if servo output which uses alternate function
			if ((bitIndex >= OPP_SERVO_FIRST_INDX) && (bitIndex < OPP_SERVO_FIRST_INDX + OPP_SERVO_NUM_INP_WING_SERVO)
					&& (opp_info.servoMask & 1 << (bitIndex - OPP_SERVO_FIRST_INDX))) {
				pinCfg.Mode = GPIO_MODE_AF_PP;
			} else {
				pinCfg.Mode = GPIO_MODE_OUTPUT_PP;
			}
			...

			*/
#if OPP_DEBUG_PORT != 0
			//*outputMask_p &= DBG_MASK;
#endif
		}
	}
	if (servo_info.servoMask) {
		/* Calculate maximum number of bytes to process at a time */
		fade_init_rec(RS232_FADE_SERVO_OFFSET, MAX_SERVOS, &currPwmVal_p, &newPwmVal_p, servo_move_proc, servo_end_move_proc);

		/* Set initial values to verify PWMs are working */
		TIMxRegs *volatile timBase_p;
		for (index = 0; index < MAX_SERVOS; index++) {
			/* Check if this is a servo */
			if (servo_info.servoMask & (1 << index)) {
				cfg_p = &opp_info.inpCfg_p->inpCfg[index + OPP_SERVO_FIRST_INDX];
				/* Disable initial pulses if config value = 0xff */
				counter = cfg_p->cfg;
				if (counter == 0xff) {
					counter = 0;
				}
				*(currPwmVal_p + index) = counter;
				*(newPwmVal_p + index) = counter;
				if (SERVO_TIM_OFFS[index] & SERVO_TIM2) {
					timBase_p = tim2Base_p;
					if (!useTim2) {
						/* Enable altFunc clock, TIM2 clock, and TIM2 partial remap (01) */
						rccBase_p->APB2ENR |= 0x00000001;
						rccBase_p->APB1ENR |= 0x00000001;
						afioBase_p->MAPR |= 0x00000100;
						useTim2 = TRUE;
						resetTim = TRUE;
					}
				} else if (SERVO_TIM_OFFS[index] & SERVO_TIM3) {
					timBase_p = tim3Base_p;
					if (!useTim3) {
						/* Enable altFunc clock, TIM3 clock, and TIM3 partial remap (10) */
						rccBase_p->APB2ENR |= 0x00000001;
						rccBase_p->APB1ENR |= 0x00000002;
						afioBase_p->MAPR |= 0x00000800;
						useTim3 = TRUE;
						resetTim = TRUE;
					}
				} else {
					timBase_p = tim4Base_p;
					if (!useTim4) {
						/* Enable TIM4 clock */
						rccBase_p->APB1ENR |= 0x00000004;
						useTim4 = TRUE;
						resetTim = TRUE;
					}
				}
				if (resetTim) {
					/* Reset timer */
					timBase_p->CR1 = 0;
					timBase_p->CCER = 0;
					timBase_p->CCMR[0] = 0;
					timBase_p->CCMR[1] = 0;
					resetTim = FALSE;
				}
				/* Initialize TIM to 10 us tick, 20 ms period */
				timBase_p->PSC = 479;
				timBase_p->ARR = 1999;
				timBase_p->CCR[SERVO_TIM_OFFS[index] & SERVO_COUNTER_MASK] = (U32) counter;

				/* Set OCxM (PWM mode 1) and OCxPE (allow preloading) */
				timBase_p->CCMR[(SERVO_TIM_OFFS[index] & 0x02) >> 1] |= (0x68 << ((SERVO_TIM_OFFS[index] & 0x01) << 3));

				/* Output enable active high CCR */
				timBase_p->CCER |= (0x1 << ((SERVO_TIM_OFFS[index] & SERVO_COUNTER_MASK) << 2));
			}
		}
		if (useTim2) {
			tim2Base_p->CR1 |= 1;
		}
		if (useTim3) {
			tim3Base_p->CR1 |= 1;
		}
		if (useTim4) {
			tim4Base_p->CR1 |= 1;
		}
	}
}

/* Note:  No servo_10ms_tick and servo_task are needed because the updates are
 * made automatically by the fade processing.  Everything else is run by the PWM
 * within the processor.
 */

/*
 * ===============================================================================
 * 
 * Name: servo_move_proc
 *
 * ===============================================================================
 *
 * Servo move processing
 *
 * Special processing for moving between two servo positions in a given time
 *
 * @param   None
 * @return  None
 * ===============================================================================
 */
void servo_move_proc(UINT offset, U8 newData) {
	TIMxRegs *volatile timBase_p;
	if (SERVO_TIM_OFFS[offset] & SERVO_TIM2) {
		timBase_p = tim2Base_p;
	} else if (SERVO_TIM_OFFS[offset] & SERVO_TIM3) {
		timBase_p = tim3Base_p;
	} else {
		timBase_p = tim4Base_p;
	}
	timBase_p->CCR[SERVO_TIM_OFFS[offset] & SERVO_COUNTER_MASK] = (U32) newData;
}

/*
 * ===============================================================================
 *
 * Name: servo_end_move_proc
 *
 * ===============================================================================
 *
 * Servo end move processing
 *
 * Empty function since no special processing needs to be done for servos
 *
 * @param   None
 * @return  None
 *
 * @pre     None
 * @note    None
 *
 * ===============================================================================
 */
void servo_end_move_proc() {
}
