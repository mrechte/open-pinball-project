#ifndef RS232_H
#define RS232_H

#include "stdtypes.h"   /* include peripheral declarations */
   
#define RS232_NUM_WING 4
#define RS232_NUM_INP 32
#define RS232_NUM_SOL_LEGACY 16
#define RS232_NUM_SOL 32
#define RS232_NUM_NEO 2

#define RS232_SZ_COLOR_TBL 32
#define RS232_SW_MATRX_INP 64
#define RS232_MATRX_COL    8
#define RS232_TIMESTAMP_BYTES 64
#define RS232_CMD_HDR      2  /* CardAddr + Cmd */
#define RS232_CRC8_SZ      1
#define RS232_MAX_TX_BUF_SZ   (RS232_CMD_HDR + RS232_TIMESTAMP_BYTES + RS232_CRC8_SZ)
#define RS232_CMD_TIMEOUT  50 // timeout (ms) to receive a complete command


// The fade module can manage 5 types of fades, these are converted into FADE_REC_T in fade.c
#define RS232_FADE_NEO_OFFSET        0x0000
#define RS232_FADE_INCAND_OFFSET     0x1000
#define RS232_FADE_LMP_MATRIX_OFFSET 0x2000
#define RS232_FADE_SERVO_OFFSET      0x3000
#define RS232_FADE_NEO2_OFFSET       0x4000


/* Each command starts with the Card ID except for inventory and EOM.  Next comes the
 *  command, then any data.
 */
typedef enum {
  RS232_GET_SER_NUM        	= 0x00,
  RS232_GET_PROD_ID        	= 0x01,
  RS232_GET_VERS           	= 0x02,
  RS232_SET_SER_NUM        	= 0x03,
  RS232_RESET              	= 0x04,
  RS232_GO_BOOT            	= 0x05,
  RS232_CONFIG_SOL         	= 0x06,
  RS232_KICK_SOL           	= 0x07,
  RS232_READ_INP      		= 0x08,
  RS232_CONFIG_INP         	= 0x09,
  RS232_SAVE_CFG           	= 0x0b,
  RS232_ERASE_CFG          	= 0x0c,
  RS232_GET_CFG       		= 0x0d,
  RS232_SET_CFG       		= 0x0e,
  RS232_CFG_NEOPIXEL  		= 0x12,
  RS232_INCAND         		= 0x13,
  RS232_CONFIG_IND_SOL     	= 0x14,
  RS232_CONFIG_IND_INP     	= 0x15,
  RS232_SET_IND_NEO        	= 0x16,
  RS232_SET_SOL_INPUT      	= 0x17,
  RS232_READ_MATRIX_INP    	= 0x19,
  RS232_GET_INP_TIMESTAMP  	= 0x1a,
  RS232_SOL_KICK_PWM       	= 0x1b,
  RS232_EXT_CONFIG_IND_SOL 	= 0x1d,
  RS232_GET_WING_CFG       	= 0x1e,
  RS232_EXT_KICK_SOL       	= 0x27,
  RS232_GET_SOL_STATE		= 0xe0,
  RS232_GET_COUNTERS		= 0xe1,
  // Special commands (variable parameter length)
  RS232_FADE       			= 0x40,
  RS232_SPI            		= 0x41,
  // Very special commands
  RS232_INVENTORY          = 0xf0,
  RS232_EOM                = 0xff,		// This not a command but End Of Message marker
} __attribute__((packed)) RS232_CMD_E;


typedef enum {
  CARD_ID_CARD_NUM_MASK     = 0x0f,

  CARD_ID_TYPE_MASK         = 0xf0,
  CARD_ID_SOL_CARD          = 0x00,
  CARD_ID_INP_CARD          = 0x10,
  CARD_ID_CARD         		= 0x20,
  
} __attribute__((packed)) RS232_CARD_ID_E;


/*
 * These are flags to be combined in order to configure a solenoid
 */
typedef enum {
  USE_SWITCH                = 0x01, // For legacy wings, indicate to use the dedicated switch input
  AUTO_CLR                  = 0x02, // If triggered by software command, this will prevent from kicking again after off time.
  ON_OFF_SOL                = 0x04, // As long as the command is there (either by soft or by switch), keeps the solenoid kicking.
  DLY_KICK_SOL              = 0x08, // The kick is delayed after the command instruct to fire
  USE_MATRIX_INP            = 0x10,
  CAN_CANCEL                = 0x20, // For flipper use, to allow deactivate col if the switch is released before the init kick time
} __attribute__((packed)) RS232_CFG_SOL_TYPE_E;


typedef enum {
  DUTY_CYCLE_MASK           = 0x0f,   /* lsb 4 bits are duty cycle */
  MIN_OFF_MASK              = 0x70,
}  __attribute__((packed)) RS232I_DUTY_E;


typedef enum {
  STATE_INPUT               = 0x00,
  FALL_EDGE                 = 0x01,
  RISE_EDGE                 = 0x02,

  SERVO_OUTPUT_THRESH       = 0x40  /* If wing 1, threshold for pin configured as servo */
} __attribute__((packed)) RS232_CFG_INP_TYPE_E;


#define RS232_MTRX_WAIT_THRESH_INDX        16
#define RS232_MTRX_DEBOUNCE_THRESH_INDX    17


/*
 * Wing types (should not be more than 32 types). No gap in numbering !
 */
typedef enum {
  WING_UNUSED               = 0x00,
  WING_SOL                  = 0x01,
  WING_INP                  = 0x02,
  WING_INCAND               = 0x03,
  WING_SW_MATRIX_OUT        = 0x04,
  WING_SW_MATRIX_IN         = 0x05,
  WING_NEO                  = 0x06,
  WING_HI_SIDE_INCAND       = 0x07,
  WING_NEO_SOL              = 0x08,
  WING_SPI                  = 0x09,
  WING_SW_MATRIX_OUT_LOW    = 0x0a,
  WING_LAMP_MATRIX_COL      = 0x0b,
  WING_LAMP_MATRIX_ROW      = 0x0c,
  WING_SOL_8                = 0x0d,
  MAX_WING_TYPES
} __attribute__((packed)) RS232_WING_TYPE_E;


typedef enum {
  INCAND_ROT_LEFT           = 0x00,
  INCAND_ROT_RIGHT          = 0x01,
  INCAND_LED_ON             = 0x02,
  INCAND_LED_OFF            = 0x03,
  INCAND_LED_BLINK_SLOW     = 0x04,
  INCAND_LED_BLINK_FAST     = 0x05,
  INCAND_LED_BLINK_OFF      = 0x06,
  INCAND_LED_SET_ON_OFF     = 0x07,

  INCAND_SET                = 0x80,
  INCAND_SET_ON             = 0x01,
  INCAND_SET_BLINK_SLOW     = 0x02,
  INCAND_SET_BLINK_FAST     = 0x04,
} __attribute__((packed)) RS232_INCAND_CMD_E;


typedef enum {
  SOL_INP_SOL_MASK          = 0x1f, // up to 32 solenoids
  SOL_INP_CLEAR_SOL         = 0x80, // bit to indicate one wants to clear instead of default set
} __attribute__((packed)) RS232_SET_SOL_INP_E;


typedef enum {
  SPI_READ                  = 0x01,
  SPI_WRITE                 = 0x02,
} __attribute__((packed)) RS232_SPI_CMD_E;


void rs232_init(void);
void rs232_task(void);



#endif
