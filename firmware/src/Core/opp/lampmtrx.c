/**
 * @file:   lampMtrx.c
 * This is the file for driving the Cobra lamp matrix plank.
 *
 *===============================================================================
 */
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "stdtypes.h"

#include "opp.h"

#include "timer.h"
#include "lampmtrx.h"
#include "digital.h"
#include "fade.h"

#define NUM_COLUMNS          8
#define NUM_ROW_BITS         8
#define LAMPMTRX_MAX_LAMPS   NUM_COLUMNS * NUM_ROW_BITS
#define COL_WING_POS         0
#define ROW_WING_POS         1
#define LAMP_DATA_OFFSET     8
#define LAMPMTRX_ALL_ON_MASK 0x0000ff00
#define LAMPMTRX_CTL_MASK    0x0000ffff    /* Includes row and column mask */

typedef struct {
	BOOL blinkSlow;
	U8 currCol;
	INT lastTime;
	U16 *intenData_p;
} LAMPMTRX_INFO;

LAMPMTRX_INFO lampmtrx_info;

/* Prototypes */
void lampmtrx_fade_proc(UINT offset, U8 newData);
void lampmtrx_end_fade_proc();

/*
 * ===============================================================================
 * 
 * Name: lampmtrx_init
 * 
 * ===============================================================================
 *
 * Initialize the lamp matrix driver
 * 
 * @param   None
 * @return  None
 * ===============================================================================
 */
void lampmtrx_init() {
	INT index;
	U8 *currPxlVal_p; /* Ptr to array of current pixel values */
	U8 *newPxlVal_p; /* Ptr to array of future pixel values */

	if ((opp_info.nvCfgInfo.wingCfg[COL_WING_POS] == WING_LAMP_MATRIX_COL)
			&& (opp_info.nvCfgInfo.wingCfg[ROW_WING_POS] == WING_LAMP_MATRIX_ROW)) {
		// deallocate first, then reset structure
		if (lampmtrx_info.intenData_p != NULL)
			free(lampmtrx_info.intenData_p);
		memset(&lampmtrx_info, 0, sizeof(lampmtrx_info));
		opp_info.haveLampMtrx = TRUE;
		lampmtrx_info.blinkSlow = TRUE;
		lampmtrx_info.currCol = 0;
		lampmtrx_info.intenData_p = (U16*) malloc(sizeof(U16) * LAMPMTRX_MAX_LAMPS);
		if (lampmtrx_info.intenData_p == NULL) {
			Error_Handler(ERR_MALLOC_FAIL);
		}
		for (index = 0; index < LAMPMTRX_MAX_LAMPS; index++) {
			lampmtrx_info.intenData_p[index] = 0;
		}

		/* Malloc (if not already done) for support of fade commands */
		fade_init_rec(RS232_FADE_LMP_MATRIX_OFFSET, LAMPMTRX_MAX_LAMPS, &currPxlVal_p, &newPxlVal_p, lampmtrx_fade_proc,
				lampmtrx_end_fade_proc);
		for (index = 0; index < LAMPMTRX_MAX_LAMPS; index++) {
			*currPxlVal_p++ = 0;
			*newPxlVal_p++ = 0;
		}
	}
} /* lampmtrx_init */

/*
 * ===============================================================================
 *
 * Name: lampmtrx_fade_proc
 *
 * ===============================================================================
 *
 * Lamp matrix fade processing
 *
 * Special processing for lamp matrices.  Used to fill out fade mask.
 *
 * @param   None
 * @return  None
 * ===============================================================================
 */
void lampmtrx_fade_proc(UINT offset, U8 newData) {
	/* Disable initial test blinking after receiving first fade command */
	lampmtrx_info.blinkSlow = FALSE;
	lampmtrx_info.intenData_p[offset] = FADE_USEC_DUR[newData >> 3];
}

/*
 * ===============================================================================
 *
 * Name: lampmtrx_end_fade_proc
 *
 * ===============================================================================
 *
 * Lamp matrix end fade processing
 *
 * Special processing occurs when fade processing is completed.
 *
 * @param   None
 * @return  None
 * ===============================================================================
 */
void lampmtrx_end_fade_proc() {
}

/*
 * ===============================================================================
 * 
 * Name: lampmtrx_task
 * 
 * ===============================================================================
 *
 * Lamp matrix task
 * 
 * Check if a cycle needs to start.  Turn on LEDs as necessary.
 * 
 * @param   None 
 * @return  None
 * 
 * ===============================================================================
 */
void lampmtrx_task() {
	U32 ledOut;
	INT index;
	INT currMsTime;
	U16 currUsTime;

	if (opp_info.haveLampMtrx) {
		/* Check if new cycle needs to be started */
		/* Switching column so no row/col bits to prevent ghosting */
		currMsTime = timer_get_ms_count();
		if (lampmtrx_info.lastTime != currMsTime) {
			lampmtrx_info.lastTime = currMsTime;
			lampmtrx_info.currCol++;
			if (lampmtrx_info.currCol >= NUM_COLUMNS) {
				lampmtrx_info.currCol = 0;
			}
			ledOut = 0;
		} else {
			/* Set correct column drive bit */
			ledOut = 1 << lampmtrx_info.currCol;

			/* Calculate bits to update */
			currUsTime = timer_get_us_count();
			if ((opp_info.ledStatus & OPP_STAT_BLINK_SLOW_ON) && lampmtrx_info.blinkSlow) {
				/* For white wood test blinking only light lamps 50% intensity */
				if (currUsTime < 500) {
					ledOut |= LAMPMTRX_ALL_ON_MASK;
				}
			} else {
				for (index = 0; index < NUM_ROW_BITS; index++) {
					if (lampmtrx_info.intenData_p[index + (lampmtrx_info.currCol * NUM_ROW_BITS)] > currUsTime) {
						ledOut |= (1 << (index + LAMP_DATA_OFFSET));
					}
				}
			}
		}

		/* Write the new values */
		digital_output_upd(ledOut, LAMPMTRX_CTL_MASK);
	}
}

/* [] END OF FILE */
