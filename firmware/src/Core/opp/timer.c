
/**
 * @file:   timer.c
 * This is the timer function.  It is currently expecting a 1ms timer, and
 * simply calls the neopixel stuff on every tick.
 *
 *===============================================================================
 */
#include "stdtypes.h"

#include "timer.h"

#include "mcu.h"
#include "opp.h"

typedef struct {
	U16 loopUsec; // Last CNT value read (0-999) (us), reset each ms.
	UINT msCnt; // counter incrementing each ms (no control on overflow)
	UINT cnt10ms; // counter from 0 - 9 (ms), for neopixel use (10 ms loop)
	UINT incandCnt; // counter from 0 - 39 (ms), for neopixel use (40 ms loop)
} TMR_INFO;

TMR_INFO tmrInfo;

/* Prototypes */
void neopxl_10ms_tick();
void fade_10ms_tick();

/*
 * ===============================================================================
 * 
 * Name: timer_init
 * 
 * ===============================================================================
 *
 * Initialize timer
 * 
 * Clear the msCnt.
 * Bluepill is clocked by a 8 MHz quartz, and clock set to 48 MHz
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void timer_init() {
	tmrInfo.msCnt = 0;
	tmrInfo.cnt10ms = 0;
	tmrInfo.incandCnt = 5; /* Offset cnt10ms by 5 ms */

	// Enable TIM1 clock
	rccBase_p->APB2ENR |= 0x00000800;

	// Initialize TIM1 1 ms timer
	tim1Base_p->PSC = 47; // Prescaler value => divide clock frequency by 48
	tim1Base_p->ARR = 999; // Auto-reload value => when CNT reaches 999, restarts to 0 and sets Interrupt flag (overflow)
	tim1Base_p->CR1 = 0x0001; // control register 1: Up counter, never stops, enabled
	tim1Base_p->CR2 = 0x0000; // control register 2:
	tim1Base_p->SMCR = 0x0000; // slave mode control register
	tim1Base_p->DIER = 0x0000; // DMA/Interrupt enable register: all disabled

	/* Register the timer isr, start the timer */
}

/*
 * ===============================================================================
 * 
 * Name: timer_overflow_isr
 * 
 * ===============================================================================
 *
 * Timer overflow ISR
 * 
 * On an overflow interrupt, it increments the ms timer.
 * This routine is not used as an ISR, but rather by polling the UIF flag from SR register.
 * 
 * @param   None 
 * @return  None
 * ===============================================================================
 */
void timer_overflow_isr() {
	tmrInfo.loopUsec = (U16) tim1Base_p->CNT; // Counter value

	/* Statement added so interface can be polled instead of interrupt driven */
	if (tim1Base_p->SR & TIMx_SR_UIF) { // status register: Update interrupt pending
		tmrInfo.msCnt++;

		/* If timer overflowed after reading usec, just set it to 0 */
		if (tmrInfo.loopUsec > 900) {
			tmrInfo.loopUsec = 0;
		}

		/* Clear isr pending bit */
		tim1Base_p->SR = 0;

		/* Call the neopixel timer.  This will eventually be a registered
		 *  recurring timer event with a function pointer.
		 */
		tmrInfo.incandCnt++;
		if (tmrInfo.incandCnt >= 40) {
			tmrInfo.incandCnt = 0;

			/* Move to next LED state */
			opp_info.ledStateNum++;
			opp_info.ledStateNum &= (OPP_MAX_STATE_NUM - 1);
			if ((opp_info.ledStateNum & 0x7) == 0) {
				opp_info.ledStatus ^= OPP_STAT_BLINK_FAST_ON;
			}
			if (opp_info.ledStateNum == 0) {
				opp_info.ledStatus ^= OPP_STAT_BLINK_SLOW_ON;

				/* Blink status LED */
				opp_info.statusBlink ^= OPP_STAT_TOGGLE_LED;
				*((R32*) OPP_STAT_BSRR_PTR) = opp_info.statusBlink;
			}
		}

		tmrInfo.cnt10ms++;
		if (tmrInfo.cnt10ms >= 10) {
			tmrInfo.cnt10ms = 0;
			neopxl_10ms_tick();
			fade_10ms_tick();
		}
	}
}

/*
 * ===============================================================================
 *
 * Name: timer_get_ms_count
 *
 * ===============================================================================
 *
 * Get ms count
 *
 * @param   None
 * @return  ms count
 * ===============================================================================
 */
UINT timer_get_ms_count() {
	return tmrInfo.msCnt;
}

/*
 * ===============================================================================
 *
 * Name: timer_get_us_count
 *
 * ===============================================================================
 *
 * Get us count
 *
 * @param   None
 * @return  us count
 * ===============================================================================
 */
U16 timer_get_us_count() {
	return tmrInfo.loopUsec;
}


/*
 * ===============================================================================
 *
 * Name: timer_get_msus_count
 *
 * ===============================================================================
 *
 * Get ms combined with us count
 * high 16-bit is ms and low 16-bit is us
 *
 * @param   None
 * @return  us count
 * ===============================================================================
 */
UINT timer_get_msus_count() {
	return tmrInfo.msCnt<<16 | tmrInfo.loopUsec;
}
