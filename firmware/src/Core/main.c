/*
 *===============================================================================
 *
 * main.c
 * This is main file. It initializes the MCU, the tasks, and runs them in a loop.
 *
 *===============================================================================
 */

#include "usb_device.h"

// TODO use Cube SDK supplied values (see Inc/main.h)
// This include will define some variables
#define INSTANTIATE_PROC
#include "opp/mcu.h"

#include "opp/opp.h"



/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);



/**
  * @brief GPIO Initialization Function
  * @retval None
  */
static void MX_GPIO_Init(void) {
   /* GPIO Ports Clock Enable */
   __HAL_RCC_GPIOC_CLK_ENABLE();
   __HAL_RCC_GPIOD_CLK_ENABLE();
   __HAL_RCC_GPIOB_CLK_ENABLE();
   __HAL_RCC_GPIOA_CLK_ENABLE();
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void) {
   RCC_OscInitTypeDef RCC_OscInitStruct = {0};
   RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
   RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

   /** Initializes the clocks
   */
   RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
   RCC_OscInitStruct.HSEState = RCC_HSE_ON;
   RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
   RCC_OscInitStruct.HSIState = RCC_HSI_ON;
   RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
   RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
   RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6; // => 8Mhz x 6 = 48 Mhz clock
   if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
      Error_Handler(1);
   }
   /** Initializes the CPU, AHB and APB busses clocks
   */
   RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
   RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
   RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
   RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2; // => APB1 @ 24 Mhz
   RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1; // => APB2 @ 48 Mhz

   if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK) {
      Error_Handler(1);
   }
   // USB
   PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
   PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;
   if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
      Error_Handler(1);
   }
}



/* Setup GPB2 as output for status led (available on STM32F103CB boards)
 *
*/
static void main_dbgled_init() {
    GPIO_InitTypeDef           pinCfg;

	pinCfg.Pin = GPIO_PIN_2;
	pinCfg.Mode = GPIO_MODE_OUTPUT_PP;
	pinCfg.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &pinCfg);
}



/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void) {

   /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
   HAL_Init();
#if OPP_DEBUG_PORT == 0
   __HAL_AFIO_REMAP_SWJ_DISABLE();
#endif

   /* Configure the system clock */
   SystemClock_Config();

   /* Initialize all configured peripherals */
   MX_GPIO_Init();
   MX_USB_DEVICE_Init();

   EnableInterrupts; /* Enable global interrupts. */

   main_dbgled_init();

   opp_main();

}


/* Gnu arm tool chain fournit une implémentation de __assert_func (cf. assert.h)
 * Il semblerait possible de la redéfinir...
 */
void __assert_func(const char *file, int line, const char *func, const char *failedexpr)
{
	Error_Handler(2);
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @param strokes: number of slow strokes after 5 initial fast strokes. This should  be a value of OPP_ERROR_E.
  * @retval None
  * strokes:
  *     1 = Hardware initialization error (ERR_HARD_INIT)
  * 	2 = assert error (cf. __assert_func) (ERR_ASSERT)
  * 	3 = hard fault (cf. HardFault_Handler) (ERR_HARD_FAULT)
  * 	4 = unable to program flash (ERR_FLASH)
  * 	5 = unable to alloc memory in heap (ERR_MALLOC_FAIL)
  */
#pragma GCC optimize ("O0")
void Error_Handler(int strokes) {
  __disable_irq();
  // either clock is configured (48MHz) or not yet (16MHz)
  #define LOOP48MHZ 500000
  const int loop_fast = SystemCoreClock == 48000000 ? LOOP48MHZ : LOOP48MHZ / 3;
  const int loop_slow = loop_fast << 1;
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
  while (1)  {
	// 5 rapid strokes
	for (int i=0; i<10; i++) {
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_2);
		for (int i = 0; i < loop_fast; i++);
	}
	// Pause 1 slow stroke
	for (int i = 0; i < (loop_slow << 2); i++);
	// @strokes slow strokes
	for (int i=0; i<(strokes << 1); i++) {
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_2);
		for (int i = 0; i < loop_slow; i++);
	}
	// Pause 1 slow stroke
	for (int i = 0; i < (loop_slow << 2); i++);
  }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
